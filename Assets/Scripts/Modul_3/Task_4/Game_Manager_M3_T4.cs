﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Cards.ScriptableObjects;

namespace Cards
{
    public class Game_Manager_M3_T4 : MonoBehaviour
    {
        private Material _baseMaterial;
       [SerializeField] private Card _cardPrefab;
        [SerializeField] private int _CardsInDesk = 30;
        [SerializeField] private CardDeck _deck1;
        [SerializeField] private CardDeck _deck2;
        [SerializeField] private CardPackConfiguration [] _packs;
        private List <CardPropertiesData> _allCards;
        private bool _IsfirstPlayerTurn = true;
        [SerializeField] private PlayerHand _playerHand1;
        [SerializeField] private PlayerHand _playerHand2;
       

        private void Awake()
        {
            _baseMaterial = new Material(Shader.Find("TextMeshPro/Sprite"))
            { 
                renderQueue = 2000 
            }
            ;
        }

        private void CreateDeck(CardDeck parent)
        {
            var deck = new LinkedList<Card>();
            float step = 0f;

            for (int i = 0; i < _CardsInDesk; i++)
            {
                var card = Instantiate(_cardPrefab, parent.transform);
                card.transform.eulerAngles = new Vector3(0f, 0f, 0f);
                card.transform.localPosition = new Vector3(0f, step, 0f);
                card.EnableFront = false;
               //card.transform.localScale = _cardPrefab.transform.localScale;

                step += 0.1f;

                var random = _allCards[Random.Range(0, _allCards.Count)];
                var mat = new Material(_baseMaterial)
                {
                    mainTexture = random.Texture
                };
                card.SetProperties(random, mat,CardUtility.GetDescriptionById(random.Id));
                deck.AddLast(card);
            }
            parent.SetDeck(deck);
        }

        void Start()
        {
            
            IEnumerable <CardPropertiesData>  allCards = new LinkedList<CardPropertiesData>();
            foreach (var pack in _packs) allCards = pack.UnionProperties(allCards);
            _allCards = new List<CardPropertiesData>(allCards);

            CreateDeck(_deck1);
            CreateDeck(_deck2);
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Space) && _IsfirstPlayerTurn || Input.GetKeyDown(KeyCode.W) && !_IsfirstPlayerTurn)
            {
                SwitchTurn();
            }
        }

        private void SwitchTurn()
        {
            if (_IsfirstPlayerTurn)
            {
                _playerHand1.ChangePlayerCards(false);
                _playerHand2.ChangePlayerCards(true);
                _playerHand2.AddCard(_deck2.DrawCard());
            }
            else
            {
                _playerHand1.ChangePlayerCards(true);
                _playerHand2.ChangePlayerCards(false);
                _playerHand1.AddCard(_deck1.DrawCard());
            }
            _IsfirstPlayerTurn = !_IsfirstPlayerTurn;
        }

        [ContextMenu("Collapse temp objects")]

        private void CollapseTempObjects() => ChangeTempObjects(false);

        [ContextMenu("Show temp objects")]

        private void ShowTempObjects() => ChangeTempObjects(true);

        private void ChangeTempObjects(bool value)
        {
            var objs = FindObjectsOfType<Transform>().Where(t => t.name.Contains("_Temp"));

            foreach(var obj in objs)
            {
                var component = obj.GetComponent<MeshRenderer>();
                if (component == null) continue;
                component.enabled = value;
            }
        }
    }

}
