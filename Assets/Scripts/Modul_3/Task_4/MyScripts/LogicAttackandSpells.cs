﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Morbus_Cards
{
    //КЛАСС СО СВСЕМИ МЕТОДАМИ РАБОТЫ КАРТЫ
    public class LogicAttackandSpells : MonoBehaviour
    {
        [SerializeField] private BaseCard _hat; //ссылка на шапочку, которая является особенной картой и не спавнится в колоду
        [SerializeField] private MorbusGameManager _manager;
        [SerializeField] private CorutineisAndEffects _effects;

        //логика хила (для заклинаний ИЗ РУКИ)

        public void Healing(BaseCard card, CardPlayer player)
        { 
            if(player.Health < player.maxHealthValue)
            {
                player.Health += card._oneActionAttack;
                HealPlayer(player);

                if (card._cardType == CardType.Spell)
                { 
                    SpellsPayCost(card,  player);

                }
            }
           
        }

        //логика щита (для заклинаний ИЗ РУКИ)

        public void Shield(BaseCard card, CardPlayer player)
        {
            if (player.Shield < player.maxShieldValue)
            {
                player.Shield += card._oneActionAttack;

                if (player.Shield > player.maxShieldValue)
                {
                    player.Shield = player.maxShieldValue;
                }

                player.ShieldText.text = player.Shield.ToString();
                _effects.ShieldEffectOn(player.myPlayerType);

                if (card._cardType == CardType.Spell)
                {
                    SpellsPayCost(card, player);

                }
            }
        }

        //логика массового урона (для заклинаний ИЗ РУКИ)

        public void DamageAllOneShot(BaseCard card, CardPlayer enemy, CardPlayer player)
        {
            foreach(var enem in enemy.Battlefield)
            {
                if (enem.pair == null) continue;
                if(card._cardType == CardType.Beast)
                {
                    enem.pair._health -= card._attack;
                    _effects.DamageForCardOn(enem.pair);
                }

                else if (card._cardType == CardType.Spell)
                {
                    enem.pair._health -= card._oneActionAttack;
                    _effects.DamageForCardOn(enem.pair);
                }
               
                if (enem.pair._health <= 0) 
                {
                    DestroyEnemyCard( enemy,  enem);
                }
                else
                {
                    
                    enem.pair._healthText.text = enem.pair._health.ToString();
                }
             }

            RebootMyFighter(card, player);

            if (card._cardType == CardType.Spell)
            {
                SpellsPayCost(card, player);
            }
            
        }

        //здоровье врагу и себе (для заклинаний ИЗ РУКИ)

        public void MaxHealingMinHealing(CardPlayer player, CardPlayer enemy, BaseCard card)
        {
            if (player.Health < player.maxHealthValue) //вот такие проверки потом засунь в другое место, в игрока
            {
                player.Health += card._oneActionAttack;

                HealPlayer(player);
               
                enemy.Health += card._attack;
                 HealPlayer(enemy);
                 SpellsPayCost(card, player);
            }
           }

        //урон врагу и себе  (для заклинаний ИЗ РУКИ)

        public void MaxDamMinDamage(CardPlayer player, CardPlayer enemy, BaseCard card)
        {
            //ДЛЯ ВРАГА

            Damage(enemy, card._oneActionAttack);
           //ДЛЯ ИГРОКА
            Damage(player, card._attack);
            SpellsPayCost(card,  player);

}
        //Урон по 1 цели. (для заклинаний С ПОЛЯ БОЯ)
        public void DamageOne(BaseCard enemyCard, BaseCard playerCard, CardPlayer player, CardPlayer enemy)
        {
            if (playerCard._cardType == CardType.Beast)
            {
                enemyCard._health -= playerCard._attack;
                _effects.DamageForCardOn(enemyCard);
            }

            else if (playerCard._cardType == CardType.Spell)
            {
                enemyCard._health -= playerCard._oneActionAttack;
                _effects.DamageForCardOn(enemyCard);
            }
            if (enemyCard._health <= 0) 
            {  
                foreach(var enem in enemy.Battlefield)
                {
                    if(enem.pair == enemyCard)
                    {
                        DestroyEnemyCard(enemy, enem);
                    }
                }
            }
            enemyCard._healthText.text = enemyCard._health.ToString();
            
            RebootMyFighter(playerCard, player);

            playerCard.IsBeastTurn = false;

            if (playerCard._cardType == CardType.Spell)
            {
                DestroySpellCard(player, playerCard);
            }

        }

        //Превращение в шапочку (для заклинаний С ПОЛЯ БОЯ)
        public void ActivSheep(BaseCard enemyCard, BaseCard playerCard, CardPlayer player, CardPlayer enemy)
        {

         var car = Instantiate(_hat, Vector3.zero, Quaternion.identity);
           car.transform.SetParent(enemyCard.myHand.transform);
            car.transform.localRotation = enemyCard.transform.localRotation;
            car.transform.localPosition = enemyCard.posOnTable.transform.localPosition;
            car._myPlayerType = enemyCard._myPlayerType;
            car.IsOnTable = true;
            car.manager = _manager;
            car.posOnTable.localPosition = enemyCard.posOnTable.transform.localPosition;
            car.posOnTable.localScale = enemyCard.posOnTable.transform.localScale;
            car.posOnTable.localRotation = enemyCard.posOnTable.transform.localRotation;
            enemy.SubScribe(car);
            car.transform.Rotate(0.0f, 180.0f, 0.0f, Space.Self);
            car.posOnTable.localRotation = car.transform.localRotation;


            foreach (var enem in enemy.Battlefield)
            { 
                if (enem.pair == enemyCard)
                {
                    enem.pair = car;
                }
            }
            enemy.myCards.Remove(enemyCard);
            _effects.DestroyBeastCardOn(enemyCard);
            RebootMyFighter(playerCard, player);
            DestroySpellCard(player, playerCard);
        }

        //Удаление шапочки (для заклинаний С ПОЛЯ БОЯ)
        public void DisActiveHat(BaseCard card, CardPlayer player, BaseCard hat)
        {
            foreach(var pl in player.Battlefield)
            {
                if(pl.pair == hat)
                {
                    pl.pair = null;
                    pl.IsEmpty = true;
                    }
            }
            
            RebootMyFighter( card,  player);
            DestroySpellCard(player, card);
            _effects.DestroyBeastCardOn(hat);
           
        }

        //урон игроку (для заклинаний С ПОЛЯ БОЯ)
        public void PlayerDamage(BaseCard enemyCard, CardPlayer player, CardPlayer enemy)
        {
            if(enemyCard._cardType == CardType.Beast)
            {
               
                enemyCard.IsBeastTurn = false;
                Damage( player, enemyCard._attack);
            }
            else if (enemyCard._cardType == CardType.Spell)
            {
                Damage(player, enemyCard._oneActionAttack);
                DestroySpellCard(enemy, enemyCard);
            }

            RebootMyFighter(enemyCard, enemy);
 }

        public void MorbusAbilka(BaseCard enemyCard, CardAbilkaPlayer abilka, CardPlayer enemy, CardPlayer player)
        {
            enemyCard._health -= abilka.myDamage;
            if (enemyCard._health <= 0) 
            {
                foreach (var enem in enemy.Battlefield)
                {
                    if (enem.pair == enemyCard)
                    { 
                        DestroyEnemyCard(enemy, enem);
                    }
                }
            }
            enemyCard._healthText.text = enemyCard._health.ToString();
            _effects.DamageForCardOn(enemyCard);
            player.Manna -= abilka.cost;
            player.MannaText.text = player.Manna.ToString();
            _effects.MannaEffectOn(player.myPlayerType);
            abilka.IsAbilka = false;
            abilka.SpellReady.SetActive(false);
            abilka.SpellEmpty.SetActive(true);
            abilka.SpellEffect.SetActive(false);
            abilka.IsMorbAbilka = false;

        }

        // общий метод оплаты за карту заклинания
        public void SpellsPayCost(BaseCard card, CardPlayer player)
        {
            player.Manna -= card._cost; 
            player.MannaText.text = player.Manna.ToString();
            _effects.MannaEffectOn(player.myPlayerType);
            card.myHand.RemoveCard(card);
            _effects.DestroySpellCardOn(card);
            
        }

        //общий метод уничтожения карты врага
        public void DestroyEnemyCard(CardPlayer enemy, FitePlace enem)
        {
            enemy.myCards.Remove(enem.pair);
            _effects.DestroyBeastCardOn(enem.pair);
            enem.IsEmpty = true;
            enem.pair = null;
        }

        //общий метод очищения настроек выбранного бойца игрока
        public void RebootMyFighter(BaseCard card, CardPlayer player)
        {
            player.myFighter = null; //обнуляю бойца у игрока
            player.IsFighterChoose = false; //проставляю буль, что боец не выбран
            card.IsFighter = false; //убираю у карты буль, что она выбрана
            card.selec.SetActive(false); //убираю у карты подсветку выбранной
        }

        //общий метод лечения игроков
        public void HealPlayer(CardPlayer player)
        {
            

            if (player.Health > player.maxHealthValue)
            {
                player.Health = player.maxHealthValue;
            }

            player.HealthText.text = player.Health.ToString();
            _effects.HealingEffectOn(player.myPlayerType);
        }

        //общий метод урона для игрока
        public void Damage(CardPlayer player, int damag)
        {
            if (player.Shield > 0)
            {
                int i = player.Shield -= damag;
                
                 if (i < 0)
                {
                    player.Shield = 0;
                    player.Health += i;
                }

                _effects.ShieldEffectOn(player.myPlayerType);
            }

            else if (player.Shield <= 0)
            {
                player.Health -= damag;
            }

            if (player.Health <= 0)
            {
                _manager.Win(player);
            }

            player.ShieldText.text = player.Shield.ToString();
            player.HealthText.text = player.Health.ToString();
            _effects.DamagEffectOn(player.myPlayerType);
        }

        //общий метод уничтожения использованной карты заклинания
        public void DestroySpellCard(CardPlayer player, BaseCard card)
        {
            player._spellPlace.IsEmpty = true;
            player._spellPlace.pair = null;
            player.myCards.Remove(card);
            _effects.DestroySpellCardOn(card);
        }
    }

}
