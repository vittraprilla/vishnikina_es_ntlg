﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace Morbus_Cards
{
    //КЛАСС РУКИ ИГРОКА
    public class Hand : MonoBehaviour
    {
        [SerializeField] private Vector3[] _positions;
       public List<BaseCard> _cards;
        [SerializeField] private List<FitePlace> _placeCards;
        [SerializeField] private CardPlayer myPlayer;
        void Start()
        {
            _cards = new List<BaseCard>(_positions.Length);
            _cards.Capacity = 10;
            for (int i = 0; i < _positions.Length; i++) _cards.Add(null);
        }


        public void AddCard(BaseCard card) //метод добавления карты в руку
        { 
           
            foreach(var place in _placeCards)
            {
                if(place.IsEmpty == true)
                {
                    int i = 0;
                    for (; i < 10; i++)
                    {
                        if (_cards[i] == null) break;
                    }

                    if (i == 10)
                    {   /*это если рука полна, больше карты не нужны,и выбранную карту мы удаляем*/
                        Destroy(card.gameObject);
                        return;
                    }

                    myPlayer.myCards.Add(card);
                    myPlayer.SubScribe(card);
                    card.transform.SetParent(transform, true);
                    _cards.Add(card);
                    card.Move(place.pos);
                    place.IsEmpty = false;
                    place.pair = card;
                    break;
                }
            }

           
        }

        public void ChangePlayerCards(bool IsShow) //закрытие карт во время хода врага
        {
            foreach (var card in _cards)
            {
                if (card == null) continue;
                card.EnableFront = IsShow;
            }
        }

        public void RemoveCard(BaseCard card) //перемещение карты на поле боя
        { 
             foreach (var pl in _placeCards)
                {
                    if (pl.pair == card)
                    {
                        pl.IsEmpty = true;

                    }
                }
                _cards.Remove(card);
        }


        //это для редактора
        [ContextMenu("Find positions")]

        private void FindPositions()
        {
            var objs = GetComponentsInChildren<Transform>();
            var positions = new LinkedList<Vector3>();
            for (int i = 0; i < objs.Length; i++)
            {
                if (objs[i] == transform || !objs[i].name.Contains("_Temp")) continue;
                positions.AddLast(objs[i].localPosition);
            }

            _positions = positions.ToArray();
        }

    }
}

