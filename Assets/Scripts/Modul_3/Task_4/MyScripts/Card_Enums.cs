﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Morbus_Cards
{
    //ВСЕ ЭНАМЫ КАРТ
   
    public enum CardClass : byte
    {
        //класс карт - базовые для всех, карты 1 и 2 игроков
        Usual = 0,
        Morbus = 1,
        Peppo = 2
    }

    public enum CardPlayerType : byte
    {
        Morbus = 0, //игровой тип карт - 1 или 2 игрок
        Peppo = 1
    }

    public enum CardType: byte
    {
        Beast = 0, //типы карт, монстр, заклинание или шапочка
        Spell = 1,
        Hat = 2
    }

    //дальше 2 перечисления типов действия
    public enum CardOneAction: byte
    {
        None = 0,
        DamageOne = 1,
        DamageAll = 2,
        Shield = 3,
        Healing = 4,
        MaxDamMinDamage = 5,
        MaxHealingMinHealing = 6,
        ActiveHat = 7,
        DisActiveHat = 8
    }

    public enum CardRegularAction: byte
    {
        None = 0,
        DamageOne = 1,
        DamageAll = 2,
        DamagePlusShieldMax = 3
    }
}

