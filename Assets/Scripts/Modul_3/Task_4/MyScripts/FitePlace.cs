﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Morbus_Cards
{
    //КЛАСС ДЛЯ БОЕВЫХ ЯЧЕЕК ДЛЯ КАРТ НА ПОЛЕ
    public class FitePlace : MonoBehaviour
    {
        public bool IsEmpty; //буль пустая ячейка или нет
        public Vector3 pos; //ее координаты на поле
        public BaseCard pair; //карта, которая лежит в ячейке
        void Start()
        {
            IsEmpty = true;
        }

        // Update is called once per frame
        void Update()
        {

        }
    }

}
