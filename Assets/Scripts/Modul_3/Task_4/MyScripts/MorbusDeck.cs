﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
namespace Morbus_Cards
{
    //КЛАСС КОЛОДЫ КАРТ
    public class MorbusDeck : MonoBehaviour
    {
       [SerializeField] private BaseCard _prefabCard; //префаб карты
        private int _cardInTable = 30; //максимальное количество карт в колоде
        [SerializeField] private CardSettings[] _defaultcards; //общие карты
        [SerializeField] private CardSettings[] _morbuscards; //карты игрока
        public List<BaseCard> _deckMorbus = new List<BaseCard>(); //лист для карт в колоде
        public MorbusGameManager manager;
        public CardPlayerType myPlayerType;
        public Hand myHand;


        void Start()
        {
            CreateDeck(this);
        }

        public void CreateDeck(MorbusDeck parent) //создание карт в колоде
        {
            float step = 0f;
            for (int i = 0; i < _cardInTable; i++)
            {
                int randInt = Random.Range(0, 3);

                //с помощью рандома спавним карты - свои и общие, пока не дойдем до максимального количества карт

                if (randInt == 0 || randInt == 2)
                {
                    int r = Random.Range(0, _defaultcards.Length);
                    var defaultBeastSettings = _defaultcards[r];
                    var card = Instantiate(_prefabCard, parent.transform);
                    card.SetSettings(defaultBeastSettings);
                    card.manager = manager;
                    card.EnableFront = false;
                    card.myHand = myHand;
                    card._myPlayerType = myPlayerType;
                    card.transform.localPosition = new Vector3(0f, step, 0f);
                    _deckMorbus.Add(card);
                }
                else if (randInt == 1 || randInt == 3)
                {
                    int r1 = Random.Range(0, _morbuscards.Length);
                    var morbusBeastSettings = _morbuscards[r1];
                    var card1 = Instantiate(_prefabCard, parent.transform);
                    card1.SetSettings(morbusBeastSettings);
                    card1.transform.localPosition = new Vector3(0f, step, 0f);
                    card1.manager = manager;
                    card1.myHand = myHand;
                    card1._myPlayerType = myPlayerType;
                    card1.EnableFront = false;
                    _deckMorbus.Add(card1);
                }
                step += 0.2f;


            }
        }

        //удаление карты из колоды
        public BaseCard DrawCard()
        { 
            if(_deckMorbus.Count > 0)
            {
                var card = _deckMorbus.Last();
                _deckMorbus.Remove(card);
                return card;
            }

            else
            {
                return null;
            }
            
        }
    }

}
