﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
namespace Morbus_Cards
{
    //КЛАСС РАБОТЫ АБИЛКИ ИГРОКОВ
    public class CardAbilkaPlayer : MonoBehaviour, IPointerClickHandler
    {
        public CardPlayerType myPlayerType; //ссылка на игровой тип
        public CardPlayer myPlayer; //ссылка на игрока
        public MorbusGameManager manager;
        public int myDamage; //урон абилки
        public TextMeshPro DamageText;
        public bool IsAbilka; //були для регулировки однократной работы абилки
        public bool IsMorbAbilka;
        public int cost; //цена
        public TextMeshPro costText;
        public GameObject SpellEmpty; //для визуального отображения работы абилки
        public GameObject SpellReady;
        public GameObject SpellEffect;

        public void OnPointerClick(PointerEventData eventData) 
        {
            //метод работы абилки 2 игрока - при клике плюсует щит игроку
            if (myPlayer.Manna >= cost && IsAbilka == true)
            {
                if (myPlayerType == CardPlayerType.Peppo && manager.PlayerType == CardPlayerType.Peppo)
                {

                    if (myPlayer.Shield < myPlayer.maxShieldValue)
                    {
                        myPlayer.Shield += myDamage;

                        if (myPlayer.Shield > myPlayer.maxShieldValue)
                        {
                            myPlayer.Shield = myPlayer.maxShieldValue;
                        }
                        myPlayer.ShieldText.text = myPlayer.Shield.ToString();
                        myPlayer.effects.ShieldEffectOn(myPlayerType);
                        myPlayer.Manna -= cost; 
                        myPlayer.MannaText.text = myPlayer.Manna.ToString();
                        myPlayer.effects.MannaEffectOn(myPlayerType);
                        IsAbilka = false;
                        SpellEmpty.SetActive(true);
                        SpellEffect.SetActive(false);

                    }
                   
                }
                //абилка 1 игрока - урон вражеской карте на поле
                else if (myPlayerType == CardPlayerType.Morbus && manager.PlayerType == CardPlayerType.Morbus)
                {
                    if(IsMorbAbilka == false)
                    {
                        if (myPlayer.myFighter != null)
                        {
                            myPlayer.myFighter.IsFighter = false; //если выбираем абилку, то сбрасывается выбранная для атаки карта
                            myPlayer.myFighter.selec.SetActive(false);
                            myPlayer.myFighter = null;
                            myPlayer.IsFighterChoose = false;
                        }
                        IsMorbAbilka = true;
                        SpellReady.SetActive(true); //в этом классе мы готовим абилку к бою, а сам метод работы в другом классе

                    }
                    else
                    {
                        IsMorbAbilka = false;
                        SpellReady.SetActive(false);
                    }
                    
                  
                }
            }
           
        }

        void Start()
        {
            costText.text = cost.ToString();
            DamageText.text = myDamage.ToString();
        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}

