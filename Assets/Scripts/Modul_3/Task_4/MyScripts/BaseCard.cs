﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
namespace Morbus_Cards
{
    //БАЗОВЫЙ КЛАСС КАРТЫ
    public class BaseCard : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler //, IBeginDragHandler, IEndDragHandler,IDragHandler
    {

        [SerializeField] private GameObject _front; //общее
        [SerializeField] private MeshRenderer _icon; //картинка карты
        public TextMeshPro _name; //имя карты
        public TextMeshPro _costText; //текст цены
        public int _cost; //цена
        public TextMeshPro _description; //описание
        [SerializeField] private CardClass _class; //класс
        public CardType _cardType; //тип карты
        public CardOneAction _oneAction; //тип однократного действия
       public int _oneActionAttack; //значение действия
       public CardRegularAction _regularAction; //тип многократного действия
        public TextMeshPro _attackText;//текст атаки
        public int _attack; //количество атаки
        public TextMeshPro _healthText;
        public int _health;//здоровье
        public string id; //айди
        public TextMeshPro _spellType; //текст типа заклинания
        private float _speed = 1f; 
        public Transform posOnTable; //трансформ для сохранения размера и положения карты
        private CardSettings _settings; //настройки
        public MorbusGameManager manager;
       public bool IsOnTable; //буль состояния карты на столе
        public bool IsBattle; //буль состояния карты на столе
        public Hand myHand; //ссылка на руку
        public bool IsFighter; //буль состояния карты на столе
        public bool IsFixRotation; //буль для поворота карт второго игрока
        public CardPlayerType _myPlayerType; //игровой тип карты
        public GameObject selec; //подсветка для выбранной карты
        public bool IsBeastTurn; //буль для состояния карт монстров
        public GameObject[] myEffects; //ссылка на эффекты карты


        public void SetSettings(CardSettings beastSettings) //при спавне карты берем ее настройки из СО
        {
            _settings = beastSettings;
            _icon.material = _settings._icon;
            _name.text = _settings._name;
            _cost = _settings._cost;
            _costText.text = _settings._cost.ToString();
            _description.text = _settings._description;
            _class = _settings._class;
            _cardType = _settings._cardType;
            _oneAction = _settings._oneAction;
            _oneActionAttack = _settings._oneActionAttack;
             id = _settings.id;
            var mat = _front.gameObject.GetComponent<MeshRenderer>();
            mat.material = _settings._front;
            _attack = _settings._attack;

            if (_cardType == CardType.Beast) //отдельно проставляем для монстров и заклинаний их параметры
            {
                _regularAction = _settings._regularAction;
                _attackText.text = _settings._attack.ToString();
                _healthText.text = _settings._health.ToString();
                _health = _settings._health;

            }

            else if (_cardType == CardType.Spell)
            {
                _spellType.text = _settings._spellType;
            }

        }

        public bool EnableFront
        {

            get => _front.activeSelf;
            set
            {
                transform.Rotate(new Vector3(0f, 0f, 180f));
                _front.SetActive(value);
            }
        }

        public void Move(Vector3 endPoint) //движение карты из деки в руку
        {
            StartCoroutine(Moving(endPoint));
            StartCoroutine(Cooldown());
            EnableFront = true;
        }

        public void MoveOnTable(Vector3 endPoint) //движение карты из руки на стол
        {
            StartCoroutine(Moving(endPoint));
            StartCoroutine(Cooldown());
            IsOnTable = false;
        }

        private IEnumerator Moving(Vector3 endPos) //корутина перемещения карты
        {
            var time = 0f;
           
            var startPos = transform.localPosition;
            while (time < 1f)
            {
                transform.localPosition = Vector3.Lerp(startPos, endPos, time);
                time += _speed * Time.deltaTime;
                posOnTable.localPosition = this.transform.localPosition;
                posOnTable.localScale = this.transform.localScale;
                posOnTable.localRotation = this.transform.localRotation;

                yield return null;

            }
        }

        IEnumerator Cooldown() //колдаун, чтобы карты не менялись в размере, если на них навести мышку, пока карта движется
        {
            yield return new WaitForSeconds(2f);
            IsOnTable = true;
            
            if (_myPlayerType == CardPlayerType.Peppo)
            {
                if(IsFixRotation == false)
                {
                    transform.Rotate(0.0f, 180.0f, 0.0f, Space.Self);
                    posOnTable.localRotation = this.transform.localRotation;
                    IsFixRotation = true;
                }
                
            }
        }


        //методы тыка в карты, она увеличивается в размерах
        public void OnPointerEnter(PointerEventData eventData)
        {
            if (!EnableFront) return; 
            else if(IsOnTable == true)
            {
                transform.localScale += new Vector3(7f, 0f, 7f);
                transform.localPosition += new Vector3(1f, 4f, 4f);
                if (_myPlayerType != manager.PlayerType)
                {
                    transform.Rotate(0.0f, 180.0f, 0.0f, Space.Self);
                }
                manager.Isturn = false;
            }
          
        }
        //метод, когда убираем курсор с карты
        public void OnPointerExit(PointerEventData eventData)
        {
            if (!EnableFront) return;
            else if(IsOnTable == true)
            {
                if (_myPlayerType != manager.PlayerType)
                {
                    transform.Rotate(0.0f, 180.0f, 0.0f, Space.Self);
                }
                transform.localScale = posOnTable.localScale;
                transform.localPosition = posOnTable.localPosition;
                manager.Isturn = true;
            }
            
        }


        //делегат и событие для клика
        public event ClickEventHandler OnClickEventHandler;
        public delegate void ClickEventHandler(BaseCard card);
        public void OnPointerClick(PointerEventData eventData)
        {
            OnClickEventHandler?.Invoke(this);
        }

        private void Start()
        { //при старте присваиваем базовые значения трансформу для движения
            posOnTable.localPosition = this.transform.localPosition;
            posOnTable.localScale = this.transform.localScale;
            IsBattle = false;
        }
    }

}
