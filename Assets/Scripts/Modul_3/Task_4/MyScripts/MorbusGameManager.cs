﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

namespace Morbus_Cards
{
    //КЛАСС МЕНЕДЖЕРА ИГРЫ
    public class MorbusGameManager : MonoBehaviour
    {
        private bool _IsfirstPlayerTurn = true; //буль для хода
        [SerializeField] private Hand _playerHand1; //ссылки на руки, деки, игроков
        [SerializeField] private Hand _playerHand2;
        [SerializeField] private MorbusDeck _deck1;//потом замени одну на девочку
        [SerializeField] private MorbusDeck _deck2;
        [SerializeField] private CardPlayer _player1;
        [SerializeField] private CardPlayer _player2;
        [SerializeField] private int _maxManna = 10; //количество манны
        [SerializeField] private int _manna = 2;
        public CardPlayerType PlayerType;
        [SerializeField] private LogicAttackandSpells _logicAttack;
        [SerializeField] private GameObject _panelMorb; //панели для выигрыша
        [SerializeField] private GameObject _panelPeppo;
        public bool Isturn = true;

        [SerializeField] private GameObject _camera;
       
        void Start()
        {
            Isturn = true;
            _IsfirstPlayerTurn = true;
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Space) && _IsfirstPlayerTurn && Isturn == true || Input.GetKeyDown(KeyCode.Space) && !_IsfirstPlayerTurn && Isturn == true)
            {
                SwitchTurn();
            }
        }

        private void SwitchTurn() //смена хода
        {
            if (_IsfirstPlayerTurn) //ход 1 игрока
            {
                PlayerType = CardPlayerType.Morbus; //меняем игровой тип на тот, который сейчас ходит
                _playerHand1.ChangePlayerCards(true); //открываем нужную руку
                _playerHand2.ChangePlayerCards(false);
                _playerHand1.AddCard(_deck1.DrawCard()); //добавляем новую карту
                if (_player1.Manna < _maxManna) //прибавляем манну
                {
                    _player1.Manna+=_manna;
                    _player1.MannaText.text = _player1.Manna.ToString();
                    _player1.effects.MannaEffectOn(_player1.myPlayerType);
                }
                _player1.myAbilka.IsAbilka = true; //активируем абилку
                _player1.myAbilka.SpellEmpty.SetActive(false);
                _player1.myAbilka.SpellEffect.SetActive(true);

                Isturn = false;
                StartCoroutine(CooldownFirst()); //запускаем корутину для действий карт в начале хода
                StartCoroutine(CameraMove()); //корутина движения камеры

                if (_deck1._deckMorbus.Count <= 1)
                {
                    _deck1.CreateDeck(_deck1); //если карты закончились, заново спавним колоду
                }

            }
            else 
            {   //ход 2 игрока
                PlayerType = CardPlayerType.Peppo;
                _playerHand1.ChangePlayerCards(false);
                _playerHand2.ChangePlayerCards(true);
                _playerHand2.AddCard(_deck2.DrawCard());
                if(_player2.Manna < _maxManna)
                {
                    _player2.Manna += _manna;
                    _player2.MannaText.text = _player2.Manna.ToString();
                    _player2.effects.MannaEffectOn(_player2.myPlayerType);
                }
                _player2.myAbilka.IsAbilka = true;
                _player2.myAbilka.SpellEmpty.SetActive(false);
                _player2.myAbilka.SpellEffect.SetActive(true);
                Isturn = false;
                StartCoroutine(CooldownTwo());
                StartCoroutine(CameraMove());

                if (_deck2._deckMorbus.Count <= 1)
                {
                    _deck2.CreateDeck(_deck2);
                }
            }

            _IsfirstPlayerTurn = !_IsfirstPlayerTurn;
        }


        IEnumerator CooldownFirst()
        {
            yield return new WaitForSeconds(1f);
            Isturn = true;
             foreach (var pl in _player1.Battlefield)
            {
               if  (pl.pair != null) //проходим по картам на поле 
                {
                    pl.pair.IsBeastTurn = true;
                    //активируем их постоянные действия
                    if (pl.pair._regularAction == CardRegularAction.DamageAll)
                    {
                        _logicAttack.DamageAllOneShot(pl.pair, _player2, _player1);
                    }
                    if (pl.pair._regularAction == CardRegularAction.DamagePlusShieldMax)
                    {
                        _logicAttack.Shield(pl.pair, _player1);
                    }
                }
               
            }
                
        }
        IEnumerator CooldownTwo()
        {
            yield return new WaitForSeconds(1f);
            Isturn = true;
            foreach (var pl in _player2.Battlefield)
            { if(pl.pair != null)
                {
                    pl.pair.IsBeastTurn = true;
                    if (pl.pair._regularAction == CardRegularAction.DamageAll)
                    {
                        _logicAttack.DamageAllOneShot(pl.pair, _player1, _player2);
                    }
                    if (pl.pair._regularAction == CardRegularAction.DamagePlusShieldMax)
                    {
                        _logicAttack.Shield(pl.pair, _player2);
                    }
                }
                
            }
        }

        public void Win(CardPlayer looser) //метод выигрыша
        {
            if(looser.myPlayerType == _player1.myPlayerType)
            {
                _panelPeppo.SetActive(true);
            }

            else if (looser.myPlayerType == _player2.myPlayerType)
            {
                _panelMorb.SetActive(true);
            }
        }


        //метод движения камеры
        IEnumerator CameraMove()
        {
            yield return new WaitForSeconds(0.5f);

            if (_IsfirstPlayerTurn == false)
            {
                var anim = _camera.GetComponent<Animation>();
                anim.Play("M3T4CameraToMorbus");

            }

            else if (_IsfirstPlayerTurn == true)
            {
                var anim = _camera.GetComponent<Animation>();
                anim.Play("M3T4CameraToPeppo");

            }


        }
    }

}
