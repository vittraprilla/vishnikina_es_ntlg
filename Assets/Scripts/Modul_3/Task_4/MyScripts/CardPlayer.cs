﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.EventSystems;
namespace Morbus_Cards
{
    //КЛАСС ИГРОКА
    public class CardPlayer : MonoBehaviour, IPointerClickHandler
    {
        public int Health; //основные параметры
        public int Shield;
        public int Manna;
        public int maxHealthValue;
        public int maxShieldValue;
        public TextMeshPro HealthText;
        public TextMeshPro ShieldText;
        public TextMeshPro MannaText;
        [SerializeField]  private MorbusGameManager _manager;
        public List <BaseCard> myCards; //список карт игрока
        public List<BaseCard> enemyCards; //список карт врага, лежащих на поле
        public List<FitePlace> Battlefield; //список из 3 мест для карт игрока на поле
        public CardPlayer Enemy; //ссылка на врага
        public FitePlace _spellPlace; //место для заклинания на поле
        public CardPlayerType myPlayerType; //игровой тип игрока
        public BaseCard myFighter; //выбранная для атаки карта
        public bool IsFighterChoose; //буль для выбора карты
        public CardAbilkaPlayer myAbilka; //ссылка на абилку
        public CorutineisAndEffects effects; //эффекты игрока

        [SerializeField] private LogicAttackandSpells _logicAttack = new LogicAttackandSpells();

       void Start()
        {
            HealthText.text = Health.ToString();
            ShieldText.text = Shield.ToString();
            MannaText.text = Manna.ToString();

        }

        public void SubScribe(BaseCard card) //метод подписки для вражеских и своих карт на метод клика по ним
        {
            if (card._myPlayerType == myPlayerType)
            {
                
                    card.OnClickEventHandler += HandleClick;

            }

            else
            {
                card.OnClickEventHandler += HandleEnemyClick;
                
            }

        }

        //метод клика на свою карту
        public void HandleClick(BaseCard card)
        {
            _manager.Isturn = true; 
            
            //я жму, если это мой ход, мой колор равен колору манагера

            if(myPlayerType == card._myPlayerType && myPlayerType == _manager.PlayerType)
            {
                //если я жму на свою карту
                
                if(card._myPlayerType == _manager.PlayerType) //ненужная проверка
                {
                    if (card.IsBattle == false) //для клика на карты в руке
                    { //если выкладываем монстра
                        if (card._cardType == CardType.Beast)
                        {
                            foreach (var place in Battlefield)
                            {
                                if (place.IsEmpty == true && card._cost <= Manna)
                                {
                                    if (card._oneAction == CardOneAction.Healing)
                                    {
                                        _logicAttack.Healing(card, this); //если у карт дополнительные действия - один раз дает щит или лечит
                                    }                                     //мы делаем их перед тем, как выложить ее на стол

                                    if (card._oneAction == CardOneAction.Shield)
                                    {
                                        _logicAttack.Shield(card, this);
                                    }

                                    //выкладываем карту на стол
                                    card.transform.localScale = card.posOnTable.localScale; //тут эти куски в отдельные методы надо
                                    card.transform.localPosition = card.posOnTable.localPosition;
                                    card.MoveOnTable(place.pos);
                                    place.IsEmpty = false;
                                    place.pair = card;
                                    card.IsBattle = true; //проставляем нужные параметры для состояния на столе
                                    card.myHand.RemoveCard(card);
                                    Manna -= card._cost;
                                    MannaText.text = Manna.ToString(); //вычитаем цену
                                    effects.MannaEffectOn(myPlayerType);
                                    Enemy.enemyCards.Add(card); //врагу тоже добавляем нашу карту
                                    Enemy.SubScribe(card);

                                    break;
                                }
                            }
                        }
                        //выкладываем заклинание
                        else if (card._cardType == CardType.Spell)
                        {
                            if (card._cost <= Manna)
                            {
                                if (_spellPlace.IsEmpty == true && card._oneAction == CardOneAction.DamageOne || _spellPlace.IsEmpty == true && card._oneAction == CardOneAction.ActiveHat || _spellPlace.IsEmpty == true && card._oneAction == CardOneAction.DisActiveHat)
                                {  //для заклинаний, которые перемещаются на поле 
                                    card.transform.localScale = card.posOnTable.localScale;
                                    card.transform.localPosition = card.posOnTable.localPosition;
                                    card.MoveOnTable(_spellPlace.pos);
                                    _spellPlace.IsEmpty = false;
                                    _spellPlace.pair = card;
                                    card.IsBattle = true;
                                    card.myHand.RemoveCard(card);
                                    Manna -= card._cost;
                                    MannaText.text = Manna.ToString();
                                    _manager.Isturn = false;
                                }
                                else  //для карт, которые действуют сразу
                                {
                                    if (card._oneAction == CardOneAction.Healing)
                                    { //все методы действия карт прописаны в отдельном классе
                                        //отсюда мы посылаем их в этот класс
                                        _logicAttack.Healing(card, this);
                                    }

                                    if (card._oneAction == CardOneAction.Shield)
                                    {
                                        _logicAttack.Shield(card, this);
                                    }

                                    if (card._oneAction == CardOneAction.DamageAll)
                                    {
                                        if(enemyCards.Count >= 1)
                                        {
                                            _logicAttack.DamageAllOneShot(card, Enemy, this);
                                        }
                                        
                                    }

                                    if (card._oneAction == CardOneAction.MaxHealingMinHealing)
                                    {
                                        _logicAttack.MaxHealingMinHealing(this, Enemy, card);
                                    }

                                    if (card._oneAction == CardOneAction.MaxDamMinDamage)
                                    {
                                        _logicAttack.MaxDamMinDamage(this, Enemy, card);
                                    }
                                }



                            }

                        }

                        else if(card._cardType == CardType.Hat) //для карты шапочка, которая ничего не делает (овца в хартстоуне)
                        {
                            if(myFighter._oneAction == CardOneAction.DisActiveHat)
                            {
                                _logicAttack.DisActiveHat(myFighter, this, card);
                            }

                            else
                            {
                                Debug.Log("NO");
                            }
                        }
                    }

                    else if (card.IsBattle == true) //для клика на карты, лежащие на поле боя
                    {
                        if (IsFighterChoose == false) //при клике мы их выбираем
                        {
                            myFighter = card;
                            IsFighterChoose = true;
                            card.IsFighter = true;
                            card.selec.SetActive(true);
                            if(myAbilka.IsMorbAbilka == true)
                            {
                                myAbilka.IsMorbAbilka = false;
                                myAbilka.SpellReady.SetActive(false);
                            }

                        }
                        else if (IsFighterChoose == true)
                        {
                            if( card == myFighter)
                            {
                                myFighter = null;
                                IsFighterChoose = false;
                                card.IsFighter = false;
                                card.selec.SetActive(false);
                                
                            }

                            
                        }
                       


                    }
                }

                
            }


           

        }

        public void HandleEnemyClick(BaseCard card)  //метод для клика на вражеские карты
        {
            if (myPlayerType != card._myPlayerType && myPlayerType == _manager.PlayerType)
                
            {
                if (myFighter != null)

                { //для заклинания
                    if(myFighter._oneAction == CardOneAction.DamageOne)
                    {
                        Debug.Log("Kuku");
                        _logicAttack.DamageOne(card, myFighter, this, Enemy);
                    }

                    else if (myFighter._regularAction == CardRegularAction.DamageOne && myFighter.IsBeastTurn == true)
                    {
                        _logicAttack.DamageOne(card, myFighter, this, Enemy);
                    }

                    else if (myFighter._oneAction == CardOneAction.ActiveHat)
                    {
                        _logicAttack.ActivSheep(card, myFighter, this, Enemy);
                    }

                    else if (myFighter._regularAction == CardRegularAction.DamagePlusShieldMax && myFighter.IsBeastTurn == true)
                    {
                        _logicAttack.DamageOne(card, myFighter, this, Enemy);
                        
                    }

                }

                else
                {
                    if(myPlayerType == CardPlayerType.Morbus && myAbilka.IsMorbAbilka == true)
                    {
                        _logicAttack.MorbusAbilka(card, myAbilka, Enemy, this);
                    }
                }

            }
        }
        // Update is called once per frame
        void Update()
        {

        }

        //метод клика на самого игрока - для врага, чтобы нанести урон
        public void OnPointerClick(PointerEventData eventData)
        {
            //тут мы проверяем, если перед игроком лежит максимальное количество его карт в бою - 3 штуки
            //то враг не может нанести урон
            //если выложено от 2 до 0 карт, то урон игроку нанести можно

            if (_manager.PlayerType == Enemy.myPlayerType)
            {
                int i = 0;
                foreach (var myfite in Battlefield)
                {
                    if (myfite.pair != null)
                    {
                        i++;
                    }
                }

                if (i< 3)
                {
                    if (Enemy.myFighter != null) 
                    {
                        if(Enemy.myFighter._cardType == CardType.Beast && Enemy.myFighter.IsBeastTurn == true)
                        {
                            _logicAttack.PlayerDamage(Enemy.myFighter, this, Enemy);
                        }
                    else if (Enemy.myFighter._cardType == CardType.Spell)
                        {
                            _logicAttack.PlayerDamage(Enemy.myFighter, this, Enemy);
                        }

                    }
                }
                
            }
        }
    }

}
