﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Morbus_Cards
{
    //КЛАСС ДЛЯ ВКЛЮЧЕНИЯ ЭФФЕКТОВ
    public class CorutineisAndEffects : MonoBehaviour
    {
       [SerializeField] private GameObject[] _effectsPlayer1;
        [SerializeField] private GameObject[] _effectsPlayer2;


        //Включение эффектов для игроков
        //тут можно было бы сделать более емко, не 4 одинаковыми кусками, но решила уже здесь не оптимизировать

        //лечение

        public void HealingEffectOn(CardPlayerType playerType)
        {
            if (playerType == CardPlayerType.Morbus)
            {
                _effectsPlayer1[0].SetActive(true);
                StartCoroutine(HealingEffectOff(playerType));
            }

            else if (playerType == CardPlayerType.Peppo)
            {
                _effectsPlayer2[0].SetActive(true);
                StartCoroutine(HealingEffectOff(playerType));
            }
        }
     public   IEnumerator HealingEffectOff(CardPlayerType playerType)
        {
            yield return new WaitForSeconds(1f);

            if(playerType == CardPlayerType.Morbus)
            {
                _effectsPlayer1[0].SetActive(false);
            }

            else if(playerType == CardPlayerType.Peppo)
            {
                _effectsPlayer2[0].SetActive(false);
            }
            
        }

        //дамаг

        public void DamagEffectOn(CardPlayerType playerType)
        {
            if (playerType == CardPlayerType.Morbus)
            {
                _effectsPlayer1[1].SetActive(true);
                StartCoroutine(DamagEffectOff(playerType));
            }

            else if (playerType == CardPlayerType.Peppo)
            {
                _effectsPlayer2[1].SetActive(true);
                StartCoroutine(DamagEffectOff(playerType));
            }
        }
        public IEnumerator DamagEffectOff(CardPlayerType playerType)
        {
            yield return new WaitForSeconds(1f);

            if (playerType == CardPlayerType.Morbus)
            {
                _effectsPlayer1[1].SetActive(false);
            }

            else if (playerType == CardPlayerType.Peppo)
            {
                _effectsPlayer2[1].SetActive(false);
            }

        }

        //щит

        public void ShieldEffectOn(CardPlayerType playerType)
        {
            if (playerType == CardPlayerType.Morbus)
            {
                _effectsPlayer1[2].SetActive(true);
                StartCoroutine(ShieldEffectOff(playerType));
            }

            else if (playerType == CardPlayerType.Peppo)
            {
                _effectsPlayer2[2].SetActive(true);
                StartCoroutine(ShieldEffectOff(playerType));
            }
        }
        public IEnumerator ShieldEffectOff(CardPlayerType playerType)
        {
            yield return new WaitForSeconds(1f);

            if (playerType == CardPlayerType.Morbus)
            {
                _effectsPlayer1[2].SetActive(false);
            }

            else if (playerType == CardPlayerType.Peppo)
            {
                _effectsPlayer2[2].SetActive(false);
            }

        }
        //манна

        public void MannaEffectOn(CardPlayerType playerType)
        {
            if (playerType == CardPlayerType.Morbus)
            {
                _effectsPlayer1[3].SetActive(true);
                StartCoroutine(MannaEffectOff(playerType));
            }

            else if (playerType == CardPlayerType.Peppo)
            {
                _effectsPlayer2[3].SetActive(true);
                StartCoroutine(MannaEffectOff(playerType));
            }
        }
        public IEnumerator MannaEffectOff(CardPlayerType playerType)
        {
            yield return new WaitForSeconds(1f);

            if (playerType == CardPlayerType.Morbus)
            {
                _effectsPlayer1[3].SetActive(false);
            }

            else if (playerType == CardPlayerType.Peppo)
            {
                _effectsPlayer2[3].SetActive(false);
            }

        }


        //для карт

        //уничтожение использованного заклинания
        public void DestroySpellCardOn(BaseCard card)
        {
            card.myEffects[2].SetActive(true);
            StartCoroutine(DestroyCardOff(card));
        }

        //уничтожение убитого монстра
        public void DestroyBeastCardOn(BaseCard card)
        {
            card.myEffects[1].SetActive(true);
            StartCoroutine(DestroyCardOff(card));
        }

        public IEnumerator DestroyCardOff(BaseCard card)
        {
            yield return new WaitForSeconds(1f);
            Destroy(card.gameObject);
        }

        //получения урона у карты
        public void DamageForCardOn(BaseCard card)
        {
            card.myEffects[0].SetActive(true);
            StartCoroutine(DamageForCardOff(card));
        }

        public IEnumerator DamageForCardOff(BaseCard card)
        {
            yield return new WaitForSeconds(1f);
            card.myEffects[0].SetActive(false);
        }
    }

}
