﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
namespace Morbus_Cards
{ //КЛАСС СОЗДАНИЯ СО ДЛЯ КАРТ

    [CreateAssetMenu(fileName = "CardSettings", menuName = "Card Settings", order = 51)]
    public class CardSettings : ScriptableObject
    {
        public Material _front;
        public Material _icon;
        public string _name;
        public int _cost;
        public string _description;
        public CardClass _class;
        public CardType _cardType;
        public CardOneAction _oneAction;
        public int _oneActionAttack;
        public CardRegularAction _regularAction;
        public int _attack;
        public int _health;
        public string _spellType;
        public string id;
    }
}


