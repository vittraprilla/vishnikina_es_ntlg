﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Cards
{
    public class CardDeck : MonoBehaviour
    {
        private LinkedList<Card> _deck;

        public Card DrawCard()
        {
            var card = _deck.Last.Value;
            _deck.Remove(card);
            return card;
        }

        public void SetDeck(LinkedList<Card> deck)
        {
            _deck = deck;
        }
    }

}
