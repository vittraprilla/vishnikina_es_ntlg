﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace Cards
{ //namespace
    public class Card : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler //, IBeginDragHandler, IEndDragHandler,IDragHandler
    { //class

        //IBeginDragHandler - берем карту, она начинает следовать за курсором, куда он наводится
        //IEndDragHandler - это когда отпускаем карту, надо проверить, находится ли она где-то над столом и куда-то ее положить
        //IDragHandler
        [SerializeField] private GameObject _front;
       [SerializeField] private TextMeshPro _name;
        [SerializeField] private TextMeshPro _cost;
        [SerializeField] private TextMeshPro _attack;
        [SerializeField] private TextMeshPro _health;
        [SerializeField] private TextMeshPro _type;
        [SerializeField] private TextMeshPro _description;
        [SerializeField] private MeshRenderer _icon;
        private float _speed = 1f;
        public bool EnableFront
        {
            get => _front.activeSelf;
            set {
                transform.Rotate(new Vector3(0f, 0f, 180f));
                _front.SetActive(value); }
        }

        public void SetProperties(CardPropertiesData data, Material material, string description)
        {
            _icon.material = material;
            _cost.text = data.Cost.ToString();
            _name.text = data.Name;
            _type.text = data.Type.ToString();
            _attack.text = data.Attack.ToString();
            _health.text = data.Health.ToString();
    }

        [ContextMenu("ChangeActive")] //позволяет вызывать из редактора методы компонента
        private void ChangeActive()
        {
            _front.SetActive(!_front.activeSelf); //а вот эта самая конструкция, которая включает и выключает
        }


        public void Move(Vector3 endPoint)
        {
            StartCoroutine(Moving(endPoint));
        }

        private IEnumerator Moving(Vector3 endPos)
        {
            var time = 0f;
            var startPos = transform.localPosition;
            while(time < 1f)
            {
                transform.localPosition = Vector3.Lerp(startPos, endPos, time);
                time += _speed * Time.deltaTime;
                yield return null;

            }
        }

        public void OnPointerEnter(PointerEventData eventData)
        { if (!EnableFront) return;
            transform.localScale += new Vector3(0.3f, 0f, 0.3f);
            transform.localPosition += new Vector3(0f, 2f, 0f);
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            if (!EnableFront) return;
            transform.localScale -= new Vector3(0.3f, 0f, 0.3f);
            transform.localPosition -= new Vector3(0f, 2f, 0f);
        }
    }//class

}//namespace
