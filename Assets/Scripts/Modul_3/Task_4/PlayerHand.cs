﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
namespace Cards
{
    public class PlayerHand : MonoBehaviour
    {
        [SerializeField] private Vector3 [] _positions;
        private List<Card> _cards;

        public void AddCard(Card card)
        {
            int i = 0;
            for (; i < _cards.Count; i++)
            {
                if (_cards[i] == null) break;
            }

            if(i == _cards.Count)
            {   /*это если рука полна, больше карты не нужны,и выбранную карту мы удаляем*/
                Destroy(card.gameObject);
                return;
            }

            _cards[i] = card;
            card.transform.SetParent(transform, true);
            card.Move(_positions[i]);
        }
        public void ChangePlayerCards(bool IsShow)
        {
            foreach (var card in _cards)
            {
                if (card == null) continue;
                card.EnableFront = IsShow;
            }
        }

        private void Start()
        {
            _cards = new List<Card>(_positions.Length);
            for (int i = 0; i < _positions.Length; i++) _cards.Add(null);
        }


        [ContextMenu("Find positions")]

        private void FindPositions()
        {
            var objs = GetComponentsInChildren<Transform>();
            var positions = new LinkedList<Vector3>();
            for (int i = 0; i < objs.Length; i++)
            {
                if (objs[i] == transform || !objs[i].name.Contains("_Temp")) continue;
                positions.AddLast(objs[i].localPosition);
            }

            _positions = positions.ToArray();
        }
    }
}

