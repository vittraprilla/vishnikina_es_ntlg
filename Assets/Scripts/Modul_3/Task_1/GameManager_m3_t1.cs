﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager_m3_t1 : MonoBehaviour
{
   [SerializeField] private List <Blocks> _blocks = new List <Blocks>(); //лист с блоками

    
    [SerializeField] private int _countBlocks; //количество блоков
    [SerializeField] private int _health = 3; //здоровье
    [SerializeField] private Text _blocks_textCount_1; //количество блоков для вывода на экран
    [SerializeField] private Text _blocks_textCount_2;
    [SerializeField] private Text _health_textCount_1; //количество здоровья для вывода на экран
    [SerializeField] private Text _health_textCount_2;
    [SerializeField] private Text _final_textCount_1; //финальное количество неразрушенных блоков
    [SerializeField] private Text _final_textCount_2;
    [SerializeField] private GameObject _panel_death_1; //окно смерти
    [SerializeField] private GameObject _panel_death_2;
    [SerializeField] private GameObject _panel_win_1; //окно победы
    [SerializeField] private GameObject _panel_win_2;



    void Start()
    {
        Time.timeScale = 1; //на всякий случай актуализирую время
        //ставлю блокам рандомный поворот
        foreach (var block in _blocks)
        {
            block.transform.rotation = Random.rotation;
       }

        _countBlocks = _blocks.Count; //устанавливаю количество блоков
        //обновляю на экране количество блоков и здоровья
        _blocks_textCount_1.text = _countBlocks.ToString();
        _blocks_textCount_2.text = _countBlocks.ToString();
        _health_textCount_1.text = _health.ToString();
        _health_textCount_2.text = _health.ToString();
    }


    //метод уничтожения блоков
    public void DestroyBlocks(Blocks block)
    {
        _blocks.Remove(block); //убираем блок из списка
        Destroy(block.gameObject); //уничтожаем блок на сцене
        _countBlocks--; //обновляем количество блоков
        _blocks_textCount_1.text = _countBlocks.ToString(); //обновляем количество на экране
        _blocks_textCount_2.text = _countBlocks.ToString();
        if (_countBlocks <= 0) 
        {
           //если все блоки разрушены, открываем окно победы
            _panel_win_1.SetActive(true);
            _panel_win_2.SetActive(true);
            StartCoroutine(TimeStop()); //и запускаем корутину остановки времени
        }
    }

    IEnumerator TimeStop() //остановка времени в случае поражения или победы
    {
        yield return new WaitForSeconds(0.5f);
        Time.timeScale = 0;

    }

    //метод нанесения урона
    public void Damage()
    {
        _health--; //отнимаем жизнь и обновляем инфу на экране
        _health_textCount_1.text = _health.ToString();
        _health_textCount_2.text = _health.ToString();

        if(_health <= 0) 
        {
           //если здоровье 0, открываем окно смерти
            _panel_death_1.SetActive(true);
            _final_textCount_1.text = _countBlocks.ToString();
           _final_textCount_2.text = _countBlocks.ToString();
            _panel_death_2.SetActive(true);
            StartCoroutine(TimeStop()); //запускаем корутину остановки времени
        }


    }

    public void ReturnMenu() //метод для кнопки возвращения в меню
    {
        Time.timeScale = 1; //возвращаем время
        SceneManager.LoadScene("Modul_3_Task_2_Menu");

    }
    public void PlayGame() //метод для рестарта уровня
    { 
        Time.timeScale = 1; //возвращаем время
        SceneManager.LoadScene("Modul_3_Lesson_1");
   }

    public void ExitButton() //метод для кнопки выхода
    {
        Application.Quit();
        Debug.Log("Exit");
    }

}
