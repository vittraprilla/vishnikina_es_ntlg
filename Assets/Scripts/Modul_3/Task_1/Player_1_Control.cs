﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;


public class Player_1_Control : MonoBehaviour
{
    [SerializeField] private float _speedPlayer; //скорость игрока
    [SerializeField] private GameObject _camera_1; //ссылка на 1 камеру
    [SerializeField] private Vector3 _offset_1; //отступ для 1 камеры
    private float _smothSpeed = 0.1f; //скорость для сглаживания движения камеры
    public bool _IsGoBall; //буль для старта
    [SerializeField] private Ball _ball; //ссылка на мячик
    public bool IsPause; //буль паузы
    [SerializeField] private GameObject _panelPause; //ссылка на панель паузы


    void Start()
    {
        StartCoroutine(Move_Player_1()); //запускаем корутину движения
        _IsGoBall = true; //проставляем були
        IsPause = false;

    }

    // Update is called once per frame
    void LateUpdate()
    { //движение камеры за игроком
        Vector3 Position_1 = transform.position + _offset_1;
        Vector3 SmothVector_1 = Vector3.Lerp(_camera_1.transform.position, Position_1, _smothSpeed);
        _camera_1.transform.position = SmothVector_1;
    }

    private IEnumerator Move_Player_1 ()
    {
        while (true)
        {

            //движение
            if (Input.GetKey(KeyCode.A))
            {
                transform.Translate(-(transform.forward) * _speedPlayer * Time.deltaTime);
              
            }
            if (Input.GetKey(KeyCode.D))
            {
                transform.Translate(transform.forward * _speedPlayer * Time.deltaTime);
            }


            if (Input.GetKey(KeyCode.S))
            {
                transform.Translate(-(transform.right) * _speedPlayer * Time.deltaTime);
            }

            if (Input.GetKey(KeyCode.W))
            {
                transform.Translate(transform.right * _speedPlayer * Time.deltaTime);
            }

            //запуск мячика
            if (Input.GetKey(KeyCode.Space) && _IsGoBall == true)
            {  
               //отдаю шару направление игрока и запускаю игру
                Vector3 _vel = transform.forward;
                _ball.SetVelocity(_vel);
               _IsGoBall = false;
        
            }
            //включение паузы
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                if (IsPause == false)
                { //если буль ложный, запускаем корутину включения паузы
                    StartCoroutine(OpenPanel());
                }

                else
                { //если пауза запущена, возвращаем время и запускаем корутину выключения паузы
                    Time.timeScale = 1;
                    StartCoroutine(ClosePanel());
                   
                }
               }

            yield return null;
        }
   }

    //метод для выключения паузы из кнопки меню-паузы
    public void KeyPause()
    {
        Time.timeScale = 1;
        StartCoroutine(ClosePanel());
   }


    //корутина открытия меню паузы
   IEnumerator OpenPanel()
    {
        yield return new WaitForSeconds(0.2f);
        _panelPause.gameObject.SetActive(true); //открываем панель с кнопками
        StartCoroutine(TimeStop()); //стартуем корутину выключения времени

    }
    
    IEnumerator TimeStop() //корутина остановки времени
    {
        yield return new WaitForSeconds(0.5f);
        IsPause = true;
        Time.timeScale = 0;

    }


    //корутина закрытия меню-паузы
    IEnumerator ClosePanel()
    {
        yield return new WaitForSeconds(0.2f);
        Time.timeScale = 1; //возвращаем время
        var anim = _panelPause.GetComponent<Animation>(); //проигрываем анимацию закрытия
        anim.Play("PanelSetCloseAnim");
        StartCoroutine(ClosePanelAfterAnim()); //запускаем корутину с закрытием панели

    }

    //корутина для закрытия панели с меню, когда анимация отыгралась
    IEnumerator ClosePanelAfterAnim()
    {
        yield return new WaitForSeconds(0.5f);
        _panelPause.gameObject.SetActive(false);
        IsPause = false;
    }

    //столкновение с мячом
    private void OnCollisionEnter(Collision collision)
    { //смотрим, что это мячик и он не нулевой
        var temp = collision.collider.GetComponent<Ball>();
        if (temp != null)
        {
            Vector3 newDirection = Vector3.Reflect(collision.transform.forward, collision.contacts[0].normal);
            temp.SetVelocity(newDirection);

            foreach (var item in collision.contacts)
            {
                Debug.DrawRay(item.point, item.normal * 100, Random.ColorHSV(0f, 1f, 1f, 1f, 0.5f, 1f), 10f);
            }
        }
    }




}
