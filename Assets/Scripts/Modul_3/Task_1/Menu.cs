﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Menu : MonoBehaviour
{
    [SerializeField] private Slider _soundSlider; //ссылка на слайдер громкости
    [SerializeField] private float _soundSliderVal; //флоат для сохранения значения громкости
    [SerializeField] private Toggle _soundToggle; //ссылка на чекбокс отключения музыки
    [SerializeField] private Dropdown _dropdown; //ссылка на выпадающий список сложности
    [SerializeField] private int _index; //инт для сохранения индекса выбранного значения в списке
    [SerializeField] private GameObject _panelSet; //ссылка на панель настроек
    void Start()
    {
        Time.timeScale = 1; //на всякий случай актуализирую время

        //прохожусь по ключам сохраненных настроек музыки и сложности
        //если ключи есть, выставляю сохраненные настройки

        if (PlayerPrefs.HasKey("SoundValue")) 
        {
            _soundSlider.value = PlayerPrefs.GetFloat("SoundValue", _soundSliderVal);

        }

        if (PlayerPrefs.HasKey("DropValue"))
        {
            _dropdown.value = PlayerPrefs.GetInt("DropValue", _index);

        }

        if (PlayerPrefs.HasKey("SoundOf"))
        {
            _soundToggle.isOn = false;
        }
    }

    public void SaveSettings() //метод сохранения настроек при выходе из окна настроек

    {
        _soundSliderVal = _soundSlider.value; //сохраняем громкость
        PlayerPrefs.SetFloat("SoundValue", _soundSliderVal);
        PlayerPrefs.Save();

        _index = _dropdown.value; //сохраняем сложность

        PlayerPrefs.SetInt("DropValue", _index);
        PlayerPrefs.Save();

        if (_soundToggle.isOn == false) //сохраняем чекбокс
        {
            PlayerPrefs.SetInt("SoundOf", 1);
            PlayerPrefs.Save();
        }

        else if(_soundToggle.isOn == true)
        {
            PlayerPrefs.DeleteKey("SoundOf");
        }

        var anim = _panelSet.GetComponent<Animation>(); //включаем анимацию закрытия панели
        anim.Play("PanelSetCloseAnim");
        StartCoroutine(ClosePanel()); //стартуем корутину закрытия панели


    }



    IEnumerator ClosePanel() //закрываем панель
    {
        yield return new WaitForSeconds(1f);
        _panelSet.gameObject.SetActive(false);

    }

    //метод для перехода в сцену игры
    public void PlayGame()
    {
        SceneManager.LoadScene("Modul_3_Lesson_1");
       
    }

    public void ExitButton() //выход:)
    {
        Application.Quit();
        Debug.Log("Exit");
    }
}
