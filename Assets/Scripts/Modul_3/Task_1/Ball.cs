﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    [SerializeField] private Player_1_Control _player; //ссылка на 1 игрока
    private Vector3 _offset_1 = new Vector3(0, -3, 0); //отступ для шарика
    private float _smothSpeed = 0.1f; //скорость сглаживания движения шарика за игроком
    public float _ballSpeed = 5f; //рабочая скорость шарика
    public float _maxBallSpeed = 10f; //максимальная скорость шарика
    public float _minBallSpeed = 5f; //базовая скорость шарика
    

    void LateUpdate()
    {
        //движение шарика за камерой игрока при старте
        if ( _player._IsGoBall == true) //проверяем буль, что шар не запущен
        { //реализуем движение шарика за игроком
            Vector3 Position_1 = _player.transform.position + _offset_1;
            Vector3 SmothVector_1 = Vector3.Lerp(transform.position, Position_1, _smothSpeed);
            transform.position = SmothVector_1;
            _offset_1 = new Vector3(0, -3, 0);
        }

    }

    //метод обновления направления движения шарика
    public void SetVelocity(Vector3 forward)
    {
        transform.forward = forward; //присваиваем шарику входящий вектор движения
    }

    private void Update()
    {  //движение шарика после запуска
        if (_player._IsGoBall == false)
        {
            transform.Translate(Vector3.forward * (_ballSpeed * Time.deltaTime));
        }
            
    }

    //возвращения шарика в стартовую позицию, если он попал в ворота
    public void ReturnStartPos()
    {
        _player._IsGoBall = true;
        _ballSpeed = _minBallSpeed; //возвращаем его скорость к минимальной
    }

    //метод для кнопки рестарта, если движение шарика зациклилось
    public void Restart()
    {
        _player._IsGoBall = true;

    }
}
