﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Walls : MonoBehaviour
{
    private void OnCollisionEnter(Collision collision)
    { //смотрим, что это мячик и он не нулевой
        var temp = collision.collider.GetComponent<Ball>();
        if (temp != null)
        {
            Vector3 newDirection = Vector3.Reflect(collision.transform.forward, collision.contacts[0].normal);
            temp.SetVelocity(newDirection);

            foreach (var item in collision.contacts)
            {
                Debug.DrawRay(item.point, item.normal * 100, Random.ColorHSV(0f, 1f, 1f, 1f, 0.5f, 1f), 10f);
            }
        }
    }

}
