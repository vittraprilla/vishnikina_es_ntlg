﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Blocks : MonoBehaviour
{
    
    [SerializeField] private GameManager_m3_t1 _gameManager; //ссылка на менеджер
    
    //столкновение с мячом
    private void OnCollisionEnter(Collision collision)
    { //смотрим, что это мячик и он не нулевой
        var temp = collision.collider.GetComponent<Ball>();
        if(temp != null)
            
        {  Vector3 newDirection = Vector3.Reflect(collision.transform.forward, collision.contacts[0].normal);
            temp.SetVelocity(newDirection); //просчитываем инерцию и отдаем шарику новое направление

            foreach (var item in collision.contacts)
            {
                Debug.DrawRay(item.point, item.normal * 100, Random.ColorHSV(0f, 1f, 1f, 1f, 0.5f, 1f), 10f);
            }

            _gameManager.DestroyBlocks(this); //уничтожаем блок

            if (temp._ballSpeed < temp._maxBallSpeed) //повышаем скорость, если она не достигла максимума
            {
                temp._ballSpeed++;
            }
        }
    }
}
