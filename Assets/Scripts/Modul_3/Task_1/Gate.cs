﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gate : MonoBehaviour
{
    [SerializeField] private GameManager_m3_t1 _gameMan; //ссылка на менеджера
    [SerializeField] private Player_1_Control _player; //ссылка на 1 игрока
   

    //столкновение с мячом
    private void OnCollisionEnter(Collision collision)
    { //смотрим, что это мячик и он не нулевой
        var temp = collision.collider.GetComponent<Ball>();
        if (temp != null)
        {
            temp.ReturnStartPos(); //возвращаем мячик игроку
            _gameMan.Damage(); //отнимаем жизнь
        }
    }
}
