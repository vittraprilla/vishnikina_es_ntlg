﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_2_Control : MonoBehaviour
{

    [SerializeField] private float _speedPlayer;
    private float _smothSpeed = 0.1f;
    [SerializeField] private GameObject _camera_2;
    private Vector3 _offset_2;
    
    void Start()
    {
        StartCoroutine(Move_Player_2());

    }

    
    void LateUpdate()
    {
        Vector3 Position_2 = transform.position + _offset_2;
        Vector3 SmothVector_2 = Vector3.Lerp(_camera_2.transform.position, Position_2, _smothSpeed);
        _camera_2.transform.position = SmothVector_2;
    }

    private IEnumerator Move_Player_2()
    {
        while (true)
        {

            //ЛЕВО
            if (Input.GetKey(KeyCode.LeftArrow))
            {
                transform.Translate(transform.forward * _speedPlayer * Time.deltaTime);
                // player_1.transform.Translate(Vector3.left * speed * Time.deltaTime);

            }

            //ПРАВО
            if (Input.GetKey(KeyCode.RightArrow))
            {

                transform.Translate(-(transform.forward) * _speedPlayer * Time.deltaTime);
            }

            //ВНИЗ
            if (Input.GetKey(KeyCode.DownArrow))
            {

                transform.Translate(-(transform.right) * _speedPlayer * Time.deltaTime);
            }

            //ВВЕРХ
            if (Input.GetKey(KeyCode.UpArrow))
            {
               transform.Translate(transform.right * _speedPlayer * Time.deltaTime);


            }

            yield return null;
        }

    }

    //столкновение с мячом
    private void OnCollisionEnter(Collision collision)
    { //смотрим, что это мячик и он не нулевой
        var temp = collision.collider.GetComponent<Ball>();
        if (temp != null)
        {
            Vector3 newDirection = Vector3.Reflect(collision.transform.forward, collision.contacts[0].normal);
            temp.SetVelocity(newDirection);

            foreach (var item in collision.contacts)
            {
                Debug.DrawRay(item.point, item.normal * 100, Random.ColorHSV(0f, 1f, 1f, 1f, 0.5f, 1f), 10f);
            }
        }
    }
}
