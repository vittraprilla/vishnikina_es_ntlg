﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
namespace bot_morbus
{
    //скрипт-менеджер ворот
    public class ZikkuratComponent : MonoBehaviour, IPointerClickHandler
    {
        public ColorsType Color; //цвет ворот
        public GameManager_Modul3_Task3 manager; //ссылка на игровой менеджер
        private bool IsInst;
        public float coolDownSpawn = 3f; //спавн мобов
        public BotMorbusComponent morbBot; //ссылка на префаб моба
        public GameObject pointSpawn; //точка спавна моба
        public List<BotMorbusComponent> listEnemy = new List<BotMorbusComponent>(); //лист врагов
        public List<BotMorbusComponent> myMorbuses = new List<BotMorbusComponent>(); //лист мобов этих ворот
        public ColorsType ColorEnemyFirst; //цвета врагов
        public ColorsType ColorEnemyTwo;

        public Text countAliveMorbus; //для отображения информации в статистике
        public Text countDeadMorbus;
        public int countDeadMorb;

        //базовые значения Морбусов

        public int BaseBotHealth;
        public float BaseBotSpeed;
        public float BaseAttackSpeed;
        public float BaseAttackRadius;
        public int BaseStrongDamage;
        public int BaseBaseDamage;

        //делегаты для события кликов по воротам
        public event ClickEventHandler OnClickEventHandler;
        public delegate void ClickEventHandler(ZikkuratComponent component);
        public void OnPointerClick(PointerEventData eventData)
        {
            OnClickEventHandler?.Invoke(this);
        }
        void Start()
        {
            
        }


        // Update is called once per frame
        void Update()
        {
            if( IsInst == false) //метод спавна мобов
            {
             var morb =  Instantiate(morbBot, pointSpawn.transform.position, pointSpawn.transform.rotation);
                morb.manager = manager;
                morb.myGate = this;
                manager.AddMorb(morb);
                myMorbuses.Add(morb);
                countAliveMorbus.text = myMorbuses.Count.ToString();
                IsInst = true;
                StartCoroutine(Cooldown_inst());
            }

            FindEnemy();
        }

        IEnumerator Cooldown_inst()
        {
            yield return new WaitForSeconds(coolDownSpawn);
            IsInst = false;

        }

        public void FindEnemy() //создание общего списка живых врагов обоих цветов
        {
            var cloneAllMorb = new List<BotMorbusComponent>(manager.allMorbuses);

            foreach (var bot in cloneAllMorb)
            {
                if (bot.Color == ColorEnemyFirst || bot.Color == ColorEnemyTwo)
                {
                    if (!listEnemy.Contains(bot))
                    {
                        listEnemy.Add(bot);
                    }

                    
                }
            }
        }

        public void RemoveEnemy(BotMorbusComponent morb) //удаление врага из списка после его смерти
        {
            if (listEnemy.Contains(morb))
            {
                listEnemy.Remove(morb);
                foreach(var bot in myMorbuses)
                {
                    bot.Isfite = false;
                    bot.Isfollow = false;
                    bot.Isfind = true; //и перевод моба в состояние поиска нового врага
                    bot.GetComponent<Animator>().SetTrigger(bot.idleAnim);
                   
                }

            }

            else
            {
                Debug.Log("It is not my Morbus");
            }
            
        }

        public void RemoveMyMorbus(BotMorbusComponent morb)
        {
            if (myMorbuses.Contains(morb))
            {
                myMorbuses.Remove(morb);
                countDeadMorb++;
                countDeadMorbus.text = countDeadMorb.ToString() ;
                countAliveMorbus.text = myMorbuses.Count.ToString(); //удаление моба после его смерти
            }

            else
            {
                Debug.Log("It is not my Morbus");
            }
        }

    }

}
