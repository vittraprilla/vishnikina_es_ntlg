﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
namespace bot_morbus
{
    //основной скрипт, который лежит на ботах
    public class BotMorbusComponent : MonoBehaviour
    {
        public ColorsType Color; //ссылка на цвет моба
        public ZikkuratComponent myGate; //ссылка на его ворота
        public GameManager_Modul3_Task3 manager; //это проставлять при спавне объекта
       public List<BotMorbusComponent> mylistEnemy = new List<BotMorbusComponent>(); //список врагов моба
        public BotMorbusComponent _enemyTarget; //текущая цель-враг
        public bool Isfite; //состояние битвы
        public bool Isfind; //состояние поиска
        public bool Isfollow; // состояние преследования
        public float longAttackRadius = 3f;
        private bool IsFiteCooldown = false; //колдаун для атаки мобов
        public string illAnim; //две ссылки на анимации
        public string idleAnim;

        //значение мобов

        public int BotHealth;
        public float BotSpeed;
        public float AttackSpeed;
        public float AttackRadius;
        public int StrongDamage;
        public int BaseDamage;


        void Start()
        {
            //присваиваю значения из базовых
            BotHealth = myGate.BaseBotHealth;
                BotSpeed = myGate.BaseBotSpeed;
                AttackSpeed = myGate.BaseAttackSpeed;
                AttackRadius = myGate.BaseAttackRadius;
                StrongDamage = myGate.BaseStrongDamage;
                BaseDamage = myGate.BaseBaseDamage;

             Isfind = true; //проставляю базовое состояние - поиск
        }

        
        void Update()
        {
            if (Isfind)
            {
                //в состоянии поиска проигрывается айдловая анимация
                GetComponent<Animator>().SetTrigger(idleAnim); 
                FindEnemy(); //ищется враг
                if (_enemyTarget != null)
                {
                    Isfind = false; //если враг нашелся, переход в другое состояние
                    Isfollow = true;
                }
            }

            else if (Isfollow)
            {
                //состояние преследования
                //высчитываем дистанцию до врага и движемся к нему
                GetComponent<Animator>().SetTrigger(idleAnim);
                if (_enemyTarget != null)
                {
                    float newActualDist = Vector3.Distance(this.transform.position, _enemyTarget.transform.position);
                    var target = _enemyTarget.transform;
                    transform.LookAt(target);
                    transform.Translate(Vector3.forward * BotSpeed * Time.deltaTime);
                    if (newActualDist <= AttackRadius && _enemyTarget.BotHealth > 0)
                        {
                            Isfite = true;
                            Isfollow = false; //если достигли врага, переходим в третье состояние
                    }
                  
                }

                else
                {
                    Isfollow = false;
                    Isfind = true;
                }

                     }

            else if (Isfite)
            {
                if(IsFiteCooldown == false && _enemyTarget.BotHealth > 0)
                {
                    //в состоянии битвы проигрываем анимацию и наносим урон
                    GetComponent<Animator>().SetTrigger(illAnim);
                    _enemyTarget.BotHealth -= BaseDamage;
                    IsFiteCooldown = true;
                    StartCoroutine(Cooldown_fite());
                }
            }
         

            
            
        }


        IEnumerator Cooldown_fite()
        {
            //корутина для колдауна атаки
            yield return new WaitForSeconds(AttackSpeed);
            IsFiteCooldown = false;

        }

      public  void  FindEnemy()  //метод поиска врага
        {
           if (myGate.listEnemy.Count != 0)
            {
                mylistEnemy = myGate.listEnemy;

                float minDistance = Vector3.Distance(this.transform.position, mylistEnemy.First().transform.position);
                BotMorbusComponent ClosestEnemy = mylistEnemy.First();
                foreach (var enem in mylistEnemy)
                {
                    if (Vector3.Distance(this.transform.position, enem.transform.position) <= minDistance)
                    {
                        minDistance = Vector3.Distance(this.transform.position, enem.transform.position);
                        
                        _enemyTarget = enem;


                    }
                }
            }

        }


        private void OnTriggerEnter(Collider other) //метод для получения урона от пуль, только для красных и зеленых ботов
        {
            if(Color == ColorsType.Green || Color == ColorsType.Red)
            {
                var pula = other.GetComponent<BulletMorbusBlue>();
                if(pula != null)
                {
                    BotHealth -= StrongDamage;
                    Destroy(pula.gameObject);
                }
            }
        }



    }

}
