﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace bot_morbus
{
    public class GameManager_Modul3_Task3 : MonoBehaviour
    {
        //менеджер игры
        public List<BotMorbusComponent> allMorbuses = new List<BotMorbusComponent>(); //хранит всех мобов
        public List<ZikkuratComponent> Gates = new List<ZikkuratComponent>(); //хранит все ворота
        public GameObject panelInfo; //ссылка на панель со статистикой
        public GameObject infoActPos; //и векторы, скорость и буль для выезжания панели
        public GameObject infoDesActPos;
        public float smoothing;
        public bool IsAct;

        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
           
            var cloneAllMorb = new List<BotMorbusComponent>(allMorbuses);
           
            foreach (var morb in cloneAllMorb)
            { if(morb != null)
                {
                    if (morb.BotHealth <= 0) //проверка здоровья ботов и их удаление при смерти
                    {
                        foreach (var gate in Gates)
                        {
                            gate.RemoveEnemy(morb);
                            gate.RemoveMyMorbus(morb);
                        }

                        allMorbuses.Remove(morb);
                        Destroy(morb.gameObject);
                    }
                }
            }

            if (IsAct) //открытие и закрытие панели со статистикой
            {
                panelInfo.transform.position = Vector3.Lerp(panelInfo.transform.position, infoActPos.transform.position, smoothing * Time.deltaTime);
            }

            else if(IsAct == false)
            {
                panelInfo.transform.position = Vector3.Lerp(panelInfo.transform.position, infoDesActPos.transform.position, smoothing * Time.deltaTime);
            }
        }


        public void AddMorb(BotMorbusComponent morb) //добавление нового моба в список
        {
            allMorbuses.Add(morb);
            
        }

        public void KillAll() //метод убийства всех мобов на сцене   
        {
            foreach (var gate in Gates)
            {
                gate.countDeadMorb += int.Parse(gate.countAliveMorbus.text);
                gate.countDeadMorbus.text = gate.countDeadMorb.ToString();
                int zer = 0;
                gate.countAliveMorbus.text = zer.ToString();
                gate.listEnemy.Clear();
                gate.myMorbuses.Clear();
            }
            foreach (var morb in allMorbuses)
            {
                Destroy(morb.gameObject);
            }
            allMorbuses.Clear();

           

        }

        public void OpenPanel() //методы для кнопок открытия и закрытия панели статистики
        {
            IsAct = true;
        }

        public void ClosePanel()
        {
            IsAct = false;
        }
    }

}
