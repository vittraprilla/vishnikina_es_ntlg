﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace bot_morbus
{
    //этот скрипт легаси, никак не используется сейчас, здесь только хранятся энамы
    //использовала его в изначальной архитектуре, которую потом переписала
    public class BotMorbusBase 
    {
        public int BotHealth;
        public float BotSpeed;
        public float AttackSpeed;
        public float AttackRadius;
        public int StrongDamage; 
        public int BaseDamage;
        public ColorsType ColorType; 
        public ColorsType EnemyFirst; //удалить
        public ColorsType EnemyTwo; //удалить

        public BotMorbusBase(ColorsType enemyFirst, ColorsType enemyTwo, ColorsType colorType, int botHealth = 120, float botSpeed = 1f, int strongDamage = 3, int baseDamage = 12, float attackRadius = 1f, float attackSpeed = 1f)
        {
            BotHealth = botHealth;
            BotSpeed = botSpeed;
            StrongDamage = strongDamage;
            BaseDamage = baseDamage;
            ColorType = colorType;
            EnemyFirst = enemyFirst;
            EnemyTwo = enemyTwo;
            AttackRadius = attackRadius;
            AttackSpeed = attackSpeed;
        }

        public BotMorbusBase() { }
       
    }

    //вот только энамы я отсюда использую
    public enum ColorsType
    {
        Red,
        Green,
        Blue
    }
}

