﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace bot_morbus
{

    public class PassivePlayer : MonoBehaviour
    {
        //private PassivePlayerControl _controls;
        private float _speed = 10f;

        void Start()
        {
            //_/*controls = new PassivePlayerControl ();*/
        }

        // Update is called once per frame
        void Update()
        {
            //OnMove(_controls.PassivePlayer.Movement.ReadValue<Vector2>().ConvertToMoveVector3());
        }

        private void Direct(Vector3 direction)
        {
            direction.x = transform.position.x;
            direction.z = transform.position.z;
        }
        private void OnMove(Vector3 movement)
        {
            Direct(movement);

           // if (movement == Vector3.zero) return;
            transform.position += transform.TransformDirection(movement * _speed * Time.deltaTime);
        }
    }
}