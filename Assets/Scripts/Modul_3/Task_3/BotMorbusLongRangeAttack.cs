﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace bot_morbus
{
    //скрипт для реализации дальнего боя для синих ботов
    public class BotMorbusLongRangeAttack : MonoBehaviour
    {
        public BotMorbusComponent baseComp; //ссылка на конкретного бота
        public float longRadiusAttack = 6f; //радиус и скорость атаки
        public float longSpeedAttack = 0.01f;
        public BlueZikk gate; //ссылка на ворота
        [SerializeField] private BulletMorbusBlue _bullet; //ссылка на пулю

        private bool IsCooldown;
        void Start()
        {  //при старте инициализируем параметры
            gate = baseComp.myGate.GetComponent<BlueZikk>();
            longRadiusAttack = gate.radius;
            longSpeedAttack = gate.speed;
        }

       
        void Update()
        {
            if(baseComp.Isfollow) //стрельба реализуется у синих ботов только в состоянии преследования
            {
                float newActualDist = Vector3.Distance(transform.position, baseComp._enemyTarget.transform.position);
                if (newActualDist <= longRadiusAttack && newActualDist > baseComp.AttackRadius && IsCooldown == false)
                {
                 var bull =   Instantiate(_bullet, transform.position, transform.rotation);
                    bull.manager = this;
                    bull.target = baseComp._enemyTarget;
                    IsCooldown = true;
                    StartCoroutine(Cooldown_fite());
                }
            }
        }

        IEnumerator Cooldown_fite()  //колдаун для стрельбы
        {
            yield return new WaitForSeconds(longSpeedAttack);
            IsCooldown = false;

        }
    }

}
