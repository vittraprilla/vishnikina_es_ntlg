﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace bot_morbus
{
    public class CameraMove : MonoBehaviour
    {
        //скрипт для движения игрока по сцене, взяла из прерыдущего дз

        [SerializeField] private float _speedPlayer;
        //public PassivePlayerCameraController cam;
    void Start()
        {
            StartCoroutine(Move_Player_2());
        }

        // Update is called once per frame
        void Update()
        {

        }



        private IEnumerator Move_Player_2()
        {
            while (true)
            {

                //ЛЕВО
                if (Input.GetKey(KeyCode.A))
                {
                    transform.Translate(transform.forward * _speedPlayer * Time.deltaTime);

                }

                //ПРАВО
                if (Input.GetKey(KeyCode.D))
                {

                    transform.Translate(-(transform.forward) * _speedPlayer * Time.deltaTime);
                }

                //ВНИЗ
                if (Input.GetKey(KeyCode.S))
                {

                    transform.Translate(-(transform.right) * _speedPlayer * Time.deltaTime);
                }

                //ВВЕРХ
                if (Input.GetKey(KeyCode.W))
                {
                    transform.Translate(transform.right * _speedPlayer * Time.deltaTime);


                }

                //UP
                if (Input.GetKey(KeyCode.E))
                {

                    transform.Translate(transform.up * _speedPlayer * Time.deltaTime);
                }

                //UP
                if (Input.GetKey(KeyCode.Q))
                {

                    transform.Translate(-(transform.up) * _speedPlayer * Time.deltaTime);
                }

                //if (Input.GetKey(KeyCode.Space))  //добавила остановку вращения камеры
                //{
                //    if (cam.IsstaticScreen == false)
                //    {
                //        cam.IsstaticScreen = true;
                //    }
                //    else if (cam.IsstaticScreen == true)
                //    {
                //        cam.IsstaticScreen = false;
                //    }
                //}


                yield return null;
            }

        }
    }
}

