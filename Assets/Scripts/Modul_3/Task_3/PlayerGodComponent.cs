﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;
using UnityEngine.UI;
namespace bot_morbus
{
    //скрипт для изменений всех данных в рантайме
    public class PlayerGodComponent : MonoBehaviour
    {
        //ссылки на панели настроек ворот
        public GameObject bluePanel;
        public GameObject redPanel;
        public GameObject greenPanel;
        public GameObject actPos; //и позиции для их перемещения по экрану
        public GameObject DesActPos;
        //ссылка на игровой менеджер
        public GameManager_Modul3_Task3 manager;
        //отдельный скрипт голубых ворот
        public BlueZikk blueZikk;
        public float smoothing; //скорость перемещения панелей и их булли
        public bool IsBluePanelActiv;
        public bool IsRedPanelActiv;
        public bool IsGreenPanelActiv;

        //здоровье ботов
        public InputField[] botsHealth;
        //дальняя атака ботов
        public InputField[] botsStrongDamage;
        //ближняя атака ботов
        public InputField[] botsBaseDamage;

       //скорость спавна ботов
        public Slider[] slidersSpawn;
        public Text[] textCountsSpawn;
        //скорость ближней атаки
        public Slider[] slidersSpeedBaseAttack;
        public Text[] textCountsSpeedBaseAttack;
        //радиус ближней атаки
        public Slider[] slidersRadiusBaseAttack;
        public Text[] textCountsRadiusBaseAttack;
        //скорость ботов
        public Slider[] slidersSpeed; //ссылка на слайдеры скорости ботов
        public Text[] textCountsSpeed; //ссылка на счетчики отображения скорости ботов

        //для голубых ботов
        public Slider radiusStrongAttack;
        public Slider speedStrongAttack;
        public Text textRadiusStrongAttack;
        public Text textSpeedStrongAttack;
        void Start()
        {
            
            foreach( var gate in manager.Gates)
            {
                gate.OnClickEventHandler += HandleClick; //подписываемся на событие клика по воротам

                //отсюда идет присвоение данных в зависимости от цвета ворот
                //если синие
                if (gate.Color == ColorsType.Blue)
                {
                    StartConfig(ColorsType.Blue, gate);
                }
                //для красных
                else if (gate.Color == ColorsType.Red)
                {
                    StartConfig(ColorsType.Red, gate);
                }
                //для зеленых
                else if (gate.Color == ColorsType.Green)
                {
                    StartConfig(ColorsType.Green, gate);
                }

            }
            //плюс отдельно для синих
            radiusStrongAttack.value = blueZikk.radius;
            textRadiusStrongAttack.text = blueZikk.radius.ToString();

            speedStrongAttack.value = blueZikk.speed;
            textSpeedStrongAttack.text = blueZikk.speed.ToString();


        }

        private void HandleClick(ZikkuratComponent gate) //при клике открываем нужные нам ворота
        {
            if (gate.Color == ColorsType.Blue && IsBluePanelActiv == false && IsGreenPanelActiv == false && IsRedPanelActiv == false)
            {
                IsBluePanelActiv = true;

            }

            else if( gate.Color == ColorsType.Green && IsBluePanelActiv == false && IsGreenPanelActiv == false && IsRedPanelActiv == false)
            {
                IsGreenPanelActiv = true;
            }

            else if(gate.Color == ColorsType.Red && IsBluePanelActiv == false && IsGreenPanelActiv == false && IsRedPanelActiv == false)
            {
                IsRedPanelActiv = true;
            }
        }

        
        void Update()
        {
            //тут не смогла придумать, как оптимизировать и иметь один метод открытия и закрытия для всех панелей
            //так что для каждой свой булль и свой метод
            if (IsBluePanelActiv)
            {
                bluePanel.transform.position = Vector3.Lerp(bluePanel.transform.position, actPos.transform.position, Time.deltaTime * smoothing);
            }

            else if(IsBluePanelActiv == false)
            {
                bluePanel.transform.position = Vector3.Lerp(bluePanel.transform.position, DesActPos.transform.position, Time.deltaTime * smoothing);
            }

            if (IsGreenPanelActiv)
            {
               greenPanel.transform.position = Vector3.Lerp(greenPanel.transform.position, actPos.transform.position, Time.deltaTime * smoothing);
            }

            else if (IsGreenPanelActiv == false)
            {
                greenPanel.transform.position = Vector3.Lerp(greenPanel.transform.position, DesActPos.transform.position, Time.deltaTime * smoothing);
            }

            if (IsRedPanelActiv)
            {
                redPanel.transform.position = Vector3.Lerp(redPanel.transform.position, actPos.transform.position, Time.deltaTime * smoothing);
            }

            else if (IsRedPanelActiv == false)
            {
                redPanel.transform.position = Vector3.Lerp(redPanel.transform.position, DesActPos.transform.position, Time.deltaTime * smoothing);
            }
        }

        //дальше методы изменения параметров после пользовательского ввода


        public void ChangeSpeed(ZikkuratComponent gate)
        {
            foreach( var vali in slidersSpeed)
            {
                var col = vali.GetComponent<Color>();
                if(col.color == gate.Color)
                {
                    gate.BaseBotSpeed = vali.value;
                }
            }

            foreach (var tcs in textCountsSpeed)
            {
                var col = tcs.GetComponent<Color>();
                if (col.color == gate.Color)
                {
                    tcs.text = gate.BaseBotSpeed.ToString();
                }
            }

        }


        public void ChangeSpawn(ZikkuratComponent gate)
        {

            foreach (var slid in slidersSpawn)
            {
                var col = slid.GetComponent<Color>();
                if (col.color == gate.Color)
                {
                    gate.coolDownSpawn = slid.value;
                }

            }

            foreach (var tcs in textCountsSpawn)
            {
                var col = tcs.GetComponent<Color>();
                if (col.color == gate.Color)
                {
                    tcs.text = gate.coolDownSpawn.ToString();
                }
            }
        }

        public void ChangeBaseAttackSpeed(ZikkuratComponent gate)
        {
            foreach (var slid in slidersSpeedBaseAttack)
            {
                var col = slid.GetComponent<Color>();
                if (col.color == gate.Color)
                {
                     gate.BaseAttackSpeed = slid.value;
                }

            }

            foreach (var tcs in textCountsSpeedBaseAttack)
            {
                var col = tcs.GetComponent<Color>();
                if (col.color == gate.Color)
                {
                    tcs.text = gate.BaseAttackSpeed.ToString();
                }
            }
        }

        public void ChangeBaseAttackRadius(ZikkuratComponent gate)
        {
            foreach (var slid in slidersRadiusBaseAttack)
            {
                var col = slid.GetComponent<Color>();
                if (col.color == gate.Color)
                {
                    gate.BaseAttackRadius = slid.value;
                }

            }

            foreach (var tcs in textCountsRadiusBaseAttack)
            {
                var col = tcs.GetComponent<Color>();
                if (col.color == gate.Color)
                {
                    tcs.text = gate.BaseAttackRadius.ToString();
                }
            }
        }

        public void ChangeHealth(ZikkuratComponent gate)
        {
            foreach (var ifbh in botsHealth)
            {
                var col = ifbh.GetComponent<Color>();
                if (col.color == gate.Color)
                {
                    gate.BaseBotHealth = int.Parse(ifbh.text);
                    
                }
            }
        }

        public void ChangeBaseDamage(ZikkuratComponent gate)
        {
            foreach (var bbd in botsBaseDamage)
            {
                var col = bbd.GetComponent<Color>();
                if (col.color == gate.Color)
                {
                   gate.BaseBaseDamage = int.Parse(bbd.text);
                }
            }
        }

        public void ChangeStrongDamage(ZikkuratComponent gate)
        { foreach (var bsd in botsStrongDamage)
            {
                var col = bsd.GetComponent<Color>();
                if (col.color == gate.Color)
                {
                    gate.BaseStrongDamage = int.Parse(bsd.text);
                }
            }
        }

        public void ChangeSpeedStrongAttack()
        {

            blueZikk.speed = speedStrongAttack.value;
            textSpeedStrongAttack.text = blueZikk.speed.ToString();
        }

        public void ChangeRadiusStrongAttack()
        {
            blueZikk.radius = radiusStrongAttack.value;
            textRadiusStrongAttack.text = blueZikk.radius.ToString();
        }

        //метод стартовой инициализации параметров и визуальной инфы на панелях

        public void StartConfig(ColorsType actColor, ZikkuratComponent gate)
        {
            //настройка скорость ботов
            foreach (var slid in slidersSpeed)
            {
                var col = slid.GetComponent<Color>();
                if (col.color == actColor)
                {
                    slid.value = gate.BaseBotSpeed;
                }

            }

            foreach (var tcs in textCountsSpeed)
            {
                var col = tcs.GetComponent<Color>();
                if (col.color == actColor)
                {
                    tcs.text = gate.BaseBotSpeed.ToString();
                }
            }

            //настройка количества здоровья
            foreach (var ifbh in botsHealth)
            {
                var col = ifbh.GetComponent<Color>();
                if (col.color == actColor)
                {
                    ifbh.text = gate.BaseBotHealth.ToString();
                }
            }


            //настройка ближнего урона
            foreach (var bbd in botsBaseDamage)
            {
                var col = bbd.GetComponent<Color>();
                if (col.color == actColor)
                {
                    bbd.text = gate.BaseBaseDamage.ToString();
                }
            }

            //настройка дальнего урона
            foreach (var bsd in botsStrongDamage)
            {
                var col = bsd.GetComponent<Color>();
                if (col.color == actColor)
                {
                    bsd.text = gate.BaseStrongDamage.ToString();
                }
            }

            //настройка скорости спавна
            foreach (var slid in slidersSpawn)
            {
                var col = slid.GetComponent<Color>();
                if (col.color == actColor)
                {
                    slid.value = gate.coolDownSpawn;
                }

            }

            foreach (var tcs in textCountsSpawn)
            {
                var col = tcs.GetComponent<Color>();
                if (col.color == actColor)
                {
                    tcs.text = gate.coolDownSpawn.ToString();
                }
            }

            //настройка скорость ближней атаки
            foreach (var slid in slidersSpeedBaseAttack)
            {
                var col = slid.GetComponent<Color>();
                if (col.color == actColor)
                {
                    slid.value = gate.BaseAttackSpeed;
                }

            }

            foreach (var tcs in textCountsSpeedBaseAttack)
            {
                var col = tcs.GetComponent<Color>();
                if (col.color == actColor)
                {
                    tcs.text = gate.BaseAttackSpeed.ToString();
                }
            }

            //настройка радиус ближней атаки
            foreach (var slid in slidersRadiusBaseAttack)
            {
                var col = slid.GetComponent<Color>();
                if (col.color == actColor)
                {
                    slid.value = gate.BaseAttackRadius;
                }

            }

            foreach (var tcs in textCountsRadiusBaseAttack)
            {
                var col = tcs.GetComponent<Color>();
                if (col.color == actColor)
                {
                    tcs.text = gate.BaseAttackRadius.ToString();
                }
            }

        }

        //и булли для открытия панелей
        public void CloseBluePanel()
        {
            IsBluePanelActiv = false;
        }

        public void CloseRedPanel()
        {
            IsRedPanelActiv = false;
        }

        public void CloseGreenPanel()
        {
            IsGreenPanelActiv = false;
        }

    }

}
