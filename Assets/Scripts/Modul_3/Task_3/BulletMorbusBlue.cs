﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace bot_morbus
{
    //скрипт для пули синих мобов
    public class BulletMorbusBlue : MonoBehaviour
    {
        public BotMorbusLongRangeAttack manager;
        public float speed = 1f;
        public BotMorbusComponent target;

        void Start()
        {
            Destroy(gameObject, 3f); //настраиваем уничтожение пуль через 3 секунды
        }

        // Update is called once per frame
        void Update()
        {
            
            if (target != null)
            {
                var myTarget = target.transform.position; //даем пуле цель и направление
                transform.LookAt(myTarget);

            }

            transform.Translate(Vector3.forward * speed * Time.deltaTime);

        }
    }

}
