﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lesson_1 : MonoBehaviour
{
    public GameObject sphere;
    public GameObject stick;
    public bool IsTrans;
    public bool IsRotate;
    public bool IsScale;
    public float timer;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        if (timer >= 1)
        {
            if (IsTrans)
            {
                transform.position = transform.position + new Vector3(0, 0, 2);
            }

            if (IsRotate)
            {
                // stick.transform.eulerAngles = stick.transform.eulerAngles + new Vector3(-5, 0, 0);
                stick.transform.Rotate(-5, 0, 0);
            }

            if (IsScale)
            {
                sphere.transform.localScale = sphere.transform.localScale + new Vector3(1.5f, 1.5f, 1.5f);
            }
            timer = 0;


        }
    }
}
