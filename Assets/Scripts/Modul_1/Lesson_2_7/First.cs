﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class First : MonoBehaviour
{

    public bool IsTrans;
    public bool IsRotate;
    public bool IsScale;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()

    {
        if(IsTrans)
        {
            transform.position = transform.position + new Vector3(0.2f, 0, 0) * Time.deltaTime;
        }

        if(IsRotate)
        {
            transform.eulerAngles = transform.eulerAngles + new Vector3(0, 2, 0) * Time.deltaTime;
        }

        if (IsScale) {
            transform.localScale = transform.localScale + new Vector3(0, 0, 0.2f) * Time.deltaTime;
        }
        
    }
}
