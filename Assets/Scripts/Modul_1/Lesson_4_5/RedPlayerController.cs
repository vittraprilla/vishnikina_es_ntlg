﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RedPlayerController : MonoBehaviour
{
    public GameObject point_1; //первая точка маршрута, игрок изначально стоит в ней
    public GameObject point_2; //вторая точка маршрута
    public GameObject point_3; //третья точка маршрута
    public GameObject point_4; //четвертая точка маршрута
    public float speed; //скорость передвижения игрока
    public float tim; //время задержки игрока на точке
    void Start()
    {
      StartCoroutine(RedGo());
    }

  

    private IEnumerator RedGo ()
    {
        while (true) { 
            //игрок движется из 1 точки в 2
        while (Vector3.Distance(transform.position, point_2.transform.position) > 0.1f) {
            transform.position = Vector3.MoveTowards(transform.position, point_2.transform.position, speed * Time.deltaTime);
           
                yield return null; 
        }
            //во 2 точке создается кубик
            var cube2 = GameObject.CreatePrimitive(PrimitiveType.Cube);
            cube2.transform.position = point_2.transform.position;
            cube2.transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);

            //задержка на 2 точке
            yield return new WaitForSeconds(tim);

            //игрок движется из 2 точки в 3
            while (Vector3.Distance(transform.position, point_3.transform.position) > 0.1f)
        {
            transform.position = Vector3.MoveTowards(transform.position, point_3.transform.position, speed * Time.deltaTime);
            yield return null;

        }
            //в 3 точке создается кубик
            var cube3 = GameObject.CreatePrimitive(PrimitiveType.Cube);
            cube3.transform.position = point_3.transform.position;
            cube3.transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);
            //задержка на 3 точке
            yield return new WaitForSeconds(tim);

            //игрок движется из 3 точки в 4
            while (Vector3.Distance(transform.position, point_4.transform.position) > 0.1f)
        {
            transform.position = Vector3.MoveTowards(transform.position, point_4.transform.position, speed * Time.deltaTime);
            yield return null;

        }
            //в 4 точке создается кубик
            var cube4 = GameObject.CreatePrimitive(PrimitiveType.Cube);
            cube4.transform.position = point_4.transform.position;
            cube4.transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);
            //задержка на 4 точке
            yield return new WaitForSeconds(tim);

            //игрок движется из 4 точки в 1
            while (Vector3.Distance(transform.position, point_1.transform.position) > 0.1f)
        {
            transform.position = Vector3.MoveTowards(transform.position, point_1.transform.position, speed * Time.deltaTime);
            yield return null;

        }
            //в 1 точке создается кубик
            var cube1 = GameObject.CreatePrimitive(PrimitiveType.Cube);
            cube1.transform.position = point_1.transform.position;
            cube1.transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);

            //задержка перед удалением всех кубиков и сменой маршрута
            yield return new WaitForSeconds(tim);

            //удаление всех кубиков
            Destroy(cube1);
            Destroy(cube2);
            Destroy(cube3);
            Destroy(cube4);



            //отсюда изменение маршрута на треугольник
            while (Vector3.Distance(transform.position, point_2.transform.position) > 0.1f)
            {
                transform.position = Vector3.MoveTowards(transform.position, point_2.transform.position, speed * Time.deltaTime);
                yield return null;

            }
            var cube2_2 = GameObject.CreatePrimitive(PrimitiveType.Cube);
            cube2_2.transform.position = point_2.transform.position;
            cube2_2.transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);
            yield return new WaitForSeconds(tim);


            while (Vector3.Distance(transform.position, point_4.transform.position) > 0.1f)
            {
                transform.position = Vector3.MoveTowards(transform.position, point_4.transform.position, speed * Time.deltaTime);
                yield return null;

            }
            var cube4_2 = GameObject.CreatePrimitive(PrimitiveType.Cube);
            cube4_2.transform.position = point_4.transform.position;
            cube4_2.transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);
            yield return new WaitForSeconds(tim);


            while (Vector3.Distance(transform.position, point_1.transform.position) > 0.1f)
            {
                transform.position = Vector3.MoveTowards(transform.position, point_1.transform.position, speed * Time.deltaTime);
                yield return null;

            }
            var cube1_2 = GameObject.CreatePrimitive(PrimitiveType.Cube);
            cube1_2.transform.position = point_1.transform.position;
            cube1_2.transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);

            yield return new WaitForSeconds(tim);
            Destroy(cube2_2);
            Destroy(cube4_2);
            Destroy(cube1_2);
            

        }

    }
    




}
