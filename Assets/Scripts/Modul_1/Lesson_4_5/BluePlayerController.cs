﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BluePlayerController : MonoBehaviour
{
    public GameObject point_1; //первая точка маршрута, игрок изначально стоит в ней
    public GameObject point_2; //вторая точка маршрута
    public GameObject point_3; //третья точка маршрута
    public GameObject point_4; //четвертая точка маршрута
    public GameObject point_5; //пятая точка маршрута
    public GameObject point_6; //шестая точка маршрута
    public float speed; //скорость передвижения игрока
    public float tim; //время задержки игрока на точке
    void Start()
    {
      StartCoroutine(BlueGo());
    }

  

    private IEnumerator BlueGo()
    {
        while (true) {

            //игрок движется из 1 точки в 3
            while (Vector3.Distance(transform.position, point_3.transform.position) > 0.1f)
            {
                transform.position = Vector3.MoveTowards(transform.position, point_3.transform.position, speed * Time.deltaTime);

                yield return null;
            }
            //во 3 точке создается кубик
            var cube3_1 = GameObject.CreatePrimitive(PrimitiveType.Cube);
            cube3_1.transform.position = point_3.transform.position;
            cube3_1.transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);

            //задержка на 3 точке
            yield return new WaitForSeconds(tim);

            //игрок движется из 3 точки в 6
            while (Vector3.Distance(transform.position, point_6.transform.position) > 0.1f)
            {
                transform.position = Vector3.MoveTowards(transform.position, point_6.transform.position, speed * Time.deltaTime);

                yield return null;
            }
            //во 6 точке создается кубик
            var cube6_1 = GameObject.CreatePrimitive(PrimitiveType.Cube);
            cube6_1.transform.position = point_6.transform.position;
            cube6_1.transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);

            //задержка на 6 точке
            yield return new WaitForSeconds(tim);

            //игрок движется из 6 точки в 2
            while (Vector3.Distance(transform.position, point_2.transform.position) > 0.1f)
            {
                transform.position = Vector3.MoveTowards(transform.position, point_2.transform.position, speed * Time.deltaTime);

                yield return null;
            }
            //во 2 точке создается кубик
            var cube2_1 = GameObject.CreatePrimitive(PrimitiveType.Cube);
            cube2_1.transform.position = point_2.transform.position;
            cube2_1.transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);

            //задержка на 2 точке
            yield return new WaitForSeconds(tim);

            //игрок движется из 2 точки в 5
            while (Vector3.Distance(transform.position, point_5.transform.position) > 0.1f)
            {
                transform.position = Vector3.MoveTowards(transform.position, point_5.transform.position, speed * Time.deltaTime);

                yield return null;
            }
            //во 5 точке создается кубик
            var cube5_1 = GameObject.CreatePrimitive(PrimitiveType.Cube);
            cube5_1.transform.position = point_5.transform.position;
            cube5_1.transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);

            //задержка на 5 точке
            yield return new WaitForSeconds(tim);

            //игрок движется из 5 точки в 1
            while (Vector3.Distance(transform.position, point_1.transform.position) > 0.1f)
            {
                transform.position = Vector3.MoveTowards(transform.position, point_1.transform.position, speed * Time.deltaTime);

                yield return null;
            }
            //во 5 точке создается кубик
            var cube1_1 = GameObject.CreatePrimitive(PrimitiveType.Cube);
            cube1_1.transform.position = point_1.transform.position;
            cube1_1.transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);

            //задержка перед удалением всех кубиков и сменой маршрута
            yield return new WaitForSeconds(tim);

            //удаление всех кубиков
            Destroy(cube1_1);
            Destroy(cube2_1);
            Destroy(cube3_1);
            Destroy(cube5_1);
            Destroy(cube6_1);

            //смена маршрута на шестиугольник


            //игрок движется из 1 точки в 2
            while (Vector3.Distance(transform.position, point_2.transform.position) > 0.1f) {
            transform.position = Vector3.MoveTowards(transform.position, point_2.transform.position, speed * Time.deltaTime);
           
                yield return null; 
        }
            //во 2 точке создается кубик
            var cube2 = GameObject.CreatePrimitive(PrimitiveType.Cube);
            cube2.transform.position = point_2.transform.position;
            cube2.transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);

            //задержка на 2 точке
            yield return new WaitForSeconds(tim);

            //игрок движется из 2 точки в 3
            while (Vector3.Distance(transform.position, point_3.transform.position) > 0.1f)
        {
            transform.position = Vector3.MoveTowards(transform.position, point_3.transform.position, speed * Time.deltaTime);
            yield return null;

        }
            //в 3 точке создается кубик
            var cube3 = GameObject.CreatePrimitive(PrimitiveType.Cube);
            cube3.transform.position = point_3.transform.position;
            cube3.transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);
            //задержка на 3 точке
            yield return new WaitForSeconds(tim);

            //игрок движется из 3 точки в 4
            while (Vector3.Distance(transform.position, point_4.transform.position) > 0.1f)
        {
            transform.position = Vector3.MoveTowards(transform.position, point_4.transform.position, speed * Time.deltaTime);
            yield return null;

        }
            //в 4 точке создается кубик
            var cube4 = GameObject.CreatePrimitive(PrimitiveType.Cube);
            cube4.transform.position = point_4.transform.position;
            cube4.transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);
            //задержка на 4 точке
            yield return new WaitForSeconds(tim);

            //игрок движется из 4 точки в 5
            while (Vector3.Distance(transform.position, point_5.transform.position) > 0.1f)
        {
            transform.position = Vector3.MoveTowards(transform.position, point_5.transform.position, speed * Time.deltaTime);
            yield return null;

        }
            //в 5 точке создается кубик
            var cube5 = GameObject.CreatePrimitive(PrimitiveType.Cube);
            cube5.transform.position = point_5.transform.position;
            cube5.transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);

            //задержка на 5 точке
            yield return new WaitForSeconds(tim);

            //игрок движется из 5 точки в 6
            while (Vector3.Distance(transform.position, point_6.transform.position) > 0.1f)
            {
                transform.position = Vector3.MoveTowards(transform.position, point_6.transform.position, speed * Time.deltaTime);
                yield return null;

            }
            //в 6 точке создается кубик
            var cube6 = GameObject.CreatePrimitive(PrimitiveType.Cube);
            cube6.transform.position = point_6.transform.position;
            cube6.transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);

            //задержка на 6 точке
            yield return new WaitForSeconds(tim);

            //игрок движется из 6 точки в 1
            while (Vector3.Distance(transform.position, point_1.transform.position) > 0.1f)
            {
                transform.position = Vector3.MoveTowards(transform.position, point_1.transform.position, speed * Time.deltaTime);
                yield return null;

            }
            //в 1 точке создается кубик
            var cube1 = GameObject.CreatePrimitive(PrimitiveType.Cube);
            cube1.transform.position = point_1.transform.position;
            cube1.transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);


            //задержка перед удалением всех кубиков и сменой маршрута
            yield return new WaitForSeconds(tim);

            //удаление всех кубиков
            Destroy(cube1);
            Destroy(cube2);
            Destroy(cube3);
            Destroy(cube4);
            Destroy(cube5);
            Destroy(cube6);


        }


    }
    




}
