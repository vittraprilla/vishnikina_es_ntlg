﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using static UnityEngine.InputSystem.InputAction;

public class NewInputController : MonoBehaviour
{
    public Player_NewInput_Controller controller;
    public InputAction Text;
    public GameObject bullet;
    public float time;
   public float jumpSpeed = 2f;
    public bool IsGrounded = true;
    public Rigidbody rigi;
    public Collider plane;

    private void Awake()
    {
        controller = new Player_NewInput_Controller();
    }

    private void OnEnable()
    {
        controller.ActionFite.Enable();
        controller.ActionFite.Messagess.performed += OnMessage;
        controller.ActionFite.Fite.performed += OnFite;
       controller.ActionFite.Jump.performed += OnJump;

    }
    public void OnFite(CallbackContext context)
    {
        if (time < 0f) {
            Instantiate(bullet, transform.position, transform.rotation);
            time = 1f;
        }
        
    }

    //тут по сути повтор прыжка из контроллера старой системы, но я не могу использовать WaitForSeconds, другой код я не смогла придумать:(( поэтому этот закомментирую

    public void OnJump(CallbackContext context) 
     {
         if(IsGrounded == true)
        {
            IsGrounded = false;
            rigi.AddForce(Vector3.up * jumpSpeed, ForceMode.Impulse);
            Debug.Log("Jump");
        }
       
        
     }

   /* private void OnCollisionEnter(Collision collision)
    {
       if (plane.gameObject.name == "Plane")
        {
            if (IsGrounded != true) return;
            IsGrounded = true;
        }
        
    } */


    void OnTriggerEnter(Collider boxCollider)
    {
        if (boxCollider.name == "Plane")
        {
           // if (IsGrounded != true) return;
            IsGrounded = true;
        }
    }

    public void OnMessage(CallbackContext context)
    {
       Debug.Log("OnMessage");
    }

 
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        time -= Time.deltaTime;

    }

    private void OnDisable()
    {
        controller.ActionFite.Disable();
    }
}
