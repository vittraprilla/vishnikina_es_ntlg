﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Move_Controller : MonoBehaviour
{
    public float speed = 5f;
    public float speedRotation = 1f;
  
    

    void Start()
    {
        StartCoroutine(Go());
        
    }

 

    private IEnumerator Go()
    {
        while (true)
        {

        
        //движение
        if (Input.GetKey(KeyCode.A))
        {
                transform.Translate(Vector3.left * speed * Time.deltaTime);

            }
        if (Input.GetKey(KeyCode.D))
        {
                transform.Translate(Vector3.right * speed * Time.deltaTime);
            }


        if (Input.GetKey(KeyCode.S))
        {
                transform.Translate(-Vector3.forward * speed * Time.deltaTime);
            }

        if (Input.GetKey(KeyCode.W))
        {
            transform.Translate (Vector3.forward  * speed * Time.deltaTime);
        }

        //поворот
        if (Input.GetKey(KeyCode.Q) || Input.GetKey(KeyCode.LeftArrow))
        {
           transform.Rotate(Vector3.down * speedRotation * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.E))
        {
            transform.Rotate(Vector3.up * speedRotation * Time.deltaTime);
        }

     

        yield return null;

        }
    }

}
