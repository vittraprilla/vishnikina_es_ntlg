﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.UI;

namespace Runner
{
    public class GameManager : MonoBehaviour
    {
        public static GameManager Manager;
        private int _currentIndex;
        private int _progress;


        private BasePlayerContoller _basePlayerContoller;

        [Space, SerializeField]
        private Transform[] _blocks;

        [SerializeField]
        private float _shift = 14f;


        [SerializeField]
        private int _countIndex = 2;

        [Range(1f, 25f)]
        public int Health = 2;

        public Slider slideHp;
        public int timeInt = 60;
        public float timer = 60;
        public Text timeTim;

        [SerializeField]
        private Text _text;

        private void Awake()
        {
            Manager = this;
        }

        private void Start()
        {
            _basePlayerContoller = FindObjectOfType<BasePlayerContoller>();
            slideHp.value = Health;
        }

        private void Update()
        {
            if(_basePlayerContoller.transform.position.y < -5f) SetDamage();

            timer += Time.deltaTime;
            if (true && timer >= 1)
            {
                timeTim.text = timeInt.ToString();
                timeInt -= 1;
                timer = 0;

            }

            //КОММЕНТИРУЮ ДЛЯ БИЛДА
            //else if (timeInt <= 0)
            //{
            //    EditorApplication.isPaused = true;
            //}
        }
        public void SetDamage ()
        {
            Health --;
            slideHp.value = Health;
            //КОММЕНТИРУЮ ДЛЯ БИЛДА
            //if (Health <= 0)
            //{
            //    EditorApplication.isPaused = true;
            //}
        }

        public void UpDateLevel()
        {
            _countIndex++;
            _progress++;
            var posZ = _countIndex * _shift;
            var position = _blocks[_currentIndex].position;
            position.z = posZ;
            _blocks[_currentIndex].position = position;
            _currentIndex++;

            if (_currentIndex >= _blocks.Length) _currentIndex = 0;
            _text.text = _progress.ToString();
        }
    }
}

