﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Runner
{
    public class NewPlayerController : BasePlayerContoller
    {
        private Controls _controls;
       

        protected override void Start()
        {
            _controls = new Controls();
            _controls.Player.Enable();
            _controls.Player.Jump.performed += _ => OnJump();

            base.Start();
        }

        private void FixedUpdate()
        {
            OnMove(_controls.Player.Movement.ReadValue<float>());
        }
    }
}

