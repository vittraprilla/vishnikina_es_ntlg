﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Runner
{
    public class TriggerComponent : MonoBehaviour
    {
        [SerializeField]
        private bool _isDamage;
        private void OnTriggerEnter(Collider other)

        {
            var player = other.GetComponent<BasePlayerContoller>(); 

            if(player != null)
            {
                if (_isDamage)
                {
                    GameManager.Manager.SetDamage();
                }
                else
                {
                    GameManager.Manager.UpDateLevel();
                }
            }
            
        }
    }
}

