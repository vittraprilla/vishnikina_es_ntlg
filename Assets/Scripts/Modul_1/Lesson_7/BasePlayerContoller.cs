﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Runner
{
    [RequireComponent(typeof(Rigidbody))]
    public abstract class BasePlayerContoller : MonoBehaviour
    {
        protected Rigidbody _rigidbody;
        private bool _canJump;

        [SerializeField, Range(2f,25f), Tooltip("Сила прыжка")]
        private float _forseJump = 2f;

        [SerializeField, Range(2f, 25f), Tooltip("Скорость перемещения")]
        private float _moveSpeed = 2f;

        [SerializeField, Range(2f, 25f), Tooltip("Скорость движения вперед")]
        private float _forwardSpeed = 2f;

        [Space, SerializeField, Range(1f, 25f), Tooltip("Ограничение скорости")]
        private float _maxForwardSpeed = 5f;

        protected virtual void Start()
        {
            _rigidbody = GetComponent<Rigidbody>();
            StartCoroutine(MoveForward());
        }

        
        protected void OnJump()
        {
            if(!_canJump) return;

            _canJump = false;
            _rigidbody.AddForce(transform.up * _forseJump, ForceMode.Impulse);


        }

       
        protected  void OnMove( float axis)
        {
           
            var vector = transform.right * axis * _moveSpeed * Time.fixedDeltaTime;
            _rigidbody.velocity += vector;
        }

        private void OnCollisionEnter(Collision collision)
        {
            _canJump = true;
        }

        private IEnumerator MoveForward()
        {
            while (true)
            {
                var velocity = _rigidbody.velocity + transform.forward * _forwardSpeed * Time.fixedDeltaTime;

                velocity.z = Mathf.Clamp(velocity.z, 0f, _maxForwardSpeed);

                _rigidbody.velocity = velocity;

                yield return new WaitForFixedUpdate();

            }
        }
    }
}

