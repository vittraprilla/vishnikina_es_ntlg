﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public GameObject enemy;
    public float _timer;
    void Start()
    {
        StartCoroutine(Spawner());
    }



    IEnumerator Spawner()
    {
        while (true)
        {
            Instantiate(enemy, transform.position, transform.rotation);
            yield return new WaitForSeconds(_timer);
        }
       
        
    }
    void Update()
    {
        
    }
}
