﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace Cars
{
    public class MenuRace : MonoBehaviour
    {
        [SerializeField] private GameObject _panelSet; //панель с таблицей результатов

        //имена игроков
        [SerializeField] private Text[] _names; //имена и время предыдущих заездов

        //время заезда игроков на эране
        [SerializeField] private Text[] _playersTimes;

        private void Start()
        {
            Time.timeScale = 1;
            LoadRes(); //загружаем в таблицу результатов предыдущие заезды, если они были

        }
        //метод загрузки результатов
        private void LoadRes()
        {
            if (PlayerPrefs.HasKey("Игрок1"))
            {
                var name = PlayerPrefs.GetString("Игрок1", "name");
                var min = PlayerPrefs.GetInt("Игрок1Минуты", 0);
                var sec = PlayerPrefs.GetInt("Игрок1Секунды", 0);
                _names[0].text = name;
                _playersTimes[0].text = min.ToString("D2") + " : " + sec.ToString("D2");

            }

            if (PlayerPrefs.HasKey("Игрок2"))
            {
                var name = PlayerPrefs.GetString("Игрок2", "name");
                var min = PlayerPrefs.GetInt("Игрок2Минуты", 0);
                var sec = PlayerPrefs.GetInt("Игрок2Секунды", 0);
                _names[1].text = name;
                _playersTimes[1].text = min.ToString("D2") + " : " + sec.ToString("D2");
            }

            if (PlayerPrefs.HasKey("Игрок3"))
            {
                var name = PlayerPrefs.GetString("Игрок3", "name");
                var min = PlayerPrefs.GetInt("Игрок3Минуты", 0);
                var sec = PlayerPrefs.GetInt("Игрок3Секунды", 0);
                _names[2].text = name;
                _playersTimes[2].text = min.ToString("D2") + " : " + sec.ToString("D2");
            }

            if (PlayerPrefs.HasKey("Игрок4"))
            {
                var name = PlayerPrefs.GetString("Игрок4", "name");
                var min = PlayerPrefs.GetInt("Игрок4Минуты", 0);
                var sec = PlayerPrefs.GetInt("Игрок4Секунды", 0);
                _names[3].text = name;
                _playersTimes[3].text = min.ToString("D2") + " : " + sec.ToString("D2");
            }

            if (PlayerPrefs.HasKey("Игрок5"))
            {
                var name = PlayerPrefs.GetString("Игрок5", "name");
                var min = PlayerPrefs.GetInt("Игрок5Минуты", 0);
                var sec = PlayerPrefs.GetInt("Игрок5Секунды", 0);
                _names[4].text = name;
                _playersTimes[4].text = min.ToString("D2") + " : " + sec.ToString("D2");
            }
        }

        //кнопки перехода в другие сцены и выход
        public void StartRace()
        {
            SceneManager.LoadScene("Arcade_Race");
        }

        public void GoTuning()
        {
            SceneManager.LoadScene("Arcade_Race_Tuning");
        }

        public void ExitButton()
        {
            UnityEditor.EditorApplication.isPlaying = false;
        }

        //закрытие таблицы с результатами
        public void ClosePanelButt()
        {
            var anim = _panelSet.GetComponent<Animation>(); //включаем анимацию закрытия панели
            anim.Play("PanelSetCloseAnim");
            StartCoroutine(ClosePanel()); //стартуем корутину закрытия панели
        }

    
        IEnumerator ClosePanel() //закрываем панель
        {
            yield return new WaitForSeconds(1f);
            _panelSet.gameObject.SetActive(false);

        }
    }

}
