﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Cars
{
    public class FinishManager : MonoBehaviour
    {
        private int sec = 0; //для подсчета времени заезда
        private int min = 0;
     [SerializeField] private Text timerText;
        private int delta = 0;

        [SerializeField] private GameObject[] _effects; //эффекты для финала
        [SerializeField] private InputField _playerName; //поле ввода имени игрока
        [SerializeField] private string _name;
        [SerializeField] private GameObject _buttonNameOk;
       [SerializeField] private GameObject _playerNamePanel;
        [SerializeField] private WinnersCountManager _winners;  //ссылка на менеджера результатов заездов

        void Start()
        {
            StartCoroutine(TimeFlow());
        }

       IEnumerator TimeFlow()  //таймер времени гонки
        {
            while (true)
            {
                if (sec == 59)
                {
                    min++;
                    sec = -1;
                }
                sec += delta;
                timerText.text = min.ToString("D2") + " : " + sec.ToString("D2");
                yield return new WaitForSeconds(1);
            }
        }

        public void StartStop(int delta)
        {
            this.delta = delta;
        }


        public void Finish()
        {
            StartCoroutine(FinishEffect()); //запускаем эффекты, если игрок добрался до финиша
            _playerNamePanel.SetActive(true);

        }

        private IEnumerator FinishEffect()
        {
            yield return new WaitForSeconds(0.5f);
            _effects[0].SetActive(true);
            _effects[1].SetActive(true);

            yield return new WaitForSeconds(0.5f);
            _effects[2].SetActive(true);
            _effects[3].SetActive(true);

            yield return new WaitForSeconds(0.5f);
            _effects[4].SetActive(true);
            _effects[5].SetActive(true);

        }

        public void EnterName()
        {
            _name = _playerName.text; //вводим имя игрока и запоминаем
            _buttonNameOk.SetActive(true);
        }

        public void CloseNameShowWinners() 
        {
             _winners.LoadRes(); //загружаем результаты предыдущих четырех заездов, если они были
            StartCoroutine(RefreshRes());
        }

        private IEnumerator RefreshRes()
        {
            yield return new WaitForSeconds(0.5f);
            _winners.Show(_name, min, sec); //добавляем в таблицу новый результат
        }
    }

}
