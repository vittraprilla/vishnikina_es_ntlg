﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Cars
{
    //скрипт со стрима, ничего не меняла тут
    public class Speedometer : MonoBehaviour
    {
        private const float _c_convertMeterInSecFromKmInH = 3.6f;
        private CarComponent _car;

        [SerializeField] private Text _text;
        [SerializeField] private float _minSpeed = 0f;
        [SerializeField] private float _maxSpeed = 300f;
        [SerializeField] private Color _minColor;
        [SerializeField] private Color _maxColor;
        [SerializeField, Range (0.1f, 1f)] private float _delay = 0.3f;
      private  void Start()
        {
            if (PlayerPrefs.HasKey("MaxSpeedValue"))
            {
                _maxSpeed = PlayerPrefs.GetFloat("MaxSpeedValue", 0);
            }

            _car = FindObjectOfType<CarComponent>();
            StartCoroutine(Speed());

           
        }

       private IEnumerator Speed()
        {
            var prevPos = _car.transform.position;

            while (true)
            {
                var distance = Vector3.Distance(_car.transform.position, prevPos);
                var speed = (float)System.Math.Round(distance / _delay * _c_convertMeterInSecFromKmInH, 0);
                _text.color = Color.Lerp(_minColor, _maxColor, speed / _maxSpeed);
                _text.text = speed.ToString();
                prevPos = _car.transform.position;

                yield return new WaitForSeconds(_delay);
            }
        }
    }

}
