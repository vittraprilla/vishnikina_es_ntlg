﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Cars
{
    public class CarComponent : MonoBehaviour
    {
        private BaseInputComponent _input;
        private WheelsComponent _wheels;
        private Rigidbody _rigidbody;
        [SerializeField] private FinishManager _finMan; //ссылка на менеджер финиша

        [SerializeField, Range(5f, 40f)] private float _maxSteerAngle = 25f;
        [SerializeField] private float _torque = 2500f;
        [SerializeField] private float _handBrakeForse = float.MaxValue;
        [SerializeField] private Vector3 _centerOfMass;
        [SerializeField] private Vector3 _centerOfMassRear; //добавила альтернативный центр масс

        private void FixedUpdate()
        {
            OnDrive();
            OnRotate();
        }

        private void OnDrive()
        {
            var torque = _input.Acceleration * _torque / 2f;
            foreach (var wheel in _wheels.GetFrontWheels) wheel.motorTorque = torque;
        }

        private void OnRotate()
        {
            _wheels.UpdateVisual(_input.Rotate * _maxSteerAngle);
        }
        private void Start()
        {
            _input = GetComponent<BaseInputComponent>();
            _input.OnHandleBrake += HandBrake;
            _wheels = GetComponent<WheelsComponent>();
           _rigidbody = GetComponent<Rigidbody>();

            if (PlayerPrefs.HasKey("CenterRear")) //тут проставляется центр масс в зависимости от настроек в тюнинге
            {
                _rigidbody.centerOfMass = _centerOfMassRear;
                Debug.Log("CenterRear");
            }

            else if (!PlayerPrefs.HasKey("CenterRear"))
            {
                _rigidbody.centerOfMass = _centerOfMass;
                Debug.Log("CenterFront");
            }
            

            //////и перенос других настроек из тюнинга
            if (PlayerPrefs.HasKey("MaxSteerAngleValue"))
            {
                _maxSteerAngle = PlayerPrefs.GetFloat("MaxSteerAngleValue", 0);
            }

            if (PlayerPrefs.HasKey("TorqueValue"))
            {
                _torque = PlayerPrefs.GetFloat("TorqueValue", 0);
            }

           

        }

        private void HandBrake(object sender, bool value)
        {
            if (value)
            {
                foreach(var wheel in _wheels.GetRearWheels)
                {
                    wheel.brakeTorque = _handBrakeForse;
                    wheel.motorTorque = 0f; //todo
                }
            }

            else
            {
                foreach(var wheel in _wheels.GetRearWheels)
                {
                    wheel.brakeTorque = 0f;
                }
            }
        }

        private void OnDestroy()
        {
            _input.OnHandleBrake -= HandBrake;
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.yellow;
            Gizmos.DrawSphere(transform.TransformPoint(_centerOfMass), 0.5f);

            Gizmos.color = Color.green;
            Gizmos.DrawSphere(transform.TransformPoint(_centerOfMassRear), 0.5f);

        }

        private void OnTriggerEnter(Collider other)
        {
            if(other.name == "Start")
            {

                _finMan.StartStop(1); //триггер для начала подсчета времени гонки
                Debug.Log("Start");
               


            }
        }
    }

  

}
