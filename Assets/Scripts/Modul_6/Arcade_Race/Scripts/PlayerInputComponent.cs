﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Cars
{
    public class PlayerInputComponent : BaseInputComponent
    {
        private CarsControl _control;
     [SerializeField]   private FinishManager _finishManager;

        protected override void FixedUpdate()
        {
           
                var direction = _control.Player.Rotate.ReadValue<float>();
                if (direction == 0f && Rotate != 0f)
                {
                    Rotate = Rotate > 0f
                        ? Rotate - Time.fixedDeltaTime
                        : Rotate + Time.fixedDeltaTime;
                }

                Rotate = Mathf.Clamp(Rotate + direction * Time.fixedDeltaTime, -1f, 1f);
                Acceleration = _control.Player.Acceleration.ReadValue<float>();
           
           
        }


        protected override void Start()
        {
            base.Start();

            
            _control.Player.HandBrake.performed += (q) => CallHandBrake(true);
            _control.Player.HandBrake.canceled += (q) => CallHandBrake(false);
        }

        private void Awake()
        {
            _control = new CarsControl();
        }

        private void OnEnable()
        {
            StartCoroutine(StartGo());
        }

        private IEnumerator StartGo() //при старте включаем управление игрока только после стартового таймера
        {
            yield return new WaitForSeconds(5f);
            _control.Player.Enable();
        }

        private void OnTriggerEnter(Collider other) //при взаимодействии с триггером финиша выключаем управление игроку
        {
            var fin = other.GetComponent<FinishManager>();
            if(fin != null)
            {
                _control.Player.Disable();
                _finishManager.StartStop(0); //останавливаем время заезда
                _finishManager.Finish(); //включаем финальную логику
            }
        }
        private void OnDisable()
        {
            _control.Player.Disable();
        }

        private void OnDestroy()
        {
            _control.Dispose();
        }
    }
}

