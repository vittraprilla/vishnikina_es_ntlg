﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace Cars
{
    public class LoadTuningSettings : MonoBehaviour
    {
        [SerializeField] private MeshRenderer _carColor; //для передачи настроек цвета
        [SerializeField] private Material[] _colors;
        [SerializeField] private Text _text;
        private int _index;
        private int _timerCount = 5; //для таймера старта игры
        private string empt = "";
        private bool IsTextEmpt;
        [SerializeField] private Animator _animator;
        private bool IsPause;
        [SerializeField] private GameObject _panelPause; //панель паузы


        [SerializeField] private Rigidbody _carMass;
        void Start()
        {
            Time.timeScale = 1;
            if (PlayerPrefs.HasKey("Цвет машины")) //при старте проставляем цвет машины, надо было бы это вынести в отдельный метод, но уже не буду
            {
                _index = PlayerPrefs.GetInt("Цвет машины", 0);

                if(_index == 0)
                {
                    _carColor.material = _colors[0];
                }

               else if (_index == 1)
                {
                    _carColor.material = _colors[1];
                }
                else if (_index == 2)
                {
                    _carColor.material = _colors[2];
                }
                else if (_index == 3)
                {
                    _carColor.material = _colors[3];
                }
            }

            if (PlayerPrefs.HasKey("MassValue"))
            {
                _carMass.mass = PlayerPrefs.GetFloat("MassValue", 0);
            }

            StartCoroutine(TimerCorut());
        }


        private void Update()
        {
            if(_timerCount == 0 && IsTextEmpt == false)
            {
                StartCoroutine(DelText());
            }

            //включение паузы
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                if (IsPause == false)
                { //если буль ложный, запускаем корутину включения паузы
                    StartCoroutine(OpenPanel());
                }

                else
                { //если пауза запущена, возвращаем время и запускаем корутину выключения паузы
                    Time.timeScale = 1;
                    StartCoroutine(ClosePanel());

                }
            }
        }

        //корутина открытия меню паузы
        IEnumerator OpenPanel()
        {
            yield return new WaitForSeconds(0.2f);
            _panelPause.gameObject.SetActive(true); //открываем панель с кнопками
            StartCoroutine(TimeStop()); //стартуем корутину выключения времени

        }

        IEnumerator TimeStop() //корутина остановки времени
        {
            yield return new WaitForSeconds(0.5f);
            IsPause = true;
            Time.timeScale = 0;

        }


        //корутина закрытия меню-паузы
        IEnumerator ClosePanel()
        {
            yield return new WaitForSeconds(0.2f);
            Time.timeScale = 1; //возвращаем время
            var anim = _panelPause.GetComponent<Animation>(); //проигрываем анимацию закрытия
            anim.Play("PanelSetCloseAnim");
            StartCoroutine(ClosePanelAfterAnim()); //запускаем корутину с закрытием панели

        }

        //корутина для закрытия панели с меню, когда анимация отыгралась
        IEnumerator ClosePanelAfterAnim()
        {
            yield return new WaitForSeconds(0.5f);
            _panelPause.gameObject.SetActive(false);
            IsPause = false;
        }


        private IEnumerator TimerCorut() //таймер старта игры
        {
            var time = 5;
            while (time != 0)
            {
                _text.text = time.ToString();
                time--;
                _timerCount--;
                yield return new WaitForSeconds(0.5f);
                _animator.SetTrigger("TimeAnim");
                yield return new WaitForSeconds(0.5f);
            }
        }

        private IEnumerator DelText() 
        {
            yield return new WaitForSeconds(1f);
            _text.text = empt;
            IsTextEmpt = true;
        }

        //кнопки из меню паузы
        public void GoMenu()
        {
            SceneManager.LoadScene("Arcade_Race_Menu");
        }

        public void GoTuning()
        {
            SceneManager.LoadScene("Arcade_Race_Tuning");
        }

        public void Restart()
        {
            SceneManager.LoadScene("Arcade_Race");
        }
    }
}

