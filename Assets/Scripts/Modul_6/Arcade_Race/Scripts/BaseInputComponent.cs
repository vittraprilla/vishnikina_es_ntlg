﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Cars
{  //этот скрипт из стрима преподователя, тут ничего не трогала
    public abstract class BaseInputComponent : MonoBehaviour
    {
      public float Acceleration { get; protected set; }
        //отн. ед. от -1 до 1

        public float Rotate { get; protected set; }

        public event EventHandler<bool> OnHandleBrake;

        protected abstract void FixedUpdate();
        protected virtual void Start() { }

        protected void CallHandBrake(bool value) => OnHandleBrake?.Invoke(null, value);

    }

}
