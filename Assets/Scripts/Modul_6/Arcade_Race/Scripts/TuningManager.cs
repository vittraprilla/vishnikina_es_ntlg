﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
namespace Cars
{

    //скрипт тюнинга машины
    public class TuningManager : MonoBehaviour
    {
        //настройки цвета машина
        [SerializeField]  private MeshRenderer _carColor;
      [SerializeField] private Material[] _colors;
        [SerializeField] private int _index;

        //настройки массы машины
        [SerializeField] private float _mass;
        [SerializeField] private Slider _massSlider;
        [SerializeField] private Text _massText;

        //настройки поворота колеса
        [SerializeField] private float _maxSteerAngle;
        [SerializeField] private Slider _maxSteerAngleSlider;
        [SerializeField] private Text _maxSteerAngleText;

        //настройки силы двигателя, не знаю,как лучше этот параметр обозвать)
        [SerializeField] private float _torque;
        [SerializeField] private Slider _torqueSlider;
        [SerializeField] private Text _torqueText;

        //ну это не настоящая максимальная скорость, но все равно добавила настройку
        [SerializeField] private float _maxSpeed;
        [SerializeField] private Slider _maxSpeedSlider;
        [SerializeField] private Text _maxSpeedText;

        [SerializeField] private Toggle _massCenterRear; //настройки центра массы, рекомендую ездить с передним)))
        [SerializeField] private Toggle _massCenterFront;

        [SerializeField] private AudioClip[] _clips; //музыка

        [SerializeField] private GameObject ActPos; //и векторы, скорость и буль для выезжания панели
        [SerializeField] private GameObject DesActPos;
        [SerializeField] private GameObject _panelSettings; //панели настроек
        [SerializeField] private GameObject _panelMus;
        [SerializeField] private GameObject _panelColor;

        [SerializeField] private GameObject _butColor; //кнопки настроек
        [SerializeField] private GameObject _butSet;
        [SerializeField] private GameObject _butMus;


        public float smoothing; //скорость и були для выезжания панелек
        private bool IsOpenpanel;
        private bool IspanelSettings;
        private bool IspanelMus;
        private bool IspanelColor;

        private void Start()
        {
            Time.timeScale = 1;
            LoadingSettings(); //загружаем сохраненные настройки, если они есть
        }

        private void Update()
        {
            if (IsOpenpanel == true && IspanelSettings == true) //открытие и закрытие панели настроек
            {
                _panelSettings.transform.position = Vector3.Lerp(_panelSettings.transform.position, ActPos.transform.position, smoothing * Time.deltaTime);
            }

            else if (IsOpenpanel == false && IspanelSettings == false)
            {
                _panelSettings.transform.position = Vector3.Lerp(_panelSettings.transform.position, DesActPos.transform.position, smoothing * Time.deltaTime);
            }



            if (IsOpenpanel == true && IspanelMus == true) //открытие и закрытие панели с выбором музыки
            {
                _panelMus.transform.position = Vector3.Lerp(_panelMus.transform.position, ActPos.transform.position, smoothing * Time.deltaTime);
            }

            else if (IsOpenpanel == false && IspanelMus == false)
            {
                _panelMus.transform.position = Vector3.Lerp(_panelMus.transform.position, DesActPos.transform.position, smoothing * Time.deltaTime);
            }



            if (IsOpenpanel == true && IspanelColor == true) //открытие и закрытие панели с цветом
            {
                _panelColor.transform.position = Vector3.Lerp(_panelColor.transform.position, ActPos.transform.position, smoothing * Time.deltaTime);
            }

            else if (IsOpenpanel == false && IspanelColor == false)
            {
                _panelColor.transform.position = Vector3.Lerp(_panelColor.transform.position, DesActPos.transform.position, smoothing * Time.deltaTime);
            }

        }

        //три метода нажатия на кнопки панелей
        public void OpenPanelSettings()
        {
            IsOpenpanel = !IsOpenpanel;
            IspanelSettings = !IspanelSettings;

            if (IspanelSettings == true)
            {
                _butMus.SetActive (false);
                _butColor.SetActive(false);
             }
            else
            {
                _butMus.SetActive(true);
                _butColor.SetActive(true);
            }
        }

        public void OpenPanelMus()
        {
            IsOpenpanel = !IsOpenpanel;
            IspanelMus = !IspanelMus;

            if (IspanelMus == true)
            {
                _butSet.SetActive(false);
                _butColor.SetActive(false);
            }
            else
            {
                _butSet.SetActive(true);
                _butColor.SetActive(true);
            }
        }

        public void OpenPanelColor()
        {
            IsOpenpanel = !IsOpenpanel;
            IspanelColor = !IspanelColor;

            if (IspanelColor == true)
            {
                _butSet.SetActive(false);
                _butMus.SetActive(false);
            }
            else
            {
                _butSet.SetActive(true);
                _butMus.SetActive(true);
            }
        }

        //дальше методы изменения настроек
        public void ChangeMass()
        {
            float i = (int)_massSlider.value;
            _massText.text = i.ToString();
        }

        public void ChangeMaxSteerAngle()
        {
            float i = (int)_maxSteerAngleSlider.value;
            _maxSteerAngleText.text = i.ToString();
        }

        public void ChangeTorque()
        {
            float i = (int)_torqueSlider.value;
            _torqueText.text = i.ToString();
        }

        public void ChangeMaxSpeed()
        {
            float i = (int)_maxSpeedSlider.value;
            _maxSpeedText.text = i.ToString();
        }

        public void ChooseBlack()
        {
            _carColor.material = _colors[0];
            _index = 0;
            PlayerPrefs.SetInt("Цвет машины", _index);
            PlayerPrefs.Save();
        }

        public void ChooseRed()
        {
            _carColor.material = _colors[2];
            _index = 2;
            PlayerPrefs.SetInt("Цвет машины", _index);
            PlayerPrefs.Save();
        }

        public void ChooseBlue()
        {
            _carColor.material = _colors[1];
            _index = 1;
            PlayerPrefs.SetInt("Цвет машины", _index);
            PlayerPrefs.Save();
        }

        public void ChooseYellow()
        {
            _index = 3;
            _carColor.material = _colors[3];
            PlayerPrefs.SetInt("Цвет машины", _index);
            PlayerPrefs.Save();
        }

        //сохраняем настройки при выходе в меню или гонку
        public void SaveSettings()
        {
            //Сохраняем массу
            _mass = _massSlider.value; //сохраняем
            PlayerPrefs.SetFloat("MassValue", _mass);
            PlayerPrefs.Save();

            //Сохраняем силу двигателя
            _torque = _torqueSlider.value; //сохраняем
            PlayerPrefs.SetFloat("TorqueValue", _torque);
            PlayerPrefs.Save();

            //Сохраняем максимальный поворот колес
            _maxSteerAngle = _maxSteerAngleSlider.value; //сохраняем
            PlayerPrefs.SetFloat("MaxSteerAngleValue", _maxSteerAngle);
            PlayerPrefs.Save();

            //Сохраняем максимальную скорость
            _maxSpeed = _maxSpeedSlider.value; //сохраняем
            PlayerPrefs.SetFloat("MaxSpeedValue", _maxSpeed);
            PlayerPrefs.Save();

            if (_massCenterRear.isOn == false && _massCenterFront.isOn == true) //сохраняем
            {
                if (PlayerPrefs.HasKey("CenterRear"))
                {
                    PlayerPrefs.DeleteKey("CenterRear");
                }
                
            }

            else if (_massCenterFront.isOn == false && _massCenterRear.isOn == true)
            {
                PlayerPrefs.SetInt("CenterRear", 1);
                PlayerPrefs.Save();
            }

         
        }

        public void ToggleFront()
        {
            if(_massCenterFront.isOn == true)
            {
                _massCenterRear.isOn = false;
            }

            else if(_massCenterFront.isOn == false)
            {
                _massCenterRear.isOn = true;
            }
        }

        public void ToggleRear()
        {
            if (_massCenterRear.isOn == true)
            {
                _massCenterFront.isOn = false;
            }

            else if (_massCenterRear.isOn == false)
            {
                _massCenterFront.isOn = true;
            }
        }

        //загрузка настроек
        public void LoadingSettings()
        {
            //Loading color

            if (PlayerPrefs.HasKey("Цвет машины"))
            {
                _index = PlayerPrefs.GetInt("Цвет машины", 0);

                if (_index == 0)
                {
                    _carColor.material = _colors[0];
                }

                else if (_index == 1)
                {
                    _carColor.material = _colors[1];
                }
                else if (_index == 2)
                {
                    _carColor.material = _colors[2];
                }
                else if (_index == 3)
                {
                    _carColor.material = _colors[3];
                }
            }


            if (PlayerPrefs.HasKey("MassValue"))
            {
                _massSlider.value = PlayerPrefs.GetFloat("MassValue", 0);
            }

            if (PlayerPrefs.HasKey("MaxSteerAngleValue"))
            {
                _maxSteerAngleSlider.value = PlayerPrefs.GetFloat("MaxSteerAngleValue", 0);
            }

            if (PlayerPrefs.HasKey("TorqueValue"))
            {
                _torqueSlider.value = PlayerPrefs.GetFloat("TorqueValue", 0);
            }


            if (PlayerPrefs.HasKey("MaxSpeedValue"))
            {
                _maxSpeedSlider.value = PlayerPrefs.GetFloat("MaxSpeedValue", 0);
            }

            if (PlayerPrefs.HasKey("CenterRear"))
            {
                _massCenterRear.isOn = true;
                _massCenterFront.isOn = false;
            }

            else if (!PlayerPrefs.HasKey("CenterRear"))
            {
                _massCenterRear.isOn = false;
                _massCenterFront.isOn = true;
            }
        }

        //кнопки смены музыки
        public void ChooseOneClip()
        {
            GameObject mus = GameObject.FindGameObjectWithTag("Musik");
            var audio = mus.GetComponent<AudioSource>();
            audio.clip = _clips[0];
            audio.Play();

        }

        public void ChooseTwoClip()
        {
            GameObject mus = GameObject.FindGameObjectWithTag("Musik");
            var audio = mus.GetComponent<AudioSource>();
            audio.clip = _clips[1];
            audio.Play();

        }

        public void ChooseThreeClip()
        {
            GameObject mus = GameObject.FindGameObjectWithTag("Musik");
            var audio = mus.GetComponent<AudioSource>();
            audio.clip = _clips[2];
            audio.Play();

        }

        //кнопки выхода со сцены
        public void GoMenu()
        {
            SaveSettings();
            SceneManager.LoadScene("Arcade_Race_Menu");
        }

        public void StartRace()
        {
            SaveSettings();
            SceneManager.LoadScene("Arcade_Race");
        }

    }
}

