﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Cars
{
    public class WinnersCountManager : MonoBehaviour
    {
        //имена игроков
      [SerializeField] private Text[] _names;

        //время заезда игроков на эране
        [SerializeField] private Text [] _playersTimes;
         [SerializeField] private GameObject _panel;
        [SerializeField] private int _countPlayers;

        //метод добавления нового результата в таблицу
        public void Show(string name, int min, int sec)
        {
            _panel.SetActive(true);

            if(!PlayerPrefs.HasKey("Игрок1"))
            {
                _names[0].text = name;
                _playersTimes[0].text = min.ToString("D2") + " : " + sec.ToString("D2");
                _countPlayers = 1;
                PlayerPrefs.SetInt("Игроки", _countPlayers);
                PlayerPrefs.SetString("Игрок1", name);
                PlayerPrefs.SetInt("Игрок1Минуты", min);
                PlayerPrefs.SetInt("Игрок1Секунды", sec);
                PlayerPrefs.Save();
            }

           else if (!PlayerPrefs.HasKey("Игрок2"))
            {
                _names[1].text = name;
                _playersTimes[1].text = min.ToString("D2") + " : " + sec.ToString("D2");
                _countPlayers = 2;
                PlayerPrefs.SetInt("Игроки", _countPlayers);
                PlayerPrefs.SetString("Игрок2", name);
                PlayerPrefs.SetInt("Игрок2Минуты", min);
                PlayerPrefs.SetInt("Игрок2Секунды", sec);
                PlayerPrefs.Save();
            }

            else if (!PlayerPrefs.HasKey("Игрок3"))
            {
                _names[2].text = name;
                _playersTimes[2].text = min.ToString("D2") + " : " + sec.ToString("D2");
                _countPlayers = 3;
                PlayerPrefs.SetInt("Игроки", _countPlayers);
                PlayerPrefs.SetString("Игрок3", name);
                PlayerPrefs.SetInt("Игрок3Минуты", min);
                PlayerPrefs.SetInt("Игрок3Секунды", sec);
                PlayerPrefs.Save();
            }

            else if (!PlayerPrefs.HasKey("Игрок4"))
            {
                _names[3].text = name;
                _playersTimes[3].text = min.ToString("D2") + " : " + sec.ToString("D2");
                _countPlayers = 4;
                PlayerPrefs.SetInt("Игроки", _countPlayers);
                PlayerPrefs.SetString("Игрок4", name);
                PlayerPrefs.SetInt("Игрок4Минуты", min);
                PlayerPrefs.SetInt("Игрок4Секунды", sec);
                PlayerPrefs.Save();
            }
            else if (!PlayerPrefs.HasKey("Игрок5"))
            {
                _names[4].text = name;
                _playersTimes[4].text = min.ToString("D2") + " : " + sec.ToString("D2");
                _countPlayers = 5;
                PlayerPrefs.SetInt("Игроки", _countPlayers);
                PlayerPrefs.SetString("Игрок5", name);
                PlayerPrefs.SetInt("Игрок5Минуты", min);
                PlayerPrefs.SetInt("Игрок5Секунды", sec);
                PlayerPrefs.Save();
            }

            else
            {
            }

        }

        //загрузка предыдущих результатов в таблицу
        public void LoadRes()
        {
            _countPlayers = PlayerPrefs.GetInt("Игроки", 0);

            if(_countPlayers == 5)
            {
                PlayerPrefs.DeleteKey("Игрок1");
                PlayerPrefs.DeleteKey("Игрок1Минуты");
                PlayerPrefs.DeleteKey("Игрок1Секунды");

                PlayerPrefs.DeleteKey("Игрок2");
                PlayerPrefs.DeleteKey("Игрок2Минуты");
                PlayerPrefs.DeleteKey("Игрок2Секунды");

                PlayerPrefs.DeleteKey("Игрок3");
                PlayerPrefs.DeleteKey("Игрок3Минуты");
                PlayerPrefs.DeleteKey("Игрок3Секунды");

                PlayerPrefs.DeleteKey("Игрок4");
                PlayerPrefs.DeleteKey("Игрок4Минуты");
                PlayerPrefs.DeleteKey("Игрок4Секунды");

                PlayerPrefs.DeleteKey("Игрок5");
                PlayerPrefs.DeleteKey("Игрок5Минуты");
                PlayerPrefs.DeleteKey("Игрок5Секунды");

                _countPlayers = 0;
                PlayerPrefs.SetInt("Игроки", _countPlayers);
                PlayerPrefs.Save();
            }

            else
            {
                if (PlayerPrefs.HasKey("Игрок1"))
                {
                    var name = PlayerPrefs.GetString("Игрок1", "name");
                    var min = PlayerPrefs.GetInt("Игрок1Минуты", 0);
                    var sec = PlayerPrefs.GetInt("Игрок1Секунды", 0);
                    _names[0].text = name;
                    _playersTimes[0].text = min.ToString("D2") + " : " + sec.ToString("D2");

                }

                if (PlayerPrefs.HasKey("Игрок2"))
                {
                    var name = PlayerPrefs.GetString("Игрок2", "name");
                    var min = PlayerPrefs.GetInt("Игрок2Минуты", 0);
                    var sec = PlayerPrefs.GetInt("Игрок2Секунды", 0);
                    _names[1].text = name;
                    _playersTimes[1].text = min.ToString("D2") + " : " + sec.ToString("D2");
                }

                if (PlayerPrefs.HasKey("Игрок3"))
                {
                    var name = PlayerPrefs.GetString("Игрок3", "name");
                    var min = PlayerPrefs.GetInt("Игрок3Минуты", 0);
                    var sec = PlayerPrefs.GetInt("Игрок3Секунды", 0);
                    _names[2].text = name;
                    _playersTimes[2].text = min.ToString("D2") + " : " + sec.ToString("D2");
                }

                if (PlayerPrefs.HasKey("Игрок4"))
                {
                    var name = PlayerPrefs.GetString("Игрок4", "name");
                    var min = PlayerPrefs.GetInt("Игрок4Минуты", 0);
                    var sec = PlayerPrefs.GetInt("Игрок4Секунды", 0);
                    _names[3].text = name;
                    _playersTimes[3].text = min.ToString("D2") + " : " + sec.ToString("D2");
                }
            }
           

        }


       
    }

}
