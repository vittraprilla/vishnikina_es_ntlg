﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;

namespace Checkers
{
    public class BaseChips : MonoBehaviour
    {
        [SerializeField]
        protected TypeColor _color;
        public event FocusEventHandler OnFocusEventHandler;
        [SerializeField]
        private InputAction _input;
        private float speed = 10f;


        // Start is called before the first frame update
        void Start()
        {
            _input.Enable();
            _input.performed += _ => OnClick();

        }

        private void OnClick()
        {
            foreach( var item in GameGo.cells)
            {
                if( item.transform.position != transform.position)
                {
                    transform.Translate(Vector3.forward * speed * Time.deltaTime);
                }
            }
        }
        // Update is called once per frame
        void Update()
        {

        }
    }

    public enum TypeColor : byte
    {
        black,
        white
    }
}
