﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
namespace Checkers
{
    public class ManagerGame : MonoBehaviour
    {

        [SerializeField] private List<ChipComponent> _chips = new List<ChipComponent>();//все фишки
        [SerializeField] private List<ChipComponent> _chipsDeath = new List<ChipComponent>(); //лист для удаления фишек
        [SerializeField] private List<CellComponent> _cells = new List<CellComponent>(); //все клетки
        [SerializeField] private List <ChipComponent> _chipsWhite = new List<ChipComponent>(); //белые фишки
        [SerializeField] private List<ChipComponent> _chipsBlack = new List<ChipComponent>(); //темные фишки

        //два материала для выделения клеток и фишек
        [SerializeField] private Material _selected; 
        [SerializeField] private Material _choseMat;
        private bool IsSelectedChip; //буль для выбора фишки
        [SerializeField] private GameObject _camera; //ссылка на камеру
        private bool _isWhiteTurn; //буль для организации хода игроков



        void Start()
        {
           

            _isWhiteTurn = true; //проставляем буль - ход белых

            _chipsDeath = _chips; //кладем в коллекцию для смерти фишек все фишки
            foreach (var item in _cells) //идем по клеткам
            {
                //здесь мы берем у каждого итема событие OnFocusEventHandler и подписываем его на обработчика события - метод HandleFocus
                //обработчик события должен соотвествовать по списку параметров и возвращаемому типу делегату события
                item.OnFocusEventHandler += HandleFocus; //подписываем клетки на события
                item.OnClickEventHandler += HandleClick;
                var neighbors = new Dictionary<NeighborType, CellComponent>(); //заводим словарь для соседей
                foreach (var cell in _cells)
                {
                    if (cell == item)
                    {
                        continue;
                    }

                    //заполняем словарь соседями 
                    //тип соседства - в зависимости от координат соседей
                    if (cell.transform.position == item.transform.position + new Vector3(x: 10, 0, z: 10))

                    {  
                        //TopLeft
                        neighbors.Add(NeighborType.TopLeft, cell);
                   }

                    //BottomLeft
                    else if (cell.transform.position == item.transform.position + new Vector3(x: -10, 0, z: 10))
                    {
                        neighbors.Add(NeighborType.BottomLeft, cell);
                    }

                    //TopRight
                    else if (cell.transform.position == item.transform.position + new Vector3(x: 10, 0, z: -10))
                    {
                        neighbors.Add(NeighborType.TopRight, cell);
                    }

                    //BottomRight
                    else if (cell.transform.position == item.transform.position + new Vector3(x: -10, 0, z: -10))
                    {
                        neighbors.Add(NeighborType.BottomRight, cell);
                    }



                }
                item.Configuration(neighbors); //передаем словарь в клетку
            }

            foreach (var item in _chips) //подписываем фишки на событие клика
            {
                //item.OnFocusEventHandler += HandleFocus;
                item.OnClickEventHandler += HandleClick;

            }

            GetPair(); //вызываем метод выбора пары
        }

        //в апдейте проверяем количество белых и темных фишек на поле
        //если все фишки одного цвета будут съедены - конец игры
        private void Update()
        {
            if(_chipsBlack.Count == 0)
            {
                Debug.Log("Белые победили");
            }

         else   if (_chipsWhite.Count == 0)
            {
                Debug.Log("Черные победили");
            }
        }

        //метод посвечивания клеток на поле
        //честно говоря, не понимаю, зачем это нужно
        //есть ли смысл подсвечивать любую клетку на поле?
        private void HandleFocus(CellComponent component, bool isSelect)
        {
            if (isSelect)
                component.AddAdditionalMaterial(_selected);
            else
                component.RemoveAdditionalMaterial();
        }


        private void GetPair() /*метод поиска пары*/
        {
            //ищу пару к фишкам
            foreach (var chip in _chips)
            {

                CellComponent myCell = null; //назначаю пустышку-клетку
               
                var minDistance = 1f; //примерная минимальная дистанция между фишкой и клеткой

                foreach (var cell in _cells) //иду по клеткам
                {
                    //проверяю дистанцию
                    var distance = Vector3.Distance(cell.transform.position, chip.transform.position);
                    if (minDistance > distance)
                    {
                        if (cell.Pair == null) //если свойство пары клетки равно 0
                        {
                            minDistance = distance;
                            myCell = cell; //в переменную кладу клетку
                            cell.Pair = chip; //клетке назначаю свойство соседа
                           break; //если пара клетки найдена, то выхожу из цикла
                        }
                    }
                }
                chip.Pair = myCell; //назначаю соседа фишке

            }



        }

        //общий метод клинка
        private void HandleClick(BaseClickComponent component)
        {
            //при клике на объект определяю, кто он. если он фишка
            if (component is ChipComponent chip)
            {
                HandleClick(chip); //иду в метод клика на фишку
            }

            //иду в другой метод, если кликаю на клетку
            else if (component is CellComponent cell)
            {
                HandleClick(cell);
            }

           
            _chips = _chipsDeath; //актуализирую количество фишек на поле
        }

        //метод нажатия на фишку

        //метод клика на фишку
        private void HandleClick(ChipComponent chip)

        {
            if (_isWhiteTurn == false) //проверка хода, если ходят черные
            {
                if (chip.GetColor == ColorType.Black) //проверка цвета фишки
                {
                    if (chip.isSelect == false && IsSelectedChip == false) //если ни одна фишка не выбрана
                    {
                        chip.AddAdditionalMaterial(_choseMat); //выделяю эту фишку
                        chip.isSelect = true; //проставляю були выбора
                        IsSelectedChip = true;
                        //начинаю проверять соседей клетки, на которой стоит фишка
                        //мне нужны нижние - левые и правые
                        CellComponent pair = (CellComponent)chip.Pair;
                        var leftBot = pair.GetNeighbors(NeighborType.BottomLeft);
                        //если у нее есть левый нижний сосед
                        if (leftBot != null)
                        {
                            if(leftBot.Pair == null) //если левая соседка пустая
                            {
                                leftBot.AddAdditionalMaterial(_choseMat); //выделяю ее
                            }
                            else if(leftBot.Pair.GetColor == ColorType.White) //если не пустая, и на ней стоит фишка-враг
                            {
                                var newLeftBot = leftBot.GetNeighbors(NeighborType.BottomLeft); //проверяю левую соседку левой соседки
                                //если левая соседка левой соседки существует и она пустая, без фишки
                                if (newLeftBot != null && newLeftBot.Pair == null)
                                {
                                    leftBot.AddAdditionalMaterial(_choseMat); //выделяю клетку
                                }
                            }
          
                        }

                        //дальше я делаю такую же проверку для правой клетки-соседа
                        //дубликат кода, только для правого соседа
                        var rightBot = pair.GetNeighbors(NeighborType.BottomRight);

                        if (rightBot != null)
                        {
                            if (rightBot.Pair == null)
                            {
                                rightBot.AddAdditionalMaterial(_choseMat);
                            }
                            else if (rightBot.Pair.GetColor == ColorType.White)
                            {
                                var newRightBot = rightBot.GetNeighbors(NeighborType.BottomRight);
                                
                                if (newRightBot != null && newRightBot.Pair == null)
                                {
                                    rightBot.AddAdditionalMaterial(_choseMat);
                                }
                            }
                        }

                    }

                    //если фишка уже выбрана
                    else if (chip.isSelect == true)
                    {
                       
                        DontSelectChip(chip); //метод снятия выделения для повторного клика на фишку

                    }
                }

                else
                {
                    Debug.Log("Сейчас ходят черные!");
                } //если в ход кликнули на чужую фишку
      
            }

            //дальше эта логика повторяется для белых фишек
            //только тут проверяем верхних соседей, а не нижних
            else if (_isWhiteTurn == true)
            {
                if(chip.GetColor == ColorType.White)
                {
                    if(chip.isSelect == false && IsSelectedChip == false)
                    {
                        //добавляю ей материал выделения
                        chip.AddAdditionalMaterial(_choseMat);
                        chip.isSelect = true;
                        IsSelectedChip = true;
                        //беру ее пару
                        CellComponent pair = (CellComponent)chip.Pair;
                        //начинаю проверять соседей. Это белая фишка, поэтому мне
                        //нужны только ее верхние соседи
                        // проверяю, есть ли у нее верхний левый сосед
                        var leftBot = pair.GetNeighbors(NeighborType.TopLeft);

                        //и дальше то же самое, что и вверху для фишки черного цвета
                        //дубликат кода
                        if (leftBot != null)
                        {
                            if (leftBot.Pair == null)
                            {
                                leftBot.AddAdditionalMaterial(_choseMat);
                            }
                            else if (leftBot.Pair.GetColor == ColorType.Black)
                            {
                                var newLeftBot = leftBot.GetNeighbors(NeighborType.TopLeft);
                                //если левая соседка левой соседки существует и она пустая, без фишки
                                if (newLeftBot != null && newLeftBot.Pair == null)
                                {
                                    leftBot.AddAdditionalMaterial(_choseMat);
                                }
                            }
                        }

                        //и здесь такой же дубликат для правого соседа клетки белой фишки
                        var rightBot = pair.GetNeighbors(NeighborType.TopRight);

                        if (rightBot != null)
                        {
                            if (rightBot.Pair == null)
                            {
                                rightBot.AddAdditionalMaterial(_choseMat);
                            }
                            else if (rightBot.Pair.GetColor == ColorType.Black)
                            {
                                var newRightBot = rightBot.GetNeighbors(NeighborType.TopRight);
                                //если левая соседка левой соседки существует и она пустая, без фишки
                                if (newRightBot != null && newRightBot.Pair == null)
                                {
                                    rightBot.AddAdditionalMaterial(_choseMat);
                                }
                            }
                        }

                    }

                    else if (chip.isSelect == true)
                    {
                        

                        DontSelectChip(chip);

                    }
                } 

                else
                {
                    Debug.Log("Сейчас ходят белые!");
                }

            } 
          
        } 


        //метод клика на клетку

        private void HandleClick(CellComponent clickedCell)
        {
            //проверяю, выбрана ли у меня фишка для хода
            if (IsSelectedChip == true)
            {
                //если выбрана, ищу в листе выбранную фишку
                foreach (var chip in _chips)
                {
                    //проверяю, что она выбрана
                    if (chip.isSelect == true)
                    {

                        //ДАЛЬШЕ ЛОГИКА ХОДА И ПОЕДАНИЯ ВРАГОВ У ТЕМНЫХ ФИШЕК

                        if (chip.GetColor == ColorType.Black)
                        {
                            //берем клетку, на которой стоит фишка для хода
                            CellComponent chipPair = (CellComponent)chip.Pair;

                            //если фишка темная,проверяю соседей ее клетки
                            //сначала левую соседку
                            var leftBot = chipPair.GetNeighbors(NeighborType.BottomLeft);

                            //если у пары фишки есть левая соседка, и она наша клетка
                            //на которую мы нажали
                            if (leftBot != null && leftBot == clickedCell)
                            {
                                
                                //проверяем, есть ли на ней вражеская фишка
                                //если нет, перемещаем на нее нашу фишку
                                if (leftBot.Pair == null)
                                {
                                    
                                    Move(chip, chipPair, clickedCell); //вызываем метод перемещения
                                    _isWhiteTurn = true; //передаем ход второму игроку
                                    DontSelectColor(chipPair); //снимаем цвет выделения
                                    DontSelectChip(chip); //убираем выбор с фишки
                                    break; //выхожу из цикла, так как все готово
                                }
                                //если вражеская фишка есть
                                else if (leftBot.Pair.GetColor == ColorType.White)
                                {
                                    //тогда лезем проверять левую соседку левой соседки
                                    var newLeftBot = leftBot.GetNeighbors(NeighborType.BottomLeft);
                                    //если левая соседка левой соседки существует и она пустая, без фишки
                                    if (newLeftBot != null && newLeftBot.Pair == null)
                                    {
                                        EatEnemy(clickedCell, chip, chipPair, newLeftBot); //вызываем метод поедания врага
                                        _isWhiteTurn = true;
                                        DontSelectColor(chipPair);
                                        DontSelectChip(chip);
                                        break;
                                    }
                                }
                            }
                            //ДАЛЬШЕ ПОВТОРЕНИЕ ЭТОЙ ЛОГИКИ ДЛЯ ПРАВОЙ СОСЕДКИ-КЛЕТКИ
                            var rightBot = chipPair.GetNeighbors(NeighborType.BottomRight);


                            if (rightBot != null && rightBot == clickedCell)
                            {

                                if (rightBot.Pair == null)
                                {
                                     Move(chip, chipPair, clickedCell);
                                    DontSelectColor(chipPair);
                                    DontSelectChip(chip);
                                    _isWhiteTurn = true;
                                    break;
                                }

                                else if (rightBot.Pair.GetColor == ColorType.White)
                                {
                                    var newRightBot = rightBot.GetNeighbors(NeighborType.BottomRight);

                                    if (newRightBot != null && newRightBot.Pair == null)
                                    {
                                        EatEnemy(clickedCell, chip, chipPair, newRightBot);
                                        DontSelectColor(chipPair);
                                        DontSelectChip(chip);
                                        _isWhiteTurn = true;
                                        break;
                                    }
                                }
                            }
                        }

                        //И ДАЛЬШЕ ТОЖЕ ПОВТОРЕНИЕ ТОЛЬКО ДЛЯ ФИШЕК БЕЛОГО ЦВЕТА

                        else if (chip.GetColor == ColorType.White)
                        {
                            CellComponent chipPair = (CellComponent)chip.Pair;
                            var leftBot = chipPair.GetNeighbors(NeighborType.TopLeft);

                            if (leftBot != null && leftBot == clickedCell)
                            {
                                if (leftBot.Pair == null)
                                { 
                                   Move(chip, chipPair, clickedCell);
                                 DontSelectColor(chipPair);
                                    _isWhiteTurn = false;
                                    DontSelectChip(chip);
                                    break; //выхожу из цикла, так как все готово
                                }
                                else if (leftBot.Pair.GetColor == ColorType.Black)
                                {
                                    //тогда лезем проверять левую соседку левой соседки
                                    var newLeftBot = leftBot.GetNeighbors(NeighborType.TopLeft);
                                    //если левая соседка левой соседки существует и она пустая, без фишки
                                    if (newLeftBot != null && newLeftBot.Pair == null)
                                    {
                                        EatEnemy(clickedCell, chip, chipPair, newLeftBot);
                                        DontSelectColor(chipPair);
                                        _isWhiteTurn = false;
                                        DontSelectChip(chip);
                                        break;
                                    }
                                }
                            }

                            var rightBot = chipPair.GetNeighbors(NeighborType.TopRight);

                            if (rightBot != null && rightBot == clickedCell)
                            {

                                if (rightBot.Pair == null)
                                {
                                     Move(chip, chipPair, clickedCell);
                                    DontSelectColor(chipPair);
                                    DontSelectChip(chip);
                                    _isWhiteTurn = false;
                                    break;
                                }
                                else if (rightBot.Pair.GetColor == ColorType.Black)
                                {
                                    var newRightBot = rightBot.GetNeighbors(NeighborType.TopRight);

                                    if (newRightBot != null && newRightBot.Pair == null)
                                    {
                                        EatEnemy(clickedCell, chip, chipPair, newRightBot);
                                        DontSelectColor(chipPair);
                                        DontSelectChip(chip);
                                        _isWhiteTurn = false;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }






        //метод поедания фишки врага
        private void EatEnemy(CellComponent cell, ChipComponent chip, CellComponent from, CellComponent to)
        {
            //кидаем в переменную враг вражескую шашку, которая стоит на выбранной клетке                                            
            var enem = cell.Pair.GetComponent<ChipComponent>();
            _chipsDeath.Remove(enem); //удаляю фишку из списка

            //мы перемещаем фишку на левую соседку левой соседки от клетки, на которой стоит фишка 
            Move(chip, from, to);
            cell.Pair = null; //обнуляем пару выбранной клетки, на которой мы съели врага

            Destroy(enem.gameObject); //удаляю фишку со сцены

            //удаляю фишку из списка ее цвета
            if (enem.GetColor == ColorType.Black)
            {
                _chipsBlack.Remove(enem);

            }

            else if(enem.GetColor == ColorType.White)
            {
                _chipsWhite.Remove(enem);
               
            }
            
        }

        //метод движения фишки
        private void Move(ChipComponent chip, CellComponent from, CellComponent to)
        {
            
            from.Pair = null; //обнуляем свойство пары для клетки, с которой фишка уходит
            chip.Pair = to; //фишке выставляю новую пару - эта клетка
            to.Pair = chip; //клетке выставляю новую пару - эта фишка
            StartCoroutine(ChipAndCameraGo(chip, to)); //вызываю корутину с движением фишки
            StartCoroutine(CameraMove()); //стартую корутину с движением камеры

        }

        //корутина движения
        private IEnumerator ChipAndCameraGo(ChipComponent chip, CellComponent to)
        {
            var time = 0f;
            while (time < 1f)
            {
                chip.transform.position = Vector3.Lerp(chip.transform.position, to.transform.position, time);
                
                time += Time.deltaTime;
                yield return null;
            }

        }

        //удаление выделения клеток цветом
        private void DontSelectColor(CellComponent pair)
        {
            foreach (NeighborType neighborType in ColorType.GetValues(typeof(NeighborType)))
            {
                var neighbor = pair.GetNeighbors(neighborType);
                if (neighbor != null)
                {
                    neighbor.RemoveAdditionalMaterial();
                }
            }

          

        }
        //метод снятия выбора с фишки
        private void DontSelectChip(ChipComponent chip)
        {
             
                chip.RemoveAdditionalMaterial();
                chip.isSelect = false;
                IsSelectedChip = false;
                CellComponent pair = (CellComponent)chip.Pair;
                 DontSelectColor(pair);

            
        }

        //метод движения камеры
        IEnumerator CameraMove()
        {
            yield return new WaitForSeconds(2f);

            if(_isWhiteTurn == false)
            {
                var anim = _camera.GetComponent<Animation>(); //сделала через анимацию, т.к. не смогла придумать иначе
                 anim.Play("CameraAnimBlack");
                
            }

            else if( _isWhiteTurn == true)
            {
                 var anim = _camera.GetComponent<Animation>();
                anim.Play("CameraAnimWhite");
              
            }


        } 

    }


} 

