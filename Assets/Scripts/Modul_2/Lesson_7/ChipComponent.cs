﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Checkers
{
    public class ChipComponent : BaseClickComponent
    {
        public bool isSelect = false;
        protected override void Start()
        {
            base.Start();
           

            //ищем клетку под шашкой - кидаем рейкаст вниз и смотрим, есть ли там CellComponent
            //если есть, то это наша начальная пара.

            /* RaycastHit hit;
             if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.down), out hit))
             {
                 var comp = hit.collider.GetComponent<CellComponent>();
                 if (comp != null)
                 {
                     Pair = comp;

                 }
                 else
                 {

                 }
             }
             */
        }


      

        //дебажная отрисовка рейкаста для наглядности (красная палка вниз от шашки)
        private void OnDrawGizmos()
        {
            Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.down) * 10, Color.red);
        }

        public override void OnPointerEnter(PointerEventData eventData)
        {
            CallBackEvent((CellComponent)Pair, true);
        }

        public override void OnPointerExit(PointerEventData eventData)
        {
            CallBackEvent((CellComponent)Pair, false);
        }

        //метод для реализации победы, если доходим до конца поля противника
        private void OnTriggerEnter(Collider other)
        {

            //проверяем имя коллайдера и цвет фишки
            if (other.name == "TriggerBlackWin" && this.GetColor == ColorType.Black)
            {
                    Debug.Log("Черные победили!");
                
            }

            else if(other.name == "TriggerWhiteWin" && this.GetColor == ColorType.White)
            {
                Debug.Log("Белые победили!");

            }

        }
    }
}
