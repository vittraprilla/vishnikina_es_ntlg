﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace rpg_prototype

{
    public class BulletBase : MonoBehaviour

    //базовый класс снарядов
    {
    public ProjectileType projectileType; //тип снаряда
    public ProjectileData data; //ссылка на структуру снаряда
    public Transform target; //ссылка на цель для снаряда


        void Start()
    {
            //на старте при создании снаряда определяю его тип и проставляю его свойства

            if (projectileType == ProjectileType.fire)

            {

                data = new ProjectileData ( weaponType: ProjectileType.fire,  damage: 2 );
            }


          else  if (projectileType == ProjectileType.freeze)

            {
                data = new ProjectileData(weaponType: ProjectileType.freeze);
            }

         else   if (projectileType == ProjectileType.poison)

            {
                data = new ProjectileData( weaponType: ProjectileType.poison,  moveSpeed: 7);
            }

      else  if (projectileType == ProjectileType.bullPlayer)

            {
                data = new ProjectileData(weaponType: ProjectileType.bullPlayer, moveSpeed: 7, damage: 2);
            }

            else
            {
                Debug.Log("нЕ СМОГЛО");
            }

            
        }


        //метод поведения снаряда - скорость и направление его движения
        protected virtual  void Update()
        {
            var speed = data.MoveSpeed;
          
            transform.LookAt(target);
            transform.Translate(Vector3.forward * speed * Time.deltaTime);

        }



    }
   
}
