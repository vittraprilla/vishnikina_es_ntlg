﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace rpg_prototype
{
    public class BulletPlayer : BulletBase

    //производный класс снаряда для снаряда игрока
    {
        protected  override void Update() //перегружаю метод, чтобы убрать у снаряда игрока цель
        {
            var speed = data.MoveSpeed;
            transform.Translate(Vector3.forward * speed * Time.deltaTime);
        }


    }

}
