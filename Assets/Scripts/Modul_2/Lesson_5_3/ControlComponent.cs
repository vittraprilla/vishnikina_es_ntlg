﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;
using Random = UnityEngine.Random;
using System.Threading;
using System.Threading.Tasks;
using System.IO;

namespace rpg_prototype
{
    //Основной скрипт, который контролирует весь игровой процесс
    public class ControlComponent : MonoBehaviour
    {
        public Transform[] SpawnPoints; //завожу массив точек спавна для врагов
        public List <Enemy> FirstWave = new List<Enemy>(); //завожу лист для всех типов врагов
        public List<BulletBase> bulletsList = new List<BulletBase>(); //завожу лист для всех типов снарядов
        public List<Enemy> aliveEnemies = new List<Enemy>(); //завожу лист для подсчета здоровья врагов
        public List<BulletBase> bulletsAlive = new List<BulletBase>(); //завожу лист для снарядов, которые на сцене
        public GameObject player; //ссылка на игрока
        private bool ice_cooldown = false; //колдауны для стрельбы врагов
        private bool fire_cooldown = false;
        private bool poison_cooldown = false;
        private float timer = 0; //таймер для подсчета жизни снарядов 
        [SerializeField]
        private Text countText; //текст для подсчета убитых врагов
        public static int count; //количество убитых врагов
        private string path; //добавляю для задания на сборку билда ссылки на строку и путь к файлу
        private string str;

        void Start()
        {
            Time.timeScale = 1;
            countText.text = count.ToString();
            str = DateTime.Now.ToString(); //сохраняю время старта игры

            foreach ( var point in SpawnPoints) //в начале прохожусь по точкам спавна и спавню на них рандомных врагов из списка
            {

                int rand = Random.Range(0, FirstWave.Count);
                var enemy = Instantiate(FirstWave[rand], point.position, point.rotation);
                 enemy.contrCom = this; //даю врагу ссылку на управляющий компонент
                aliveEnemies.Add(enemy); //каждого врага добавляю в лист здоровья врагов 

            }
  
        }
        //колдауны стрельбы врагов
        IEnumerator Cooldown_ice()
        {
            yield return new WaitForSeconds(1f);
            ice_cooldown = false;
          
        }

        IEnumerator Cooldown_fire()
        {
            yield return new WaitForSeconds(1f);
            fire_cooldown = false;

        }


        IEnumerator Cooldown_poison()
        {
            yield return new WaitForSeconds(1f);
            poison_cooldown = false;

        }

        void Update()
        {
            //логика стрельбы врагов

            var clonAlEn = new List<Enemy>(aliveEnemies);

            foreach (var enemy in clonAlEn) //прохожусь по списку живых врагов
            {
                //высчитываю дистанцию между врагом и игроком
                float distPlayer = Vector3.Distance(enemy.transform.position, player.transform.position);

                if (enemy.data != null) //проверяю валидность ссылки
                {
                    if (distPlayer <= enemy.data.EnemyRadiusAttack)
                    {


                        //если игрок в радиусе, создаю снаряд
                        if (enemy.EnemyType == EnemyTypes.asteroid && ice_cooldown == false)
                        {
                            var bullet = Instantiate(bulletsList[0], enemy.transform.position, enemy.transform.rotation);
                            bullet.target = player.transform; //ссылка на игрока-цель
                            bulletsAlive.Add(bullet); //добавляю пулю в список пуль на сцене
                            ice_cooldown = true;
                            StartCoroutine(Cooldown_ice());
                        }

                        else if (enemy.EnemyType == EnemyTypes.robot && fire_cooldown == false)
                        {
                            var bullet = Instantiate(bulletsList[1], enemy.transform.position, enemy.transform.rotation);
                            bullet.target = player.transform;
                            bulletsAlive.Add(bullet);
                            fire_cooldown = true;
                            StartCoroutine(Cooldown_fire());

                        }

                        else if (enemy.EnemyType == EnemyTypes.alien && poison_cooldown == false)
                        {
                            var bullet = Instantiate(bulletsList[2], enemy.transform.position, enemy.transform.rotation);
                            bullet.target = player.transform;
                            bulletsAlive.Add(bullet);
                            poison_cooldown = true;
                            StartCoroutine(Cooldown_poison());
                        }

                        else
                        {

                        }

                    }
                    //контроль за здоровьем врагов и спавном новых

                    if (enemy.data.EnemyHealth <= 0) //если нахожу врага с нулевым здоровьем
                    {
                        int rand = Random.Range(0, FirstWave.Count); //спавню нового и добавляю его в список
                        var newenemy = Instantiate(FirstWave[rand], enemy.gameObject.transform.position, enemy.gameObject.transform.rotation);
                        newenemy.contrCom = this;
                        aliveEnemies.Add(newenemy);
                        aliveEnemies.Remove(enemy); //удаляю погибшего врага из списка
                        Destroy(enemy.gameObject); //удаляю самого врага со сцены
                        count++; //добавляю его в счетчик убитых врагов
                        countText.text = count.ToString();

                    }
            }

                else
            {
                Debug.Log("Пока нет ссылки на дату врага");
            }

        }

            //слежу за временем жизни снарядов
            timer += Time.deltaTime; //запускаю таймер
            if (timer >= 1)

            { if (bulletsAlive.Count > 0)
                {
                    var cloneBulAlive = new List<BulletBase>(bulletsAlive);

                    foreach (var bull in cloneBulAlive)
                    {
                        if (bull.data.LifeTime <= 0)
                        {
                            bulletsAlive.Remove(bull); //удаляю снаряд, если время жизни меньше 0
                            Destroy(bull.gameObject);
                        }

                        else
                        {
                            bull.data.LifeTime--; //если больше нуля, минусую на единицу
                        }

                    }
                }
         
             timer = 0;

            }


            if (Input.GetKey(KeyCode.Escape))
            {
                ReturnToMenu();
            }
        }

        //метод уничтожения пуль, если они попали в цель
        public void DestroyBullets(BulletBase bullet)
        {
            
                    bulletsAlive.Remove(bullet); //удаляю пулю из списка
                    Destroy(bullet.gameObject); //удаляю пулю со сцены
        }

        //метод создания снаряда игрока
        public void CreatePlayerBullet()
        {
           var bullet = Instantiate(bulletsList[3], player.transform.position, player.transform.rotation);
           bulletsAlive.Add(bullet);

        }

      public void ReturnToMenu()
        {
            //тут создание файла с временем конца и начала игры
            path = Environment.CurrentDirectory + "//GameFile.txt";
            var file = File.Create(path);
            var stream = new StreamWriter(file);
            var str1 = DateTime.Now.ToString();
            stream.WriteLine("Start Game: " + str);
            stream.WriteLine("End Game: " + str1);
            stream.Close();
            file.Close();

            Time.timeScale = 1;

            SceneManager.LoadScene("Modul4_Task1_Menu_Lesson_5_3");
        }
    }
}

