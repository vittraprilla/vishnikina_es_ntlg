﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace rpg_prototype
{
    //снаряд решила сделать структурой
    public struct ProjectileData
    {
        public int Damage; //урон снаряда
        public float MoveSpeed; //скорость снаряда
        public float LifeTime; //время жизни снаряда
     

        public ProjectileType _type;


        public ProjectileData(ProjectileType weaponType, int damage = 1, float moveSpeed = 5f, float lifeTime = 5f)
        {
            Damage = damage; //урон снаряда
            MoveSpeed = moveSpeed; //скорость снаряда
            LifeTime = lifeTime; //время жизни снаряда
            
            _type = weaponType;
        }

       


       
    }
}

