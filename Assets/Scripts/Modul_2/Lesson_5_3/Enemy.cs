﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace rpg_prototype
{
    public class Enemy : MonoBehaviour
    {
        //создаю базовый класс врага

      public EnemyTypes EnemyType; //помещаю в него ссылку на перечисления типов врагов
      public EnemyData data; //ссылка на класс с базовыми параметрами, будет иницилизироваться при старте игры
      public ControlComponent contrCom; //ссылка на управляющий компонент
      public TMP_Text textHP; //ссылка на текст здоровья врагов, который показывается на сцене

        public void Start()
        {
            //при старте проверяю, какой тип был присвоен конкретному врагу
            if (EnemyType == EnemyTypes.alien) 
            {
                //исходя из типа создаю экземпляр класса с параметрами конкретного типа врага
                data = new EnemyData(enemyType: EnemyTypes.alien, weaponType: ProjectileType.poison, enemySpeedMove: 8) ;

            }

            else if (EnemyType == EnemyTypes.asteroid)
            {
                data = new EnemyData(enemyType: EnemyTypes.asteroid, weaponType: ProjectileType.freeze, enemyRadiusAttack:10);

            }

            else if (EnemyType == EnemyTypes.robot)
            {
                data = new EnemyData(enemyType: EnemyTypes.robot, weaponType:ProjectileType.fire, enemySpeedAttack:7);
            }

            else
            {
                Debug.Log("нЕ СМОГЛО");
            }

            textHP.text = data.EnemyHealth.ToString();
        }


        public void Update()
        {
            

            

           
        }


       public virtual void Fight()
        {

        }


       
    }
}


