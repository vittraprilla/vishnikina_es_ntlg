﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace rpg_prototype
{

    //перечисления типов врагов и типов снарядов
    public enum EnemyTypes : byte

    {
        alien = 0,
        robot = 1,
        asteroid = 2
    }

    public enum ProjectileType : byte

    {
        freeze = 0,
        fire = 1,
        poison = 2,
        bullPlayer = 3
    }
}

