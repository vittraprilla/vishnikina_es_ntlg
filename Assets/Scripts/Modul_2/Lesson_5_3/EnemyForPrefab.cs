﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace rpg_prototype
{
    public class EnemyForPrefab : Enemy
    {
        //скрипт для врагов в проекте

        //логика взаимодействия коллайдера врага и пули игрока
        private void OnTriggerEnter(Collider other)
        {
            var pula = other.GetComponent<BulletBase>();

            //если снаряд принадлежит игроку
            if (pula.data._type == ProjectileType.bullPlayer)
            {
                data.EnemyHealth -= pula.data.Damage; //минусуем здоровье врага

                textHP.text = data.EnemyHealth.ToString(); 
                contrCom.DestroyBullets(pula); //уничтожаем саму пулю

            }

            else
            {
                Debug.Log("It is my bullet");
                //если снаряд не игрока, ничего не происходит
            }

        }
    }
}

