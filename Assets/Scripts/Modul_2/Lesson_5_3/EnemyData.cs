﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace rpg_prototype
{
    //Создаю класс с базовыми характеристиками врагов
    
    public class EnemyData
    {
    
        public int EnemyHealth = 10;//здоровье
        public float EnemySpeedAttack = 3; //скорость атаки
        public float EnemySpeedMove = 5; //скорость движения
        public float EnemyRadiusAttack = 2; //радиус атаки
        EnemyTypes EnemyType; //тип врага
        ProjectileType WeaponType; //тип его снаряда

        //внутри класса создаю конструктор с базовыми значениями, которые потом буду переопределять в базовом классе врагов
        public EnemyData(EnemyTypes enemyType, ProjectileType weaponType, int enemyHealth = 10, float enemySpeedAttack = 3f, float enemySpeedMove = 5f, float enemyRadiusAttack = 4f)
        {
            EnemyHealth = enemyHealth;
            EnemySpeedAttack = enemySpeedAttack;
            EnemySpeedMove = enemySpeedMove;
            EnemyRadiusAttack = enemyRadiusAttack;
            EnemyType = enemyType;
            WeaponType = weaponType;
        }

        public EnemyData()
        {

        }
    }

    
}


