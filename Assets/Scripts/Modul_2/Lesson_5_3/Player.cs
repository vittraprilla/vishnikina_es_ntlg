﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace rpg_prototype
{
    public class Player : MonoBehaviour
    {
        //класс игрока
        public int PlayerHealth = 20; //здоровье
        public float speed = 5f; //скорость
        public float speedRotation = 1f; //скорость поворота
        public bool cooldown = false; //колдаун для стрельбы
        public Slider BarHp; //бар здоровья
        public ControlComponent contrCom; //ссылка на управляющий компонент
        public GameObject panelDeath; //окно смерти игрока
        public Text countText; //счетчик убитых врагов


        void Start()
        {
            StartCoroutine(Go());
            BarHp.value = PlayerHealth;
        }

        private void Update()
        {
            //слежу за здоровьем игрока, если он погибает, останавливаю время и открываю окно смерти
            if (PlayerHealth <= 0)
            {
                Time.timeScale = 0;
                countText.text = ControlComponent.count.ToString();
                panelDeath.gameObject.SetActive(true);

            }
            //использую дефайн для задания по сборке билда, тут вызов параллельного управления
#if UNITY_EDITOR

            EditorControl();

#endif

        }

        //колдаун для стрельбы
        IEnumerator Cooldown()
        {
            yield return new WaitForSeconds(1f);
            cooldown = false;
            
        }


        private IEnumerator Go()
        {
            while (true)
            {


                //движение
                if (Input.GetKey(KeyCode.A))
                {
                    transform.Translate(Vector3.left * speed * Time.deltaTime);

                }
                if (Input.GetKey(KeyCode.D))
                {
                    transform.Translate(Vector3.right * speed * Time.deltaTime);
                }


                if (Input.GetKey(KeyCode.S))
                {
                    transform.Translate(-Vector3.forward * speed * Time.deltaTime);
                }

                if (Input.GetKey(KeyCode.W))
                {
                    transform.Translate(Vector3.forward * speed * Time.deltaTime);
                }

                //поворот
                if (Input.GetKey(KeyCode.Q) || Input.GetKey(KeyCode.LeftArrow))
                {
                    transform.Rotate(Vector3.down * speedRotation * Time.deltaTime);
                }
                if (Input.GetKey(KeyCode.E))
                {
                    transform.Rotate(Vector3.up * speedRotation * Time.deltaTime);
                }

                //СТРЕЛЬБА
                if (Input.GetKey(KeyCode.Space) && cooldown == false)
                {
                  
                  contrCom.CreatePlayerBullet(); 
                    cooldown = true;
                    StartCoroutine(Cooldown());

                }

                yield return null;

            }
        }

        //метод для взаимодействия коллайдера игрока и снарядов
        private void OnTriggerEnter(Collider other)
        {
            var pula = other.GetComponent<BulletBase>();

            //если снаряд игрока, то ничего не происходит

            if (pula.data._type == ProjectileType.bullPlayer)
            {
                Debug.Log("It is my bullet");
            }

            //если снаряд врагов, то наносим урон
            else
            {
                PlayerHealth -= pula.data.Damage;
                BarHp.value = PlayerHealth;
                contrCom.DestroyBullets(pula); //уничтожаю пулю на сцене
            }
  
        }

        //для задания по сборке билда
        //тут сам параллельный метод работы управления
      
        private void EditorControl()
        {
            if (Input.GetKey(KeyCode.LeftShift) && cooldown == false)
            {
                contrCom.CreatePlayerBullet();
                cooldown = true;
                StartCoroutine(Cooldown());
            }

            if (Input.GetKey(KeyCode.LeftArrow))
            {
                transform.Translate(Vector3.left * speed * Time.deltaTime);

            }
            if (Input.GetKey(KeyCode.RightArrow))
            {
                transform.Translate(Vector3.right * speed * Time.deltaTime);
            }


            if (Input.GetKey(KeyCode.DownArrow))
            {
                transform.Translate(-Vector3.forward * speed * Time.deltaTime);
            }

            if (Input.GetKey(KeyCode.UpArrow))
            {
                transform.Translate(Vector3.forward * speed * Time.deltaTime);
            }
        }


    }
}

