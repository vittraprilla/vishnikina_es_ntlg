﻿using rpg_prototype;
using System.Collections.Generic;
using System.Linq;

using UnityEngine;

public class DataBase : MonoBehaviour
{
    public const int c_INVENTORY_CAPACITY = 100;

    private List<EffectData> MyEffects = new List <EffectData>(); //коллекция эффектов
    private List <EnemyController> MyEnemies = new List<EnemyController>();//коллекция врагов
    private Dictionary<EquipmentItemType, Equipment> MyEquipment = new Dictionary<EquipmentItemType, Equipment>();//словарь экипировки
    private List <Item> Inventary = new List<Item>(c_INVENTORY_CAPACITY); //коллекция инвентаря
    

    #region Effects Data

    /// <summary>
    /// Добавление нового эффекта
    /// </summary>
    /// <param name="data">Добавляемый эффект</param>
    public void AddEffect(EffectData data)
    {
        MyEffects.Add(data);

    }

    /// <summary>
    /// Попытка удалить эффект из коллекции эффектов
    /// </summary>
    /// <param name="data">Удаляемый эффект</param>
    /// <returns>Успешна-ли операция</returns>
    public bool TryToRemoveEffect(EffectData data)
    {
     MyEffects.Remove(data);

       /* if(MyEffects.Contains (data))
        {
            return false;
        }
        else
        {
            return true;
        }*/
        return !(MyEffects.Contains(data));  //ВНЕСЛА ПРАВКУ ОТ ПРЕПОДА  
        
	}

    /// <summary>
    /// Удаление эффектов, наложенных на определенную цель
    /// </summary>
    /// <param name="target">Цель, с которой удаляются все эффекты</param>
    public void RemoveAllEffectOnTarget(Controller target)
    {
        MyEffects.RemoveAll(item => item.Target == target);//ВНЕСЛА ПРАВКУ ОТ ПРЕПОДА  


        //var delEffects = new List<EffectData>(MyEffects); //делаю клон коллекции с эффектами
        //foreach(var effect in delEffects) //перебираю в клоне-коллекции эффекты
        //{
        //     if (effect.Target == target) //если нахожу эффект с искомым таргетом
        //     {
        //         MyEffects.Remove(effect); //удаляю эффект из основной коллекции
        //     } 


        //}

    }

    /// <summary>
    /// Удаление эффектов, наложенных на других, каким-то источником
    /// </summary>
    /// <param name="source">Источник эффектов</param>
    public void RemoveAllEffectsBySource(Controller source)
    {

        MyEffects.RemoveAll(item => item.Source == source); //ВНЕСЛА ПРАВКУ ОТ ПРЕПОДА  

        //var delEffects = new List<EffectData>(MyEffects); //делаю клон коллекции с эффектами
        // foreach (var effect in delEffects) //перебираю в клоне-коллекции эффекты
        // {
        //     if (effect.Source == source) //если нахожу эффект с искомым источником
        //     {
        //         MyEffects.Remove(effect); //удаляю эффект из коллекции
        //     }
        // }
    }

    /// <summary>
    /// Возвращает все эффекты на какой-то цели
    /// </summary>
    /// <param name="target">Цель, эффекты, наложенные на которую, нужно вернуть</param>
    /// <returns>Коллекция эффектов</returns>
    public IEnumerable<EffectData> FindAllEffectsOnTarget(Controller target)
    {
        var effectsTarget = new List<EffectData>(); //создаю пустой лист возвращаемых эффектов

        effectsTarget = MyEffects.Where(effect => effect.Target == target).ToList(); //ищу в коллекции эффекты с нужной целью, если нахожу, добавляю этот эффект в пустой лист

        return effectsTarget; //возвращаю коллекцию с выбранными эффектами

    }

    /// <summary>
    /// Возвращает все эффекты, которые наложил какой-то источник
    /// </summary>
    /// <param name="target">Источник, который наложил эффекты на других персонажей</param>
    /// <returns>Коллекция эффектов</returns>
    public IEnumerable<EffectData> FindAllEffectsBySource(Controller source) //тоже самое, что и предыдущее, только другой параметр для выборки
    {
        var effectsSource = new List<EffectData>();

        effectsSource = MyEffects.Where(effect => effect.Source == source).ToList();

        return effectsSource;
    }

    /// <summary>
    /// Здесь каждый кадр нужно уменьшать время существование всех эффектов,
    /// если длительность эффекта закончилась, то эффект должен удалиться из коллекции
    /// </summary>
    private void Update()
	{
        var cloneEffects = new List<EffectData>(MyEffects); //создаю клон коллекции эффектов

        foreach (var effect in cloneEffects) //прохожусь по коллекции
        {
            effect.Duraction--; //в каждом элементе коллекции уменьшаю продолжительность на единицу
            if (effect.Duraction <= 0) //проверяю, если продолжительность меньше или равна нулю
            {
                MyEffects.Remove(effect); //в этом случае удаляю эффект из коллекции
            }
        }
    }

	#endregion

	#region Enemies Data

    /// <summary>
    /// Добавление в коллекцию нового противника
    /// </summary>
    public void AddEnemy(EnemyController enemy)
    {
        MyEnemies.Add(enemy);
    }

    /// <summary>
    /// Удаление противника из отслеживания
    /// </summary>
    public void RemoveEnemy(EnemyController enemy)
    {
        MyEnemies.Remove(enemy);
    }

    /// <summary>
    /// Добавление группы противников
    /// </summary>
    public void AddEnemies(IEnumerable<EnemyController> enemies)
    {
        MyEnemies.AddRange(enemies);
    }

    /// <summary>
    /// Удаление группы противников
    /// </summary>
    public void RemoveEnemies(IEnumerable<EnemyController> enemies)
    {
        foreach (var enemy in enemies)
        {
            MyEnemies.Remove(enemy);
        }

     
    }

    /// <summary>
    /// Возвращает всех противников определенного типа
    /// </summary>
    /// <param name="type">Тип противников</param>
    /// <returns>Коллекция типовых противников</returns>
    public IEnumerable<EnemyController> GetAllEnemiesByType(EnemyType type)
    {
        //ПЕРВЫЙ ВАРИАНТ
        //var result = new List<EnemyController>();
        //foreach (var enemy in MyEnemies)
        //{
        //    if (enemy.Type == type)
        //        result.Add(enemy);
        //}
        //return result;

        //ВТОРОЙ ВАРИАНТ
        return MyEnemies.Where(enemy => enemy.Type == type);
    }

    /// <summary>
    /// Поиск самого ближайшего противника к юниту
    /// </summary>
    /// <param name="unit">Юнит</param>
    /// <returns>Искомый противник</returns>
    public EnemyController GetNearestEnemy(Controller unit) 
        {
          var enemiesDistance = new List<float>(); //завожу локальный лист с дистанциями врагов
           var enem = new EnemyController(); //обозначаю противника, которого буду искать

           foreach (var enemy in MyEnemies) //прохожусь по листу с противниками
           {
               float enemyRadius = Vector3.Distance(enemy.transform.position, unit.transform.position); //высчитываю дистанцию между юнитом и противником для каждого врага
               enemiesDistance.Add(enemyRadius); //добавляю эту дистанцию в лист с дистанциями

           }

           float minDist = enemiesDistance.Min(); //нахожу минимальное значение в листе с дистанциями

           foreach (var enemy in MyEnemies) //снова прохожусь по коллеции с врагами
           {
               float enemyRadius = Vector3.Distance(enemy.transform.position, unit.transform.position); //опять высчитываю их дистанцию
               if (enemyRadius == minDist) //сравниваю дистанцию текущего врага и минимальную
               {
                    enem = enemy; //если дистанции равны, то кладу этого врага в изначально созданную переменную и выхожу из цикла
                   break;
               }

           } 
      
        

            return enem; //возвращаю найденного врага
    }

    /// <summary>
    /// Поиск всех противников в радиусе вокруг юнита
    /// </summary>
    /// <param name="radius">Радиус, вокруг которого происходит поиск</param>
    /// <param name="unit">Юнит, находящийся в центре окружности поиска</param>
    /// <returns>Список всех противников, стоящих достаточно близко</returns>
    public IEnumerable<EnemyController> GetEnemiesInRadius(float radius, Controller unit)
    {
      

        var enemyInRadius = new List<EnemyController>(); //завожу лист для врагов в радиусе

        foreach (var enemy in MyEnemies) //прохожу по списку и считаю у каждого врага расстояние от юнита
        {
            float enemyRadius = Vector3.Distance(enemy.transform.position, unit.transform.position); 
            if (enemyRadius < radius) //если расстояние меньше радиуса, то заношу врага в лист
            {
                    enemyInRadius.Add(enemy);
                }

            }
       
            
       return enemyInRadius; //возвращаю коллекцию с врагами в радиусе

    }

    /// <summary>
    /// Добавление эффектов на юнитов, стоящих в определенной отдаленности от источника 
    /// </summary>
    /// <param name="effect">Шаблон накладываемого эффекта</param>
    /// <param name="radius">Расстояние от цели до границы наложения эффекта</param>
    /// <param name="source">Центральный юнит, накладывающий эффекты</param>
    public void AddEffectInRadius(EffectData effect, float radius, Controller source)
    {
        var units = new List<Controller>(); //создаю лист с юнитами. На самом деле, он пустой, не знаю, надо ли было в него значение класть откуда-то?
        foreach (var unit in units) //прохожусь по списку с юнитами, проверяю их дистанцию от источника
        {
            if(Vector3.Distance(unit.transform.position, source.transform.position) < radius) //ВНЕСЛА ПРАВКУ ОТ ПРЕПОДА
            {
                //effect.Source = source; //КОММЕНЧУ ИЗ-ЗА КОНФЛИКТА ДОБАВЛЕННОГО АССЕТА В ПРОЕКТ
                MyEffects.Add(effect);
            }
            //float unitDistance = Vector3.Distance(unit.transform.position, source.transform.position);
            //    if(unitDistance < radius)
            //{
            //    effect = new EffectData(); // если дистанция меньше радиуса, то создаю эффект. Тут я тоже не понимаю, правильно ли, я не понимаю, где я создаю эффект
            //}
        }
        

    }

    /// <summary>
    /// Возвращает всех противников определенных типов
    /// </summary>
    /// <param name="typeы">Коллекция типов противников</param>
    /// <returns>Коллекция типовых противников</returns>
    /// <remarks>Запись (params EnemyType[] types) означает, что в качестве аргументов может быть любое количество EnemyType, даже нуль</remarks>
    /// Если types равен нулю нужно вернуть пустую коллекцию
    public IEnumerable<EnemyController> GetAllEnemiesByTypes(params EnemyType[] types)
    {

        var enCon = new List<EnemyController>(); //создаю лист с противниками разных типов

        if (types == null) return enCon; //если коллекция с типами пустая, то я возвращаю пустой лист

        foreach (var type in types) //прохожусь по коллекции с типами, рассматриваю каждый тип
        {
            var result = MyEnemies.Where(enemy => enemy.Type == type); //если в моей коллекции врагов есть враги этого типа, добавляю их в локальное перечисление результат
            if (result != null)
            {                         //если враги этого типа нашлись, то добавляю их в коллекцию типовых противников
                enCon.AddRange(result);

            }
        }

        return enCon;
    }

    /// <summary>
    /// Возвращает всех противников определенного типа в определенном радиусе
    /// </summary>
    /// <param name="radius">Радиус, вокруг которого происходит поиск</param>
    /// <param name="unit">Юнит, находящийся в центре окружности поиска</param>
    /// <param name="type">Тип противников</param>
    /// <returns>Коллекция типовых противников</returns>
    public IEnumerable<EnemyController> GetNearestInRaduisByType(float radius, Controller unit, EnemyType type)
    {
     
        var enemyInRadius = new List<EnemyController>(); //выше уже такую логику описывала, повторяться не буду

        foreach (var enemy in MyEnemies)
        {
            float enemyRadius = Vector3.Distance(enemy.transform.position, unit.transform.position);

            if (enemyRadius < radius && enemy.Type == type) //тут идет проверка не только на радиус, но и на тип
            {
                enemyInRadius.Add(enemy);
            }

        }


        return enemyInRadius;

    }

    /// <summary>
    /// Возвращает всех противников определенных типов в определенном радиусе
    /// </summary>
    /// <param name="radius">Радиус, вокруг которого происходит поиск</param>
    /// <param name="unit">Юнит, находящийся в центре окружности поиска</param>
    /// <param name="types">Типы противников</param>
    /// <returns>Коллекция типовых противников</returns>
    public IEnumerable<EnemyController> GetNearestInRaduisByTypes(float radius, Controller unit, params EnemyType[] types)
    {
        var enemysInRadius = new List<EnemyController>(); //создаю лист, который потом буду возвращать

        foreach (var type in types) //опять пробегаюсь по типам в коллекции с типами врагов

        {
            foreach(var enemy in MyEnemies)
            {
                float enemyRadius = Vector3.Distance(enemy.transform.position, unit.transform.position);
                if(enemyRadius < radius && enemy.Type == type) //для каждого типа проверяю типа врага и радиус
                {
                    enemysInRadius.Add(enemy); //если все подходит, добавляю его в коллекцию
                }

            }
          
        }

        return enemysInRadius;

     

    }

    #endregion

    #region Player Equipment    //СО СЛОВАРЕМ САМОЕ НЕПОНЯТНОЕ

    /// <summary>
    /// Пытается снарядить предмет на персонажа
    /// </summary>
    /// <param name="equipment">Надеваемый предмет</param>
    /// <returns>Удачно-ли произошло надевание</returns>
    public bool TryToEquipItem(Equipment equipment)
    {

        if (MyEquipment.ContainsKey(equipment.Type)) //проверка, есть ли в словаре итем с нужным ключом
        {
            return false; 

        }

        else {
            MyEquipment.Add(equipment.Type, equipment); //если нет, добавляем в словарь, возвращаем true
            return true;
        }
    }

    /// <summary>
    /// Пытается получить предмет с персонажа
    /// </summary>
    /// <param name="type">Тип предмета</param>
    /// <param name="isSuccess">Надет-ли предмет на персонажа</param>
    /// <returns>Запрашиваемый предмет</returns>
    public Equipment TryGetEquipItem(EquipmentItemType type, out bool isSuccess)
    {
        if (MyEquipment.ContainsKey(type)) //проверяем, есть ли предмет с этим ключом в экипировке
        {
            isSuccess = true; //если есть, ставим в буль тру, возвращаем предмет с этим ключом
            return MyEquipment[type];

        }
      
        else
        {
            isSuccess = false; //если нет,в буль ставим фолс, возвращаем значением по дефолту
            return default;
        }

    }

    /// <summary>
    /// Снаряжает с заменой предмет
    /// </summary>
    /// <param name="equip">Надеваемый предмет</param>
    /// <param name="isSwitch">Произошла-ли замена снаряжения</param>
    /// <returns>Значение по-умолчанию или снятые предмет</returns>
    public Equipment EquipOrSwitchItem(Equipment equip, out bool isSwitch)
    {
       
        if (MyEquipment.ContainsValue(equip)) //проверяем, есть ли в словаре предмет с искомым значением
        {
            isSwitch = false; //если есть, в булль ставим фолс, возвращаем дефолт, заменить не смогли
            return default;
        }

        else
        {
            MyEquipment.Add(equip.Type, equip); //если нет, добавляем искомое в словарь
            isSwitch =true; //в буль тру, возвращаем предмет из словаря
            return MyEquipment[equip.Type];
        }

        


    }

    /// <summary>
    /// Проверяет, надет-ли предмет
    /// </summary>
    /// <param name="id">Уникальный идентификатор предмета</param>
    /// <returns>Надет-ли предмет на персонажа</returns>
    public bool CheckEquipByID(ulong id)
    {

        var thing = MyEquipment.FirstOrDefault(item => item.Value.Item.ID == id); //ВНЕСЛА ПРАВКУ ОТ ПРЕПОДА
        if (!thing.Equals(default(KeyValuePair<int, Equipment>)))
        {
            return true;
        }
        else
        {
            return false;
        }

        /*   var  i = 0; //здесь жуткий костыль, но не смогла придумать лучше. завожу локальную переменную типа счетчика или буля:)

           foreach (var item in MyEquipment) //прохожусь по экипировке
           {
               if( item.Value.Item.ID == id) //если у итема искомый айдишник
               {
                   i++; //плюсую локальную переменную

               }

               else
               {

               }

           }

           if (i > 0) //если локальная переменная больше нуля
           {
               return true; //возвращаем тру
           }
           else
           {
               return false;
           }

           */

    }

  

    /// <summary>
    /// Проверяет, надет-ли предмет
    /// </summary>
    /// <param name="name">Название предмета</param>
    /// <returns>Надет-ли предмет на персонажа</returns>
    public bool CheckEquipByName(string name)
    {
        var i = 0; //тут такой же костыль, не смогла понять, как сделать правильно

        foreach (var item in MyEquipment)
        {
            if (item.Value.Item.Name == name)
            {
                i++;
            }

        }

        if (i > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /// <summary>
    /// Пытается получить предмет по идентификатору
    /// </summary>
    /// <param name="id">Уникальный идентификатор предмета</param>
    /// <param name="equip">Значение по-умолчанию или запрашиваемый предмет</param>
    /// <returns>Надет-ли данный предмет на персонажа</returns>
    public bool TryToGetEquipByID(ulong id, out Equipment equip)
    {
        equip = default; //присваиваем пустому значению дефолт на всякий пожарный

        foreach (var item in MyEquipment) //перебираем экипировку
        {
           
            
            if( item.Value.Item.ID == id) //ищем предмет с искомым идентификатором
            {
                 MyEquipment.TryGetValue(item.Key, out equip); //берем его значение через ключ

                equip = item.Value; //присваеваем значение этого айтема в пустой параметр метода

                return true;
            }

            else
            {
                equip = default; //если не нашли, возвращаем дефолт
                return false;

            }
            
        }


        return false; //это тоже какой-то костыль, но с буллями не могу понять, как надо в этих случаях правильно сделать

    }

    /// <summary>
    /// Пытается получить предмет по названию
    /// </summary>
    /// <param name="id">Название предмета</param>
    /// <param name="equip">Значение по-умолчанию или запрашиваемый предмет</param>
    /// <returns>Надет-ли данный предмет на персонажа</returns>
    public bool TryToGetEquipByName(string name, out Equipment equip)
    {
        equip = default; //тут тоже самое, только поиск по имени

        foreach (var item in MyEquipment) //перебираем экипировку
        {


            if (item.Value.Item.Name == name) //ищем предмет с искомым идентификатором
            {
                MyEquipment.TryGetValue(item.Key, out equip);

                equip = item.Value;

                return true;
            }

            else
            {
                equip = default;
                return false;

            }

        }
        return false; // и тот же костыль

    }

    /// <summary>
    /// Снятие предмета с персонажа
    /// </summary>
    /// <param name="type">Тип снимаемого предмета</param>
    /// <returns>Снятый предмет</returns>
    public Equipment UnequipItem(EquipmentItemType type)  
    {

        if (MyEquipment.ContainsKey(type)) //ищу предмет в словаре по ключу
        {

            var delType = MyEquipment[type]; //присваиваю этот айтем в локальную переменную, чтобы сохранить

            MyEquipment.Remove(type); //если нашла, то удаляю его

            return delType; //возвращаю сохраненное значение в локальной переменной
        }

        else
        {
            return default;
        }

    }

    /// <summary>
    /// Снятие предмета с персонажа
    /// </summary>
    /// <param name="name">Название снимаемого предмета</param>
    /// <returns>Снятый предмет</returns>
    public Equipment UnequipItem(string name)
    {
        
        foreach (Equipment eqiap in MyEquipment.Values) //прохожусь по значениям в словаре
        {
            if(eqiap.Item.Name == name) //если нахожу предмет с нужным именем
            {
                
                MyEquipment.Remove(eqiap.Type); //удаляю этот предмет
                return eqiap; //возвращаю его же. тут уже без локальной переменной, какой вариант правильнее, не знаю


            }

        
        }

        return default;
    }

    /// <summary>
    /// Снятие предмета с персонажа
    /// </summary>
    /// <param name="id">Идентификатор снимаемого предмета</param>
    /// <returns>Снятый предмет</returns>
    public Equipment UnequipItem(ulong id)
    {

        foreach (Equipment eqiap in MyEquipment.Values) //тут тоже самое, только параметр поиска другой
        {
            if (eqiap.Item.ID == id) 
            {

                MyEquipment.Remove(eqiap.Type);
                return eqiap;


            }


        }

        return default;
    }

    #endregion

    #region Player inventory

    /// <summary>
    /// Добавляем в сумку персонажу предмет
    /// </summary>
    /// <param name="item">Новый предмет</param>
    /// <returns>Умещается-ли в сумку предмет</returns>
    public bool AddItem (Item item)
    {
        
       
        if (Inventary.Count < GetCapacityInventory()) //проверка,есть ли в инваке свободное место
        {
            Inventary.Add(item); //если есть, добавляем
            return true;
        }
        else
        {
            return false;
        }

    }

    /// <summary>
    /// Добавляем в сумку персонажу предметы
    /// </summary>
    /// <param name="items">Коллекция предметов</param>
    /// <param name="filling">Заполнять-ли сумку до верху</param>
    /// <returns>Умещается-ли в сумку предмет</returns>
    public bool AddItems(IEnumerable<Item> items, bool filling = false)
    {
        if (filling) //если сумка не заполнена
        {
            do //и до тех пор, пока не заполнится
            {
                foreach (var newItem in items) //кладем в инвак предметы
                {
                    Inventary.Add(newItem);
                
                }

            } while (Inventary.Count < GetCapacityInventory()); 
            return true;
        }

        else
        {
            return false;
        }
    }

    /// <summary>
    /// Возвращает количество предметов в инвентаре
    /// </summary>
    /// <returns>Число предметов</returns>
    public int GetCountAllItems()
    {
     return Inventary.Count;
    }

    /// <summary>
    /// Возвращает вместимость инвентаря
    /// </summary>
    /// <returns>Максимальное количество предметов в сумке</returns>
    public int GetCapacityInventory()
    {
      //return Inventary.Capacity;
        return c_INVENTORY_CAPACITY;
    }

    /// <summary>
    /// Возвращает количество предметов одного типа
    /// </summary>
    /// <param name="id">Идентификатор типа предметов</param>
    /// <returns>Количество предметов</returns>
    public int GetCountItemsByID (ulong id)
    {
        var SameIdCount = new List<ulong>();  //создаем пустой лист для айдишников

        foreach (var SameId in Inventary) //смотрим по инваку, есть ли предеты с искомым айди
        {
            if(SameId.ID == id)
            {
                SameIdCount.Add(SameId.ID); //если есть, добавляем этот айди в лист
            }
        }

        return SameIdCount.Count; //возвращаем количество айдишников

    }

    /// <summary>
    /// Возвращает количество предметов одного наименования
    /// </summary>
    /// <param name="name">Название предметов</param>
    /// <param name="isFullCompliance">Считать только те, что имеют название точь-в-точь</param>
    /// <returns>Количество предметов</returns>
    public int GetCountItemsByName (string name, bool isFullCompliance = true)
    {
       
        int matches = 0; //заводим инт для количества предметов

        if (isFullCompliance) //если нам надо совпадение точь-в-точь
        {
            foreach (var item in Inventary)
            {
                if (item.Name.Equals(name)) //проверяем, есть ли в инваке предметы с идентичным именем
                {
                    matches = matches ++; //если есть, плюсуем счетчик
                }
            }
        }
        else
        {
            foreach (var item in Inventary) //и если нам не нужно точное посимвольное совпадение
            {
                if (item.Name.Contains(name))
                {
                    matches = matches ++;
                }
            }
        }

        if (matches == null) return 0;

       else return matches; //возвращаем количество предметов


    }

    /// <summary>
    /// Возвращает количество уникальных предметов
    /// </summary>
    public int GetCountUniqueItems()

    {
        //первый вариант, но это похожу слишком объемно

        //var UniqueItemsCount = new List<int>(); //создаю лист для уникальных предметов

        //IEnumerable<Item> UniqueItems = Inventary.Distinct(); //убираю дубликаты из коллекции

        //foreach (var UniqueItem in UniqueItems) //перебираю перечисление-коллекцию и добавляю ее элементы в коллекцию уникальных предметов
        //{
        //    UniqueItemsCount.Add(1);
        //}

        //return UniqueItemsCount.Count; //возвращаю их количество

        //второй вариант

        var unicItems = new List<Item>(Inventary); //делаю клон инвака

        unicItems.Distinct(); //удаляю из клона дубликаты
        return unicItems.Count; //возвращаю их количество
    }

    /// <summary>
    /// Возвращает все дубликаты предметов из инвентаря и удаляет их оттуда
    /// </summary>
    /// <returns>Коллекция извлеченных предметов</returns>
    public IEnumerable<Item> GetAndRemoveAllDuplicates()
    {
        var ClonInventary = new List<Item>(Inventary); //делаю клон инвака
        Inventary.Distinct(); //из инвака удаляю дубликаты
        IEnumerable<Item> extractItems = ClonInventary.Except(Inventary); //получаю разность клона и инвака, а это удаленные предметы

        return extractItems; //возвращаю удаленные предметы

    }

    /// <summary>
    /// Возвращает все одинаковые предметы
    /// </summary>
    /// <param name="id">Идентификатор запрашиваемых предметов</param>
    /// <returns>Коллекция одинаковых предметов</returns>
    public IEnumerable<Item> GetItemsByID(ulong id)
    {

        var allSameItems = new List<Item>(); //создаю пустой лист

        foreach (var SameItems in Inventary) //похожусь по инваку
        {
            if (SameItems.ID == id)
            {
                allSameItems.Add(SameItems); //если айтем с нужным мне айди, добавляю его в пустой лист
            }
        }

        return allSameItems;
    }

    /// <summary>
    /// Возвращает все одинаковые предметы
    /// </summary>
    /// <param name="name">Название предметов</param>
    /// <param name="isFullCompliance">Считать только те, что имеют название точь-в-точь</param>
    /// <returns>Коллекция одинаковых предметов</returns>
    public IEnumerable<Item> GetItemsByName(string name, bool isFullCompliance = true)
    {
        var collectionSameItems = new List<Item>(); 

        if (isFullCompliance) // тут тоже самое, только провожу еще проверку, нужно ли мне точное совпадение или нет
        {
            foreach (var item in Inventary)
            {
                if (item.Name.Equals(name))
                {
                    collectionSameItems.Add(item);
                }
            }
        }

        else
        {
            foreach (var item in Inventary)
            {
                if (item.Name.Contains(name))
                {
                    collectionSameItems.Add(item);
                }
            }
        }

        return collectionSameItems;
    }

    /// <summary>
    /// Уничтожает все одинаковые предметы по имени
    /// </summary>
    /// <param name="name">Название предметов</param>
    /// <param name="isFullCompliance">Считать только те, что имеют название точь-в-точь</param>
    public void RemoveAllItemsWithName(string name, bool isFullCompliance = true)
    {
        var clonInventary = new List<Item>(Inventary); //делаю локальную коллекцию - клон Инвентаря

        if (isFullCompliance) //проверяю, если нужно точное совпадение имени
        {
            foreach (var item in clonInventary) //перебираю локальную коллекцию
            {
                if (item.Name.Equals(name)) //если имя предмета совпадает полностью
                {
                    Inventary.Remove(item); //удаляю предмет из Инвентаря
                }
            }
        }

        else //тоже самое, только если не использую полное совпадение точь-в-точь
        {
            foreach (var item in clonInventary)
            {
                if (item.Name.Contains(name))
                {
                    Inventary.Remove(item);
                }
            }
        }
    }

    /// <summary>
    /// Уничтожает все одинаковые предметы по идентификатору
    /// </summary>
    /// <param name="name">Идентификатор предметов</param>
    public void RemoveAllItemsWithID(ulong id)

    {

        var allSameItems = new List<Item>(Inventary); //тоже самое, только с айди

        foreach (var sameItems in allSameItems)
        {
            if (sameItems.ID == id)
            {
                Inventary.Remove(sameItems);
            }
        }

       

    }

    /// <summary>
    /// Заменяет предмет на новый
    /// </summary>
    /// <param name="replaceID">Идентификатор заменяемых предметов</param>
    /// <param name="count">Количество заменяемых предметов</param>
    /// <param name="newItem">Новый предмет на замену</param>
    /// <returns>Удачна-ли операция</returns>
    public bool ReplaceItemsWith(ulong replaceID, int count, Item newItem)
    {
        //ПЕРВЫЙ ВАРИАНТ, НАВЕРНЯКА НЕПРАВИЛЬНЫЙ

        /*   var allNewItems = new List<Item>(Inventary); //делаю локальную коллекцию, копию Инвентаря

     for( int i = count; i == count; i--) //прохожусь по ней столько раз, сколько предметов надо заменить
           {
               foreach (var item in allNewItems) //проверяю в клоне, если айди предмета равен айди заменяемого предмета
               {
                  if (item.ID == replaceID)
                   {
                       Inventary.Remove(item); //удаляю этот предмет
                       Inventary.Add(newItem); //вместо него кладу новый
                   }

               }
           }

           if (Inventary.Contains(newItem)) return true; //проверяю, есть ли новый предмет в инвентаре
           else return false;

       */

        //ВТОРОЙ ВАРИАНТ

        var replaced = 0; //завожу локальную переменную, количество перемещенных предметов

        for (int i = 0; i < Inventary.Count && replaced < count; i++) //прохожусь по содержимому инвака с условием, пока количество перемещенных предметов меньше общего количества предметов, которые надо переместить
        {
            if(Inventary[i].ID == replaceID) //если айди равен искомому, то заменяем предмет и плюсуем локальную переменную
            {
                Inventary[i] = newItem;
                replaced++;
            }
        }

        return replaced > 0; 
    }



    /// <summary>
    /// Заменяет предмет на новый
    /// </summary>
    /// <param name="replaceName">Название заменяемых предметов</param>
    /// <param name="count">Количество заменяемых предметов</param>
    /// <param name="newItem">Новый предмет на замену</param>
    /// <returns>Удачна-ли операция</returns>
    public bool ReplaceItemsWith(string replaceName, int count, Item newItem)
    {

        var delItems = new List<Item>(Inventary);//делаю локальную коллекцию, копию Инвентаря
        foreach (var item in delItems) //проверяю в клоне, если имя предмета равно имени заменяемого предмета
        {
            if (item.Name == replaceName)
            {
                Inventary.Remove(item); //удаляю этот предмет

            }

        }

        var newItems = new List<Item>(count); //делаю коллекция новых предметов, которые надо положить

        Inventary.AddRange(newItems); //добавляю эти предметы
        if (Inventary.Contains(newItem)) return true; //проверяю, есть ли новый предмет в инвентаре
        else return false;
    }

    #endregion
}
