﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace net
{
    public static class Dubugger
    {
        private static Text _console;

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterSceneLoad)]
        public static void OnStart()
        {
            _console = GameObject.FindObjectsOfType<Text>().FirstOrDefault(t => t.name == "console");

#if UNITY_EDITOR
            Debug.Log("Console not found");
#endif
        }

        public static void Log (object message)
        {
#if UNITY_EDITOR
            Debug.Log(message);
#elif UNITY_STANDALONE_WIN && !UNITY_EDITOR
            _console.text += message;
#endif
        }


        public static byte[] SerialezePlayerData (object data)
        {
            var player = (PlayerData)data;
            var array = new List<byte>(16);
            array.AddRange(BitConverter.GetBytes(player.posX));
            array.AddRange(BitConverter.GetBytes(player.posZ));
            array.AddRange(BitConverter.GetBytes(player.rotY));
            array.AddRange(BitConverter.GetBytes(player.HP));

            return array.ToArray();
        }

        public static object DeserialezePlayerData(byte[] data)
        {
            return new PlayerData
            {
                posX = BitConverter.ToSingle(data, 0),
                posZ = BitConverter.ToSingle(data, 4),
                rotY = BitConverter.ToSingle(data, 8),
                HP = BitConverter.ToSingle(data, 12)
            };
        }
    }

    public struct PlayerData
    {
        public float posX;
        public float posZ;
        public float rotY;
        public float HP;

        public static PlayerData Create (PlContrl player)
        {
            return new PlayerData
            {
                posX = player.transform.position.x,
                posZ = player.transform.position.z,
                rotY = player.transform.eulerAngles.y,
                HP = player.Health

            };
        }

        public void Set (PlContrl player)
        {
            var vector = player.transform.position;
            vector.x = posX; vector.z = posZ;
            player.transform.position = vector;

            vector = player.transform.eulerAngles;
            vector.y = rotY;
            player.transform.eulerAngles = vector;

            player.Health = HP;
        }
    }
}
