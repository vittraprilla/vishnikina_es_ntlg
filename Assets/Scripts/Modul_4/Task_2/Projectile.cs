﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace net
{
    public class Projectile : MonoBehaviour
    {
        [SerializeField, Range(1f, 10f)]
        private float _movespeed = 3f;
        [SerializeField, Range(1f, 10f)]
        private float _damage = 1f;
        [SerializeField, Range(1f, 15f)]
        private float _lifeTime = 7f;
        public string Parent { get; set; }
        public float GetDamage => _damage;
        void Start()
        {
            StartCoroutine(OnDie());
        }

        // Update is called once per frame
        void Update()
        {
            transform.position += transform.forward * _movespeed * Time.deltaTime;
        }

        private IEnumerator OnDie()
        {
            yield return new WaitForSeconds(_lifeTime);
            Destroy(gameObject);
        }
    }
}

