﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace net
{
    public class Timer : MonoBehaviour
    {
        [SerializeField] private Text _text;
        void Start()
        {
            StartCoroutine(TimerCorut());
        }

       private IEnumerator TimerCorut()
        {
            var time = 15;
            while(time != 0)
            {
                _text.text = time.ToString();
                time--;
                yield return new WaitForSeconds(1f);
            }
        }
    }

}
