﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

namespace net
{
    public class MenuManager : MonoBehaviourPunCallbacks
    {
        public void OnCreate_UnityEditor()
        {
            PhotonNetwork.CreateRoom(null, new Photon.Realtime.RoomOptions { MaxPlayers = 2 });
        }

        public void OnJoin_UnityEditor()
        {
            PhotonNetwork.JoinRandomRoom();
        }

        public void OnQuit_UnityEditor()
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#elif  UNITY_STANDALONE_WIN && !UNITY_EDITOR
            Application.Quit();
#endif
        }
        void Start()
        {
#if UNITY_EDITOR
            PhotonNetwork.NickName = "1";
#elif UNITY_STANDALONE_WIN && !UNITY_EDITOR
            PhotonNetwork.NickName = "2";
#endif

            PhotonNetwork.AutomaticallySyncScene = true;
            PhotonNetwork.GameVersion = "0.0.1";
            PhotonNetwork.ConnectUsingSettings();
        }

        public override void OnConnectedToMaster()
        {
            Dubugger.Log("Ready to connection");
        }

        public override void OnJoinedRoom()
        {
            PhotonNetwork.LoadLevel("Modul_4_Task_2");
        }
       
    }
}

