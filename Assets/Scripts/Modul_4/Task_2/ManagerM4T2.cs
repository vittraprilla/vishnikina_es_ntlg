﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using Photon.Pun;
using UnityEngine.SceneManagement;
using ExitGames.Client.Photon;

namespace net
{
    public class ManagerM4T2 : MonoBehaviourPunCallbacks
    {
        
        private PlContrl _player1;
        private PlContrl _player2;
        [SerializeField] private Transform[] SpawnPoints;
        private List<SphereBase> _allSpheres = new List<SphereBase>(15);
        [SerializeField] private SphereBase _badSphere;
        [SerializeField] private SphereBase _goodSphere;
        [SerializeField]   private InputAction _quit;
        [SerializeField] private string _playerPrefabName;
        [SerializeField, Range(1f, 15f)] private float _randomInterval = 7f;
        void Start()
        {
            _quit.Enable();
            _quit.performed += OnQuit;

            var pos = new Vector3(UnityEngine.Random.Range(-_randomInterval, _randomInterval), 1.5f, UnityEngine.Random.Range(-_randomInterval, _randomInterval));
            var GO = PhotonNetwork.Instantiate(_playerPrefabName + PhotonNetwork.NickName, pos, new Quaternion());

            PhotonPeer.RegisterType(typeof(PlayerData), 100, Dubugger.SerialezePlayerData, Dubugger.DeserialezePlayerData);


            foreach (var point in SpawnPoints)
            {
                int randInt = UnityEngine.Random.Range(0, 3);
                if(randInt == 0 || randInt == 2)
                {
                    Instantiate(_goodSphere, point.position, point.rotation);
                }

                else if (randInt == 1 || randInt == 3)
                {
                    Instantiate(_badSphere, point.position, point.rotation);
                }
            }
        }

        public void AddPlayer(PlContrl player)
        {
            if (player.name.Contains("1")) _player1 = player;
            else _player2 = player;

            if (_player1 != null && _player2 != null)
            {
                _player1.SetTarget(_player2.transform);
                _player2.SetTarget(_player1.transform);
            }
        }
        public override void OnLeftRoom()
        {
            SceneManager.LoadScene(0);
        }

        private void OnQuit(InputAction.CallbackContext obj)
        {
            PhotonNetwork.LeaveRoom();
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#elif  UNITY_STANDALONE_WIN && !UNITY_EDITOR
            Application.Quit();
#endif
        }

        private void OnDestroy()
        {
            _quit.Dispose();
        }
    }

}
