﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;


namespace net
{
    public class PlContrl : MonoBehaviour, IPunObservable
    {
        //private bool _FirstPlayer;
        private Contrl _controls;
       [SerializeField] private Rigidbody _rigitBody;
        [SerializeField, Range(1f, 10f)] private float _movespeed = 2f;
        [SerializeField, Range(0.5f, 5f)] private float _maxSpeed = 2f;
        [Range(1f, 50f)] public float Health = 5f;
        private Transform _target;
        [SerializeField] private Projectile _bulletPrefab;
        [SerializeField, Range(0.1f, 1f)] private float _attackDelay = 0.4f;
        [SerializeField, Range(0.1f, 1f)] private float _rotateDelay = 0.25f;
        [SerializeField] private Vector3 _firePoint;
        [SerializeField] private GameObject _spawnPoint;
        private Transform _bulletPool;
        [SerializeField] private PhotonView _photonView;
        //отсюда мои добавления
        [SerializeField]
        private float _speed = 10f;
        [SerializeField]
        private float _speedRotation = 60f;

        //private GameObject _camera_1; //ссылка на 1 камеру
        //[SerializeField] private Vector3 _offset_1; //отступ для 1 камеры
        //private float _smothSpeed = 0.1f; //скорость для сглаживания движения камеры


        void Start()
        {
            //var cam = FindObjectOfType<Camera>();
            //_camera_1 = cam.gameObject;
            //_camera_1.transform.eulerAngles = transform.eulerAngles;
            //_camera_1.transform.Rotate(new Vector3(34f, 0f, 0f));


            _bulletPool = FindObjectOfType<UnityEngine.EventSystems.EventSystem>().transform;
            _rigitBody = GetComponent<Rigidbody>();
            _controls = new Contrl();

            FindObjectOfType<ManagerM4T2>().AddPlayer(this);

          //  _FirstPlayer = name.Contains("1");
          //  if (_FirstPlayer)   _controls.Player1.Enable();
          //else  _controls.Player2.Enable();

           //_controls.Player1.Enable();

           // StartCoroutine(Fire());
           // StartCoroutine(Focus());
        }

        public void SetTarget (Transform target)
        {
            _target = target;
            if (!_photonView.IsMine) return;

            _controls.Player1.Enable();

            //StartCoroutine(Fire());
            //StartCoroutine(Focus());

        }
        private void OnDrawGizmos()
        {
            Gizmos.color = Color.green;
            Gizmos.DrawSphere(_firePoint, 0.2f);
        }
        private IEnumerator Fire()
        {
            while (true)
            {
                //var bullet = Instantiate(_bulletPrefab, _spawnPoint.transform.position, /*transform.TransformPoint(_firePoint)*/ transform.rotation);
                var bullet = Instantiate(_bulletPrefab, _bulletPool);
                bullet.transform.position = _spawnPoint.transform.position;
                bullet.transform.rotation = transform.rotation;
                bullet.Parent = name;
                yield return new WaitForSeconds(_attackDelay);
            }
        }

        private IEnumerator Focus()
        {
            while (true)
            {
                transform.LookAt(_target);
                transform.eulerAngles = new Vector3(0f, transform.eulerAngles.y, 0f);
                yield return new WaitForSeconds(_rotateDelay);
            }
            
        }

        //ТУТ ПОКА КОММИЧУ ВЕСЬ МЕТОД
        //void FixedUpdate()
        //{
        //    if (!_photonView.IsMine) return;

        //    //var direction = _FirstPlayer
        //    //    ? _controls.Player1.Movement.ReadValue<Vector2>()
        //    //    : _controls.Player2.Movement.ReadValue<Vector2>();

        //    var direction =  _controls.Player1.Movement.ReadValue<Vector2>();

        //    if (direction.x == 0 && direction.y == 0) return;

        //    var velocity = _rigitBody.velocity;
        //    velocity += new Vector3(direction.x, 0f, direction.y) * _movespeed * Time.fixedDeltaTime;

        //    velocity.y = 0f;
        //    velocity = Vector3.ClampMagnitude(velocity, _maxSpeed);
        //    _rigitBody.velocity = velocity;
        //}

        private void Update()
        {
            //движение камеры за игроком
            //Vector3 Position_1 = transform.position + _offset_1;
            //Vector3 SmothVector_1 = Vector3.Lerp(_camera_1.transform.position, Position_1, _smothSpeed);
            //_camera_1.transform.position = SmothVector_1;
            


            //движение
            if (Input.GetKey(KeyCode.A))
            {
                transform.Translate(Vector3.left * _speed * Time.deltaTime);

            }
            if (Input.GetKey(KeyCode.D))
            {
                transform.Translate(Vector3.right * _speed * Time.deltaTime);
            }


            if (Input.GetKey(KeyCode.S))
            {
                transform.Translate(-Vector3.forward * _speed * Time.deltaTime);
            }

            if (Input.GetKey(KeyCode.W))
            {
                transform.Translate(Vector3.forward * _speed * Time.deltaTime);
            }

            //поворот
            if (Input.GetKey(KeyCode.Q) || Input.GetKey(KeyCode.LeftArrow))
            {
                transform.Rotate(Vector3.down * _speedRotation * Time.deltaTime);
               
            }
            if (Input.GetKey(KeyCode.E))
            {
                transform.Rotate(Vector3.up * _speedRotation * Time.deltaTime);
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            var bullet = other.GetComponent<Projectile>();
            if (bullet == null || bullet.Parent == name) return;
            Health -= bullet.GetDamage;

            Destroy(other.gameObject);
            if (Health <= 0f) Debug.Log("Died");
        }
        private void OnDestroy()
        {
            _controls.Player1.Disable();
            _controls.Player2.Disable();
        }

        public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
        {
            if (stream.IsWriting)
            {
                stream.SendNext(PlayerData.Create(this));
            }
            else
            {
                ((PlayerData)stream.ReceiveNext()).Set(this);
            }
        }
    }

}
