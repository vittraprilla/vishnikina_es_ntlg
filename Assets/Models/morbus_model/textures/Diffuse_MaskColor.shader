﻿Shader "Diffuse MaskColor"
{
	Properties
	{
		_MainTex ("Diffuse", 2D) = "white" {}
		_MaskTex ("Mask", 2D) = "white" {}

		_Color ("Color", Color) = (1.0, 1.0, 1.0, 1.0)
	}

	SubShader
	{
		Tags
		{
			"RenderType" = "Opaque"
		}

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

			sampler2D _MainTex;
			float4 _MainTex_ST;
			sampler2D _MaskTex;
			float4 _MaskTex_ST;

			fixed4 _Color;

			struct VertexInput
			{
				float4 Position : POSITION;
				float2 TexCoord : TEXCOORD0;
			};

			struct VertexOutput
			{
				float4 Position : SV_POSITION;
				float4 TexCoord : TEXCOORD0;
			};

			struct FragmentOutput
			{
				fixed4 Color : SV_TARGET;
			};

			VertexOutput vert(VertexInput input)
			{
				VertexOutput output;

				output.Position = UnityObjectToClipPos(input.Position);

				output.TexCoord.xy = TRANSFORM_TEX(input.TexCoord, _MainTex);
				output.TexCoord.zw = TRANSFORM_TEX(input.TexCoord, _MaskTex);

				return output;
			}

			FragmentOutput frag(VertexOutput input)
			{
				FragmentOutput output;

				fixed4 Color = tex2D(_MainTex, input.TexCoord.xy);
				fixed Mask = tex2D(_MaskTex, input.TexCoord.zw).r;

				Color.rgb *= lerp(1.0f.xxx, _Color.rgb, Mask);

				output.Color = Color;

				return output;
			}
			ENDCG
		}
	}
}